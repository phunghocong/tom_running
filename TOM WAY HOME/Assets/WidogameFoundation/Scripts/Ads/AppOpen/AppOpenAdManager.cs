#if MEDIATION_APP_OPEN_AD
#if MMP_ADJUST
using com.adjust.sdk;
#elif MMP_APPSFLYER
using AppsFlyerSDK;
#endif
using Firebase.Analytics;
using GoogleMobileAds.Api;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WidogameFoundation.Ads;

public class AppOpenAdManager {
    private static AppOpenAdManager instance;

    private AppOpenAd ad;

    private bool isShowingAd = false;
    private Action onAdFinishAction;
    private DateTime loadTime;
    private bool lastLoadFailed;
    private ScreenOrientation currentScreenOrientation = ScreenOrientation.Portrait;
    private int currentIdIndex;
    private int continuousNoFill;

    public static AppOpenAdManager Instance {
        get {
            if (instance == null) {
                instance = new AppOpenAdManager();
            }

            return instance;
        }
    }

    private string AdUnitId {
        get {
            string[] waterFall = WaterFall;
#if UNITY_ANDROID
            string standaloneId = WidogameFoundation.Config.WidogameAppSettingsLoader.AppSettings.AndroidAdmobAppOpenId;
#else
            string standaloneId = WidogameFoundation.Config.WidogameAppSettingsLoader.AppSettings.IOSAdmobAppOpenId;
#endif
            if (waterFall != null && waterFall.Length > 0) {
                if (currentIdIndex >= waterFall.Length) {
                    currentIdIndex = 0;
                }
                return waterFall[currentIdIndex];
            } else {
                return standaloneId;
            }
        }
    }

    private string[] WaterFall {
        get {
#if UNITY_ANDROID
            return WidogameFoundation.Config.WidogameAppSettingsLoader.AppSettings.AndroidAdmobAppOpenWaterFall;
#else
            return WidogameFoundation.Config.WidogameAppSettingsLoader.AppSettings.IOSAdmobAppOpenWaterFall;
#endif

        }
    }

    public void ShowAdIfAvailable(Action finishAction) {
        if (!IsAdAvailable || isShowingAd || AdsMediationController.IsFullscreenAdsShowing || Math.Abs(DateTimeOffset.Now.ToUnixTimeMilliseconds() - AdsMediationController.LastFullscreenAdsShown) < 2000) {
            LoadAdIfExpiredOrFailed();
            finishAction?.Invoke();
            return;
        }
        this.onAdFinishAction = finishAction;
        ad.OnAdFullScreenContentClosed += HandleAdDidDismissFullScreenContent;
        ad.OnAdFullScreenContentFailed += HandleAdFailedToPresentFullScreenContent;
        ad.OnAdFullScreenContentOpened += HandleAdDidPresentFullScreenContent;
        ad.OnAdImpressionRecorded += HandleAdDidRecordImpression;
        ad.OnAdPaid += HandlePaidEvent;
        ad.Show();
    }
    private void HandleAdDidDismissFullScreenContent() {
        Debug.Log("Closed app open ad");
        // Set the ad to null to indicate that AppOpenAdManager no longer has another ad to show.
        ad = null;
        isShowingAd = false;
        LoadAd();
        AdsMediationController.Instance.Enqueue(InvokeOnAdFinished);        
    }

    private void HandleAdFailedToPresentFullScreenContent(AdError adError) {
        Debug.LogFormat("Failed to present the ad (reason: {0})", adError.GetMessage());
        // Set the ad to null to indicate that AppOpenAdManager no longer has another ad to show.
        ad = null;
        AdsMediationController.Instance.Enqueue(InvokeOnAdFinished);
        LoadAd();
    }

    private void InvokeOnAdFinished() {
        onAdFinishAction?.Invoke();
    }

    private void HandleAdDidPresentFullScreenContent() {
        isShowingAd = true;
    }

    private void HandleAdDidRecordImpression() {
    }

    private void HandlePaidEvent(AdValue adValue) {
        double revenue = adValue.Value / 1000000.0;
#if MMP_ADJUST
        AdjustAdRevenue adjustAdRevenue = new AdjustAdRevenue(AdjustConfig.AdjustAdRevenueSourceAdMob);
        adjustAdRevenue.setRevenue(revenue, adValue.CurrencyCode);
        Adjust.trackAdRevenue(adjustAdRevenue);
#elif MMP_APPSFLYER
        Dictionary<string, string> dic = new Dictionary<string, string>();
        dic["af_quantity"] = "1";
        AppsFlyerAdRevenue.logAdRevenue("admob", AppsFlyerAdRevenueMediationNetworkType.AppsFlyerAdRevenueMediationNetworkTypeGoogleAdMob, revenue, "USD", dic);
#endif

        FirebaseAdsRevenueLogger.AddAppOpenRevenue((float)revenue);

        var parameters = new[] {
                new Parameter("ad_platform", "Admob"),
                new Parameter("ad_format", "app_open"),
                new Parameter("value", revenue),
                new Parameter("currency", adValue.CurrencyCode),
            };
        FirebaseAdsRevenueLogger.LogAdsRevenueEvent(parameters);

    }

    public bool IsAdAvailable {
        get {
            return ad != null && !IsAdExpired;
        }
    }

    private bool IsAdExpired
    {
        get
        {
            return (DateTime.Now - loadTime).TotalHours > 4;
        }
    }

    private void LoadAdIfExpiredOrFailed() {
        if ((ad != null && IsAdExpired) || (ad == null && lastLoadFailed)) {
            LoadAd();
        }
    }

    public void LoadAd(ScreenOrientation orientation) {
        currentScreenOrientation = orientation;
        LoadAd();
    }

    private void LoadAd() {
        AdRequest request = new AdRequest.Builder().Build();
        lastLoadFailed = false;
        // Load an app open ad for portrait orientation
        AppOpenAd.Load(AdUnitId, currentScreenOrientation, request, ((appOpenAd, error) => {
            if (error != null) {
                // Handle the error.
                Debug.LogFormat("Failed to load the ad. (reason: {0})", error.GetMessage());
                lastLoadFailed = true;
                if (error.GetCode() != 2) {
                    continuousNoFill++;
                    currentIdIndex++;
                    string[] waterFall = WaterFall;
                    if (waterFall != null && continuousNoFill < waterFall.Length) {
                        LoadAd();
                    }
                }
                return;
            }
            continuousNoFill = 0;
            // App open ad is loaded.
            ad = appOpenAd;
            loadTime = DateTime.Now;
        }));
    }

}
#endif