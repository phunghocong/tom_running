using System;
using System.Collections;
using System.Collections.Generic;
#if MMP_ADJUST
using com.adjust.sdk;
#elif MMP_APPSFLYER
using AppsFlyerSDK;
#endif
using UnityEngine;
using WidogameFoundation.Ads;
using WidogameFoundation.Config;
using Firebase.Analytics;
#if MEDIATION_ADMOB || MEDIATION_ADMOB_RI || MEDIATION_ADMOB_MREC
using GoogleMobileAds.Api;
using GoogleMobileAds.Common;

namespace WidogameFoundation.Ads {
	public class AdmobAdsMediation : BaseAdsMediation {

		private BannerView bannerView;
		private BannerView mrecBannerView;
		private InterstitialAd interstitialAd;
		private RewardedAd rewardedAd;
		private RewardedInterstitialAd rewardedInterstitialAd;
		private bool isBannerLoaded;
		private bool isMrecLoaded;
		private BannerPosition currentBannerPosition;
		private BannerPosition currentMrecPosition;
		private List<Action> pendingActions = new List<Action>();
		private bool isInitialized;

		public override bool IsInterstitialAvailable {
			get {
#if UNITY_EDITOR
				return interstitialAd != null && UnityEngine.Random.Range(0, 2) == 1;
#else
				return interstitialAd != null;
#endif
			}
		}

		public override bool IsRewardedAdsAvailable {
			get {
#if UNITY_EDITOR
				return rewardedAd != null && UnityEngine.Random.Range(0, 2) == 1;
#else
				return rewardedAd != null;
#endif
			}
		}

		private string BannerAdUnitId {
			get {
#if UNITY_ANDROID
				return WidogameAppSettingsLoader.AppSettings.AndroidAdmobBannerId;
#else
				return WidogameAppSettingsLoader.AppSettings.IOSAdmobBannerId;
#endif
			}
		}

		private string InterstitialAdUnitId {
			get {
#if UNITY_ANDROID
				return WidogameAppSettingsLoader.AppSettings.AndroidAdmobInterstitialId;
#else
				return WidogameAppSettingsLoader.AppSettings.IOSAdmobInterstitialId;
#endif
			}
		}

		private string RewardedAdUnitId {
			get {
#if UNITY_ANDROID
				return WidogameAppSettingsLoader.AppSettings.AndroidAdmobRewardedId;
#else
				return WidogameAppSettingsLoader.AppSettings.IOSAdmobRewardedId;
#endif
			}
		}

		private string RewardedInterstitialAdUnitId {
			get {
#if UNITY_ANDROID
				return WidogameAppSettingsLoader.AppSettings.AndroidAdmobRewardedInterstitialId;
#else
				return WidogameAppSettingsLoader.AppSettings.IOSAdmobRewardedInterstitialId;
#endif
			}
		}

		private string MrecBannerAdUnitId {
			get {
#if UNITY_ANDROID
				return WidogameAppSettingsLoader.AppSettings.AndroidMrecBannerId;
#else
				return WidogameAppSettingsLoader.AppSettings.IOSMrecBannerId;
#endif
			}
		}

		public override bool IsRewardedInterstitialAvailable {
			get {
				return rewardedInterstitialAd != null;
			}
		}

		public override void HideBanner() {
			if (bannerView != null) {
				bannerView.Hide();
			}
		}

		public override void Init() {
			MobileAds.Initialize(InitAdsFormats);
		}

		private void InitAdsFormats(InitializationStatus status) {
			isInitialized = true;
			if (pendingActions.Count > 0) {
				for (int i = 0; i < pendingActions.Count; i++) {
					pendingActions[i].Invoke();
				}
				pendingActions.Clear();
			}
		}

		public override void InitInterstitial() {
		}

		private void InterstitialAd_OnAdClosed() {
			InvokeOnInterstitialClosed();
			DestroyInterstitial();
		}

		private void InterstitialAd_OnAdFailedToShow(AdError error) {
			InvokeOnInterstitialDisplayFailed();
			DestroyInterstitial();
		}

		private void InterstitialAd_OnAdFailedToLoad(object sender, AdFailedToLoadEventArgs e) {
			InvokeOnInterstitialLoadFailed();
		}

		private void InterstitialAd_OnAdLoaded(object sender, EventArgs e) {
			InvokeOnInterstitialLoaded(WidogameConstants.AD_NETWORK_NAME_NONE);
		}

		public override void InitRewardedAds() {
		}

		private void RewardedAd_OnAdClosed() {
			InvokeOnRewardedClosed();
			DestroyRewardedAds();
		}

		private void RewardedAd_OnAdFailedToShow(AdError e) {
			DestroyRewardedAds();
			InvokeOnRewardedDisplayFailed();
		}

		private void RewardedAd_OnAdFailedToLoad(object sender, AdFailedToLoadEventArgs e) {
			InvokeOnRewardedLoadFailed();
		}

		private void RewardedAd_OnUserEarnedReward(Reward e) {
			InvokeOnRewardedSuccess();
		}

		public override void LoadInterstitial() {
			if (isInitialized) {
				DoLoadingInterstitial();
			} else {
				pendingActions.Add(DoLoadingInterstitial);
			}
		}

		private void DoLoadingInterstitial() {
			DestroyInterstitial();
			AdRequest request = new AdRequest.Builder().Build();
			InterstitialAd.Load(InterstitialAdUnitId, request, OnLoadInterstitialAdCallback);
		}

		private void DestroyInterstitial() {
			if (interstitialAd != null) {
				interstitialAd.Destroy();
				interstitialAd = null;
			}
		}

		private void OnLoadInterstitialAdCallback(InterstitialAd ad, LoadAdError error) {
			if (error != null || ad == null) {
				InvokeOnInterstitialLoadFailed();
			} else {
				this.interstitialAd = ad;
				this.interstitialAd.OnAdFullScreenContentClosed += InterstitialAd_OnAdClosed;
				this.interstitialAd.OnAdFullScreenContentFailed += InterstitialAd_OnAdFailedToShow;
				this.interstitialAd.OnAdPaid += Ad_OnInterPaidEvent;
				InvokeOnInterstitialLoaded(WidogameConstants.AD_NETWORK_NAME_NONE);
			}
		}

		public override void LoadRewardedAds() {
			if (isInitialized) {
				DoLoadingRewardedAds();
			} else {
				pendingActions.Add(DoLoadingRewardedAds);
			}
		}

		private void DoLoadingRewardedAds() {
			DestroyRewardedAds();
			AdRequest request = new AdRequest.Builder().Build();
			RewardedAd.Load(RewardedAdUnitId, request, OnLoadRewardedAdCallback);
		}

		private void DestroyRewardedAds() {
			if (rewardedAd != null) {
				this.rewardedAd.Destroy();
				this.rewardedAd = null;
			}
		}

		private void OnLoadRewardedAdCallback(RewardedAd ad, LoadAdError error) {
			if (error != null || ad == null) {
				InvokeOnRewardedLoadFailed();
			} else {
				rewardedAd = ad;
				rewardedAd.OnAdFullScreenContentClosed += RewardedAd_OnAdClosed;
				rewardedAd.OnAdFullScreenContentFailed += RewardedAd_OnAdFailedToShow;
				rewardedAd.OnAdPaid += Ad_OnRewardedPaidEvent;
				InvokeOnRewardedLoaded(WidogameConstants.AD_NETWORK_NAME_NONE);
			}
		}

		public override void ShowBanner(BannerPosition position) {
			if (isBannerLoaded) {
				if (position != currentBannerPosition) {
					bannerView.OnAdPaid -= Ad_OnBannerPaidEvent;
					bannerView.Destroy();
					LoadBanner(position);
				}
			} else {
				LoadBanner(position);
			}
			bannerView.Show();
		}

		public override void ShowInterstitial(string placement) {
			this.interstitialAd.Show();
		}

		public override void ShowRewardedAds(string placement) {
			this.rewardedAd.Show(RewardedAd_OnUserEarnedReward);
		}

		private void LoadBanner(BannerPosition position) {
			this.bannerView = new BannerView(BannerAdUnitId, AdSize.SmartBanner, position == BannerPosition.Bottom ? AdPosition.Bottom : AdPosition.Top);
			this.bannerView.OnAdPaid += Ad_OnBannerPaidEvent;
			AdRequest request = new AdRequest.Builder().Build();
			this.bannerView.LoadAd(request);
			isBannerLoaded = true;
			currentBannerPosition = position;
		}

		private void Ad_OnBannerPaidEvent(AdValue adValue) {
			Ad_OnPaidEvent(adValue);
			var parameters = new[] {
				new Parameter("ad_platform", "Admob"),
				new Parameter("ad_format", "banner"),
				new Parameter("value", adValue.Value / 1000000.0),
				new Parameter("currency", adValue.CurrencyCode),
			};
			FirebaseAdsRevenueLogger.LogAdsRevenueEvent(parameters);
			FirebaseAdsRevenueLogger.AddBannerRevenue(adValue.Value / 1000000.0f);
		}

		private void Ad_OnInterPaidEvent(AdValue adValue) {
			Ad_OnPaidEvent(adValue);
			var parameters = new[] {
				new Parameter("ad_platform", "Admob"),
				new Parameter("ad_format", "interstitial"),
				new Parameter("value", adValue.Value / 1000000.0),
				new Parameter("currency", adValue.CurrencyCode),
			};
			FirebaseAdsRevenueLogger.LogAdsRevenueEvent(parameters);
			FirebaseAdsRevenueLogger.AddInterRevenue(adValue.Value / 1000000.0f);
		}

		private void Ad_OnRewardedPaidEvent(AdValue adValue) {
			Ad_OnPaidEvent(adValue);
			var parameters = new[] {
				new Parameter("ad_platform", "Admob"),
				new Parameter("ad_format", "rewarded"),
				new Parameter("value", adValue.Value / 1000000.0),
				new Parameter("currency", adValue.CurrencyCode),
			};
			FirebaseAdsRevenueLogger.LogAdsRevenueEvent(parameters);
			FirebaseAdsRevenueLogger.AddRewardedRevenue(adValue.Value / 1000000.0f);
		}

		private void Ad_OnRewardedInterPaidEvent(AdValue adValue) {
			Ad_OnPaidEvent(adValue);
			var parameters = new[] {
				new Parameter("ad_platform", "Admob"),
				new Parameter("ad_format", "rewarded_interstital"),
				new Parameter("value", adValue.Value / 1000000.0),
				new Parameter("currency", adValue.CurrencyCode),
			};
			FirebaseAdsRevenueLogger.LogAdsRevenueEvent(parameters);
			FirebaseAdsRevenueLogger.AddRewardedInterRevenue(adValue.Value / 1000000.0f);
		}

		private void Ad_OnPaidEvent(AdValue adValue) {
			double revenue = adValue.Value / 1000000.0;
#if MMP_ADJUST
			AdjustAdRevenue adjustAdRevenue = new AdjustAdRevenue(AdjustConfig.AdjustAdRevenueSourceAdMob);
			adjustAdRevenue.setRevenue(revenue, adValue.CurrencyCode);
			Adjust.trackAdRevenue(adjustAdRevenue);
#elif MMP_APPSFLYER
			Dictionary<string, string> dic = new Dictionary<string, string>();
			dic["af_quantity"] = "1";
			AppsFlyerAdRevenue.logAdRevenue("admob", AppsFlyerAdRevenueMediationNetworkType.AppsFlyerAdRevenueMediationNetworkTypeGoogleAdMob, revenue, adValue.CurrencyCode, dic);
#endif
		}

		public override void InitRewardedInterstitialAds() {
			
		}

		public override void LoadRewardedInterstitial() {
			DestroyRewardedInterstitial();
			AdRequest request = new AdRequest.Builder().Build();
			RewardedInterstitialAd.Load(RewardedInterstitialAdUnitId, request, RewardedInterstitialAdLoadCallback);
		}

		private void DestroyRewardedInterstitial() {
			if (rewardedInterstitialAd != null) {
				rewardedInterstitialAd.Destroy();
				rewardedInterstitialAd = null;
			}
		}

		public override void ShowRewardedInterstitial(string placement) {
			rewardedInterstitialAd.Show(RewardedInterstitialEarnRewardCallback);
		}

		private void RewardedInterstitialAdLoadCallback(RewardedInterstitialAd ad, LoadAdError error) {
			if (error == null && ad != null) {
				rewardedInterstitialAd = ad;
				rewardedInterstitialAd.OnAdFullScreenContentFailed += RewardedInterstitialAdDisplayFailed;
				rewardedInterstitialAd.OnAdFullScreenContentClosed += RewardedInterstitialAdClosed;
				rewardedInterstitialAd.OnAdPaid += Ad_OnRewardedInterPaidEvent;
				InvokeOnRewardedInterstitialLoaded();
			} else {
				InvokeOnRewardedInterstitialLoadFailed();
			}
		}

		private void RewardedInterstitialAdClosed() {
			rewardedInterstitialAd.OnAdFullScreenContentFailed -= RewardedInterstitialAdDisplayFailed;
			rewardedInterstitialAd.OnAdFullScreenContentClosed -= RewardedInterstitialAdClosed;
			InvokeOnRewardedInterstitialClosed();
			DestroyRewardedInterstitial();
		}

		private void RewardedInterstitialAdDisplayFailed(AdError error) {
			InvokeOnRewardedInterstitialDisplayFailed();
			DestroyRewardedInterstitial();
		}

		private void RewardedInterstitialEarnRewardCallback(Reward reward) {
			InvokeOnRewardedInterstitialSuccess();
		}

		public override void ShowMrec(BannerPosition position) {
			if (isMrecLoaded) {
				if (position != currentMrecPosition) {
					mrecBannerView.OnAdPaid -= Ad_OnBannerPaidEvent;
					mrecBannerView.Destroy();
					LoadMrec(position);
				}
			} else {
				LoadMrec(position);
			}
			mrecBannerView.Show();
		}

		public override void HideMrec() {
			mrecBannerView.Hide();
		}

		private void LoadMrec(BannerPosition position) {
			if (position == BannerPosition.CenterLeft || position == BannerPosition.CenterRight) {
				int bannerHeight = AdSize.MediumRectangle.Height;
				int bannerWidth = AdSize.MediumRectangle.Width;
				int x = position == BannerPosition.CenterLeft ? - Screen.width / 4 : Screen.width / 4;
				int y = Screen.height / 2;
				mrecBannerView = new BannerView(MrecBannerAdUnitId, AdSize.MediumRectangle, x, y);
			} else {
				AdPosition admobAdPosition = ConvertPosition(position);
				mrecBannerView = new BannerView(MrecBannerAdUnitId, AdSize.MediumRectangle, admobAdPosition);
			}
			mrecBannerView.OnAdPaid += Ad_OnBannerPaidEvent;
			AdRequest adRequest = new AdRequest.Builder().Build();
			mrecBannerView.LoadAd(adRequest);
			isMrecLoaded = true;
			currentMrecPosition = position;
		}

		private AdPosition ConvertPosition(BannerPosition src) {
			switch (src) {
				case BannerPosition.Top:
					return AdPosition.Top;
				case BannerPosition.Bottom:
					return AdPosition.Bottom;
				case BannerPosition.Center:
					return AdPosition.Center;
				case BannerPosition.TopLeft:
					return AdPosition.TopLeft;
				case BannerPosition.TopRight:
					return AdPosition.TopRight;
				case BannerPosition.BottomLeft:
					return AdPosition.BottomLeft;
				case BannerPosition.BottomRight:
					return AdPosition.BottomRight;
				default:
					return AdPosition.Center;
			}
		}
	}
}
#endif