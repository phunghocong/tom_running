using System.Collections;
using System.Collections.Generic;
#if MEDIATION_APS_ENABLED
using AmazonAds;
#if MMP_APPSFLYER
using AppsFlyerSDK;
#endif
#endif
#if MMP_ADJUST
using com.adjust.sdk;
#endif
using Firebase.Analytics;
using GoogleMobileAds.Api;
using UnityEngine;
using WidogameFoundation.Config;
#if MEDIATION_IRONSOURCE
namespace WidogameFoundation.Ads {
	public class IronSourceAdsMediation : BaseAdsMediation {
		private bool isBannerLoaded;
		private BannerPosition currentBannerPosition;
		public override bool IsInterstitialAvailable {
			get {
#if UNITY_EDITOR
				return UnityEngine.Random.Range(0, 2) == 1;
#else
				return IronSource.Agent.isInterstitialReady();
#endif

			}
		}

		public override bool IsRewardedAdsAvailable {
			get {
#if UNITY_EDITOR
				return UnityEngine.Random.Range(0, 2) == 1;
#else
				return IronSource.Agent.isRewardedVideoAvailable();
#endif
			}
		}

		public override void HideBanner() {
			IronSource.Agent.hideBanner();
		}

		private string IronSourceDevKey {
			get {
#if UNITY_ANDROID
				return WidogameAppSettingsLoader.AppSettings.AndroidIronSourceDevKey;
#else
                return WidogameAppSettingsLoader.AppSettings.IOSIronSourceDevKey;
#endif
			}
		}

		private string APSApiKey {
			get {
#if UNITY_ANDROID
				return WidogameAppSettingsLoader.AppSettings.AndroidAPSAppId;
#else
				return WidogameAppSettingsLoader.AppSettings.IOSAPSAppId;
#endif
			}
		}

		private string APSBannerSlotId {
			get {
#if UNITY_ANDROID
				return WidogameAppSettingsLoader.AppSettings.AndroidAPSBannerSlotId;

#else
				return WidogameAppSettingsLoader.AppSettings.IOSAPSBannerSlotId;
#endif
			}
		}

		private string APSInterstitialSlotId {
			get {
#if UNITY_ANDROID
				return WidogameAppSettingsLoader.AppSettings.AndroidAPSInterstitialSlotId;
#else
				return WidogameAppSettingsLoader.AppSettings.IOSAPSInterstitialSlotId;
#endif
			}
		}

		public override bool IsRewardedInterstitialAvailable {
			get {
				return false;
			}
		}

		public override void Init() {
#if MEDIATION_APS_ENABLED
			Amazon.Initialize(APSApiKey);
			Amazon.SetAdNetworkInfo(new AdNetworkInfo(DTBAdNetwork.IRON_SOURCE));
#endif
			IronSource.Agent.setConsent(true);
			IronSource.Agent.shouldTrackNetworkState(true);
			IronSource.Agent.init(IronSourceDevKey);

			IronSourceInterstitialEvents.onAdClosedEvent += IronSourceEvents_onInterstitialAdClosedEvent;
			IronSourceInterstitialEvents.onAdLoadFailedEvent += IronSourceEvents_onInterstitialAdLoadFailedEvent;
			IronSourceInterstitialEvents.onAdShowFailedEvent += IronSourceEvents_onInterstitialAdShowFailedEvent;
			IronSourceInterstitialEvents.onAdReadyEvent += IronSourceEvents_onInterstitialAdReadyEvent;

			IronSourceRewardedVideoEvents.onAdClosedEvent += IronSourceRewardedVideoEvents_onAdClosedEvent;
			IronSourceRewardedVideoEvents.onAdShowFailedEvent += IronSourceRewardedVideoEvents_onAdShowFailedEvent;
			IronSourceRewardedVideoEvents.onAdAvailableEvent += IronSourceRewardedVideoEvents_onAdAvailableEvent;
			IronSourceRewardedVideoEvents.onAdUnavailableEvent += IronSourceRewardedVideoEvents_onAdUnavailableEvent;
			IronSourceRewardedVideoEvents.onAdRewardedEvent += IronSourceRewardedVideoEvents_onAdRewardedEvent;

			IronSourceBannerEvents.onAdLoadedEvent += IronSourceEvents_onBannerAdLoadedEvent;
			

			IronSourceEvents.onImpressionDataReadyEvent += IronSourceEvents_onImpressionDataReadyEvent;
		}

		private void IronSourceRewardedVideoEvents_onAdUnavailableEvent() {
			//InvokeOnInterstitialLoadFailed();
		}

		private void IronSourceEvents_onImpressionDataReadyEvent(IronSourceImpressionData impressionData) {
			double revenue = impressionData.revenue.HasValue ? impressionData.revenue.Value : 0;
#if MMP_ADJUST
			AdjustAdRevenue adjustAdRevenue = new AdjustAdRevenue(AdjustConfig.AdjustAdRevenueSourceIronSource);
			adjustAdRevenue.setRevenue(revenue, "USD");
			// optional fields
			adjustAdRevenue.setAdRevenueNetwork(impressionData.adNetwork);
			adjustAdRevenue.setAdRevenueUnit(impressionData.adUnit);
			adjustAdRevenue.setAdRevenuePlacement(impressionData.placement);
			// track Adjust ad revenue
			Adjust.trackAdRevenue(adjustAdRevenue);
#elif MMP_APPSFLYER
			Dictionary<string, string> dic = new Dictionary<string, string>();
			dic["af_quantity"] = "1";
			AppsFlyerAdRevenue.logAdRevenue(impressionData.adNetwork, AppsFlyerAdRevenueMediationNetworkType.AppsFlyerAdRevenueMediationNetworkTypeIronSource, revenue, "USD", dic);
#endif
			string lowerAdUnit = impressionData.adUnit.ToLower();
			switch (lowerAdUnit) {
				case "banner":
					FirebaseAdsRevenueLogger.AddBannerRevenue((float)revenue);
#if MEDIATION_APS_ENABLED
			ManuallyLoadAPSBanner();
#endif
					break;
				case "interstitial":
					FirebaseAdsRevenueLogger.AddInterRevenue((float)revenue);
					break;
				case "rewarded_video":
					FirebaseAdsRevenueLogger.AddRewardedRevenue((float)revenue);
					break;

			}

			Parameter[] parameters = {
				new Parameter("ad_platform", "ironSource"),
				new Parameter("ad_source", impressionData.adNetwork),
				new Parameter("ad_unit_name", impressionData.instanceName),
				new Parameter("ad_format", impressionData.adUnit),
				new Parameter("currency","USD"),
				new Parameter("value", impressionData.revenue == null ? 0 : impressionData.revenue.Value)
			};
			FirebaseAdsRevenueLogger.LogAdsRevenueEvent(parameters);
		}

		private void IronSourceEvents_onBannerAdLoadedEvent(IronSourceAdInfo adInfo) {
		}

		private void IronSourceRewardedVideoEvents_onAdRewardedEvent(IronSourcePlacement placement, IronSourceAdInfo adInfo) {
			InvokeOnRewardedSuccess();
		}

		private void IronSourceRewardedVideoEvents_onAdAvailableEvent(IronSourceAdInfo adInfo) {
			InvokeOnRewardedLoaded(adInfo.adNetwork);
		}

		private void IronSourceRewardedVideoEvents_onAdShowFailedEvent(IronSourceError error, IronSourceAdInfo adInfo) {
			InvokeOnRewardedDisplayFailed();
		}

		private void IronSourceRewardedVideoEvents_onAdClosedEvent(IronSourceAdInfo adInfo) {
			InvokeOnRewardedClosed();
		}

		private void IronSourceEvents_onInterstitialAdReadyEvent(IronSourceAdInfo adInfo) {
			InvokeOnInterstitialLoaded(adInfo.adNetwork);
		}

		private void IronSourceEvents_onInterstitialAdShowFailedEvent(IronSourceError error, IronSourceAdInfo adInfo) {
			InvokeOnInterstitialDisplayFailed();
		}

		private void IronSourceEvents_onInterstitialAdLoadFailedEvent(IronSourceError error) {
			InvokeOnInterstitialLoadFailed();
		}

		private void IronSourceEvents_onInterstitialAdClosedEvent(IronSourceAdInfo adInfo) {
			InvokeOnInterstitialClosed();
		}

		public override void InitInterstitial() {
			IronSource.Agent.init(IronSourceDevKey, IronSourceAdUnits.INTERSTITIAL);
		}

		public override void InitRewardedAds() {
			IronSource.Agent.init(IronSourceDevKey, IronSourceAdUnits.REWARDED_VIDEO);
		}

		public override void LoadInterstitial() {
#if MEDIATION_APS_ENABLED
#if MEDIATION_APS_LANDSCAPE
			APSVideoAdRequest request = new APSVideoAdRequest(480, 320, APSInterstitialSlotId);
#else
			APSVideoAdRequest request = new APSVideoAdRequest(320, 480, APSInterstitialSlotId);
#endif
			request.onFailedWithError += (addError) => {
				IronSource.Agent.loadInterstitial();
			};
			request.onSuccess += (adResponse) => {
				IronSource.Agent.setNetworkData(APSMediationUtils.APS_IRON_SOURCE_NETWORK_KEY, APSMediationUtils.GetInterstitialNetworkData(APSInterstitialSlotId, adResponse));
				IronSource.Agent.loadInterstitial();
			};
			request.LoadAd();
#else
			IronSource.Agent.loadInterstitial();
#endif
		}

		public override void LoadRewardedAds() {
		}

		public override void ShowBanner(BannerPosition position) {
			if (isBannerLoaded) {
				if (position != currentBannerPosition) {
					IronSource.Agent.destroyBanner();
					IronSourceBannerPosition ironsourcePosition = position == BannerPosition.Top ? IronSourceBannerPosition.TOP : IronSourceBannerPosition.BOTTOM;
					AdsMediationController.Instance.Enqueue(LoadBannerDelay(ironsourcePosition));
				}
			} else {
				IronSource.Agent.init(IronSourceDevKey, IronSourceAdUnits.BANNER);
				LoadBanner(position == BannerPosition.Top ? IronSourceBannerPosition.TOP : IronSourceBannerPosition.BOTTOM);
				isBannerLoaded = true;
			}
			currentBannerPosition = position;
			IronSource.Agent.displayBanner();
		}

		private IEnumerator LoadBannerDelay(IronSourceBannerPosition position) {
			yield return null;
			LoadBanner(position);
		}

		private void LoadBanner(IronSourceBannerPosition position) {
#if MEDIATION_ADAPTIVE_BANNER_ENABLED
			//int width = (int)(Screen.width / MobileAds.Utils.GetDeviceScale());
			//IronSourceBannerSize bannerSize = new IronSourceBannerSize(width, 50);
			IronSourceBannerSize bannerSize = IronSourceBannerSize.SMART;
			bannerSize.SetAdaptive(true);
#else
			IronSourceBannerSize bannerSize = IronSourceBannerSize.BANNER;
#endif
#if MEDIATION_APS_ENABLED
			APSBannerAdRequest request = new APSBannerAdRequest(320, 50, APSBannerSlotId);
			request.onFailedWithError += (adError) => {
				IronSource.Agent.loadBanner(bannerSize, position);
			};
			request.onSuccess += (adResponse) => {
				IronSource.Agent.setNetworkData(APSMediationUtils.APS_IRON_SOURCE_NETWORK_KEY, APSMediationUtils.GetBannerNetworkData(APSBannerSlotId, adResponse));
				IronSource.Agent.loadBanner(bannerSize, position);
			};
			request.LoadAd();
#else
			IronSource.Agent.loadBanner(bannerSize, position);
#endif
		}

#if MEDIATION_APS_ENABLED
		private void ManuallyLoadAPSBanner() {
			APSBannerAdRequest request = new APSBannerAdRequest(320, 50, APSBannerSlotId);
			request.onFailedWithError += (adError) => {
			};
			request.onSuccess += (adResponse) => {
				IronSource.Agent.setNetworkData(APSMediationUtils.APS_IRON_SOURCE_NETWORK_KEY, APSMediationUtils.GetBannerNetworkData(APSBannerSlotId, adResponse));
			};
			request.LoadAd();
		}
#endif

		public override void ShowInterstitial(string placement) {
#if UNITY_EDITOR
			InvokeOnInterstitialClosed();
#endif
			IronSource.Agent.showInterstitial(placement);
		}

		public override void ShowRewardedAds(string placement) {
#if UNITY_EDITOR
			InvokeOnRewardedSuccess();
			InvokeOnRewardedClosed();
#endif
			IronSource.Agent.showRewardedVideo(placement);
		}

		public override void InitRewardedInterstitialAds() {
			
		}

		public override void LoadRewardedInterstitial() {
			
		}

		public override void ShowRewardedInterstitial(string placement) {
			ShowRewardedAds(placement);
		}

		public override void ShowMrec(BannerPosition position) {
		}

		public override void HideMrec() {
		}
	}
}
#endif