using System;
using System.Collections;
using System.Collections.Generic;
using Firebase.Analytics;
using UnityEngine;

namespace WidogameFoundation.Ads {

    public class FirebaseAdsRevenueLogger {

        private const string AD_BANNER_REVENUE = "ad_banner_revenue";
        private const string AD_INTER_REVENUE = "ad_inter_revenue";
        private const string AD_REWARDED_REVENUE = "ad_rewarded_revenue";
        private const string AD_REWARDED_INTER_REVENUE = "ad_rewarded_inter_revenue";
        private const string AD_APP_OPEN_REVENUE = "ad_app_open_revenue";
        private const string INTERSTITIAL_ADS_PAID = "interstitial_ads_paid";
        private const string REWARDED_ADS_PAID = "rewarded_ads_paid";
        private const string ADS_PAID = "ad_impression";

        private static Action<string, float> _onInterstitialRevenueLogCallback;
        private static Action<string, float> _onRewardedRevenueLogCallback;
        private static Action<string, Parameter[]> _onAdsRevenueLogCallback;
        private static Action<string, float> _onUserPropertyLogCallback;

        public static event Action<string, float> OnInterstitialRevenueLogCallback {
            add {
                _onInterstitialRevenueLogCallback += value;
            }
            remove {
                _onInterstitialRevenueLogCallback -= value;
            }
        }

        public static event Action<string, float> OnRewardedRevenueLogCallback {
            add {
                _onRewardedRevenueLogCallback += value;
            }
            remove {
                _onRewardedRevenueLogCallback -= value;
            }
        }

        public static event Action<string, Parameter[]> OnAdsRevenueLogCallback {
            add {
                _onAdsRevenueLogCallback += value;
            }
            remove {
                _onAdsRevenueLogCallback -= value;
            }
        }

        public static event Action<string, float> OnUserPropertyLogCallback {
            add {
                _onUserPropertyLogCallback += value;
            }
            remove {
                _onUserPropertyLogCallback -= value;
            }
        }

        public static event Action<float> OnBannerRevenueLogCallback;


        private static float BannerRevenue {
			get {
                return PlayerPrefs.GetFloat(AD_BANNER_REVENUE, 0f);
			}
            set {
                PlayerPrefs.SetFloat(AD_BANNER_REVENUE, value);
            }
        }

        private static float InterRevenue {
            get {
                return PlayerPrefs.GetFloat(AD_INTER_REVENUE, 0f);
            }
            set {
                PlayerPrefs.SetFloat(AD_REWARDED_REVENUE, value);
            }
		}

        private static float RewardedRevenue {
            get {
                return PlayerPrefs.GetFloat(AD_REWARDED_REVENUE, 0f);
            }
            set {
                PlayerPrefs.SetFloat(AD_REWARDED_REVENUE, value);
            }
        }

        private static float AppOpenRevenue {
            get {
                return PlayerPrefs.GetFloat(AD_APP_OPEN_REVENUE, 0f);
            }

            set {
                PlayerPrefs.SetFloat(AD_APP_OPEN_REVENUE, value);
            }
        }

        private static float RewardedInterRevenue {
            get {
                return PlayerPrefs.GetFloat(AD_REWARDED_INTER_REVENUE, 0f);
            }
            set {
                PlayerPrefs.SetFloat(AD_REWARDED_INTER_REVENUE, value);
            }
        }

        private static void LogUserProperty(string name, float value) {
            _onUserPropertyLogCallback?.Invoke(name, value);
        }

        public static void AddBannerRevenue(float revenue) {
            AdsMediationController.Instance.Enqueue(() => {
                BannerRevenue += revenue;
                LogUserProperty(AD_BANNER_REVENUE, BannerRevenue);
            });
        }

        public static void AddInterRevenue(float revenue) {
            AdsMediationController.Instance.Enqueue(() => {
                InterRevenue += revenue;
                LogUserProperty(AD_INTER_REVENUE, InterRevenue);
                InvokeLogInterstitialRevenue(INTERSTITIAL_ADS_PAID, revenue);
            });
        }

        public static void AddRewardedRevenue(float revenue) {
            AdsMediationController.Instance.Enqueue(() => {
                RewardedRevenue += revenue;
                LogUserProperty(AD_REWARDED_REVENUE, RewardedRevenue);
                InvokeLogRewardedRevenue(REWARDED_ADS_PAID, revenue);
            });
        }

        public static void AddAppOpenRevenue(float revenue) {
            AdsMediationController.Instance.Enqueue(() => {
                AppOpenRevenue += revenue;
                LogUserProperty(AD_APP_OPEN_REVENUE, AppOpenRevenue);
            });
        }

        public static void AddRewardedInterRevenue(float revenue) {
            AdsMediationController.Instance.Enqueue(() => {
                RewardedInterRevenue += revenue;
                LogUserProperty(AD_REWARDED_INTER_REVENUE, RewardedInterRevenue);
            });
        }

        public static void LogAdsRevenueEvent(Parameter[] parameters) {
            AdsMediationController.Instance.Enqueue(() => {
                _onAdsRevenueLogCallback?.Invoke(ADS_PAID, parameters);
            });
        }

        private static void InvokeLogInterstitialRevenue(string eventName, float revenue) {
            _onInterstitialRevenueLogCallback?.Invoke(eventName, revenue);
        }

        private static void InvokeLogRewardedRevenue(string eventName, float revenue) {
            _onRewardedRevenueLogCallback?.Invoke(eventName, revenue);
        }
    }
    
}