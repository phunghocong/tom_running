using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace WidogameFoundation.Config
{
    public class WidogameAppSettings : ScriptableObject
    {
        public static readonly string WIDOGAME_CONFIGURATION_ASSET_PATH = Path.Combine(WidogameConstants.WIDOGAME_RESOURCES_PATH, WidogameConstants.WIDOGAME_APP_SETTINGS_NAME + ".asset");

        [Header("Admob")]
        public string AndroidAdmobAppOpenId;
        public string[] AndroidAdmobAppOpenWaterFall;
        public string AndroidAdmobBannerId;
        public string AndroidAdmobInterstitialId;
        public string AndroidAdmobRewardedId;
        public string AndroidAdmobRewardedInterstitialId;
        public string AndroidMrecBannerId;
        public string IOSAdmobAppOpenId;
        public string[] IOSAdmobAppOpenWaterFall;
        public string IOSAdmobBannerId;
        public string IOSAdmobInterstitialId;
        public string IOSAdmobRewardedId;
        public string IOSAdmobRewardedInterstitialId;
        public string IOSMrecBannerId;

        [Header("Max Mediation")]
        public string MaxSdkKey;
        public string AndroidMaxBannerAdUnit;
        public string AndroidMaxInterstitialAdUnit;
        public string AndroidMaxRewardedAdUnit;
        public string IOSMaxBannerAdUnit;
        public string IOSMaxInterstitialAdUnit;
        public string IOSMaxRewardedAdUnit;

        [Header("IronSource Mediation")]
        public string AndroidIronSourceDevKey;
        public string IOSIronSourceDevKey;

        [Header("Huawei Ads")]
        public string AndroidHuaweiBannerAdUnit;
        public string AndroidHuaweiInterstitialAdUnit;
        public string AndroidHuaweiRewardedAdUnit;
        public string AndroidHuaweiRewardedInterstitialAdUnit;

        [Header("APS Amazon")]
        public string AndroidAPSAppId;
        public string AndroidAPSBannerSlotId;
        public string AndroidAPSInterstitialSlotId;
        public string IOSAPSAppId;
        public string IOSAPSBannerSlotId;
        public string IOSAPSInterstitialSlotId;
    }
}
