using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DataHolder", menuName = "ScriptableObject/DataHolder")]

public class DataHolder : ScriptableObject
{
    [Header("song list Ui")]
    public List<Sprite> spriteList;
}
