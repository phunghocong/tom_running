﻿using UnityEngine;
using UnityEngine.UI;
using PolyAndCode.UI;
using UnityEngine.Rendering;
using TMPro;

public class DemoCell : MonoBehaviour, ICell
{
    //UI
    public TextMeshProUGUI nameLabel;
    //public TextMeshProUGUI idLabel;

    //Model
    private ContactInfo _contactInfo;
    private int _cellIndex;


    public void ConfigureCell(ContactInfo contactInfo,int cellIndex)
    {
        _cellIndex = cellIndex;
        _contactInfo = contactInfo;

        nameLabel.text = contactInfo.Name;
        //idLabel.text = contactInfo.id;
    }

  
}
