using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class ShowRewardedController : MonoBehaviour
{
    public static bool isWatchingRewarded;
    [Tooltip("Screen")]
    public string scenarios;
    [Tooltip("Button")]
    public string placement;

    [SerializeField]
    private UnityEvent onRewardedAdsNotAvailable;
    [SerializeField]
    private UnityEvent onRewardedAdsStart;
    [SerializeField]
    private UnityEvent onRewardedAdsFailedToShow;
    [SerializeField]
    private OnRewardedClosedEvent onClosed;
   
    private float lastTimescale;

    public void Show() {
        //GameAnalytics.LogButtonClick(scenarios, placement);
        Debug.Log(AdsMediationController.Instance.IsRewardedAvailable + " " + AdsMediationController.Instance.IsInterstitialAvailable);
        if ((Application.internetReachability != NetworkReachability.NotReachable && AdsMediationController.Instance.IsRewardedAvailable) || GameData.IsTutorialMode()) { 
            AudioHelper.Instance.MuteSound();
            AudioHelper.Instance.MutePlaylist();
            isWatchingRewarded = true;
            GameData.showAds = true;
            lastTimescale = Time.timeScale;
            Time.timeScale = 0;
            //SDKPlayPrefs.SetDateTime(StringConstants.PREF_INTERSTITIAL_LAST_SHOWN, DateTime.Now);
            if (GameData.IsTutorialMode()) // Ko show reward ads o tutorial
            {
                OnRewardedAdsClosed(true);
            }
            else
            {
                AdsMediationController.Instance.ShowRewardedAds(placement, OnRewardedAdsFailedToShow, OnRewardedAdsClosed);
            }
            //GameAnalytics.LogWatchRewardAds(scenarios, true);
            onRewardedAdsStart?.Invoke();
        } else {
            //GameAnalytics.LogWatchRewardAds(scenarios, false);
            onRewardedAdsNotAvailable?.Invoke();
        }
    }

    private void OnRewardedAdsClosed(bool success) {
        //GameAnalytics.LogWatchRewardAdsDone(scenarios, true, success.ToString());
        AudioHelper.Instance.UnmuteSound();
        AudioHelper.Instance.UnmutePlaylist();
        Time.timeScale = lastTimescale;
        isWatchingRewarded = false;
        if (success)
        {
            onClosed?.Invoke(success);
        }
        else
        {
            onRewardedAdsFailedToShow?.Invoke();
        }
        Invoke("MarkShowAdsFalseDelay", 0.05f);
    }


    private void OnRewardedAdsFailedToShow()
    {
        //GameAnalytics.LogWatchRewardAdsDone(scenarios, true, "can't_show");
        AudioHelper.Instance.UnmuteSound();
        AudioHelper.Instance.UnmutePlaylist();
        Time.timeScale = lastTimescale;
        isWatchingRewarded = false;
        onRewardedAdsFailedToShow?.Invoke();
        Invoke("MarkShowAdsFalseDelay", 0.05f);
    }

    private void MarkShowAdsFalseDelay() {
        GameData.showAds = false;
    }
}

[Serializable]
public class OnRewardedClosedEvent : UnityEvent<bool> {

}
