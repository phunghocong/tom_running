using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HolderAds : MonoBehaviour
{
    private float timeStartUp;
    private float lastTimescale;
    private void OnApplicationPause(bool pause)
    {
        if (pause)
        {
            timeStartUp = Time.realtimeSinceStartup;
        }
        else
        {
            Debug.Log("HolderAds onApplicationPause");
            if (Time.realtimeSinceStartup - timeStartUp >= 10 && !GameData.showAds)
            {
                Debug.Log("Show inter ads on Application resume");
                if (AdsMediationController.Instance.IsInterstitialAvailable && Application.internetReachability != NetworkReachability.NotReachable) {
                    AudioHelper.Instance.MutePlaylist();
                    lastTimescale = Time.timeScale;
                    Time.timeScale = 0;
                    AdsMediationController.Instance.ShowInterstitial("sleep_ads", OnInterstitialFinished);
                }

            }
            timeStartUp = Time.realtimeSinceStartup;

        }
    }

    private void OnInterstitialFinished(bool success) {
        AudioHelper.Instance.UnmutePlaylist();
        Time.timeScale = lastTimescale;
    }

}
