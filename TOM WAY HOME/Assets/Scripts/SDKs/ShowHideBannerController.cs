﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WidogameFoundation.Ads;

public class ShowHideBannerController : MonoBehaviour
{
    [SerializeField]
    private bool showOnEnabled;
    [SerializeField] bool showTop;
    [SerializeField] bool showBottom;

    //private void Start()
    //{
    //    ShowHideBanner(!showOnEnabled);
    //}
    //private void OnDisable()
    //{
    //    ShowHideBanner(showOnEnabled);
    //}

    public void ShowHideBanner(bool hide)
    {
        if (hide)
        {
            AdsMediationController.Instance.HideBanner();
        }
        else
        {
            if (showTop)
            {
                Debug.Log("show banner o top");
                AdsMediationController.Instance.ShowBanner(WidogameFoundation.Ads.BannerPosition.Top);
            }
            else if (showBottom)
            {
                Debug.Log("show baner o bottom");
                AdsMediationController.Instance.ShowBanner(WidogameFoundation.Ads.BannerPosition.Bottom);
            }
            else
            {
                Debug.Log("show baner o bottom");
                AdsMediationController.Instance.ShowBanner(WidogameFoundation.Ads.BannerPosition.Bottom);
            }
        }

    }
}
