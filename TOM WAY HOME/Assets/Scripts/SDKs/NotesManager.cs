﻿using System.Collections;
using UnityEngine;
using Melanchall.DryWetMidi.Core;
using Melanchall.DryWetMidi.Interaction;
using System.IO;
using UnityEngine.Networking;
using System.Collections.Generic;
using System;
using Newtonsoft.Json;
using NAudio.CoreAudioApi;
using System.Threading;
//using System.Text;
//using NAudio.Wave;

public enum ObjectType {
    None,
    NormalTile,
    DynamicTile,
    DynamicTileWithCoin,
    DynamicTileWithStar,
    SlideTile,
    Coin,
    CoinEndgame,
    Star,
    PlatformRun,
    FakeTile,
    StaticObs_1,
    StaticObs_2,
    DynamicObs_1,
    DynamicObs_2,
    DynamicObs_3,
}
public enum MoodStyle {
    Blue,
    Yellow,
    Red
}
[Serializable]
public struct NoteData {
    public int noteID;
    public string name;
    public float time;
    public float duration;
    public int lane;
    public int laneAttack;
    public int laneJump;
    public float velocity;
    public float Lenght;
    public float nextNoteDistance;
    public int nextJumpNoteID;
    public ObjectType type;
}
[Serializable]
public struct NoteMoodInfo {
    public float time;
    public MoodStyle mood;
}

public class NotesManager : SingletonDonDestroy<NotesManager>
{
    //public static NotesManager instance;

    public static MidiFile midiFile;

    [SerializeField] private bool useLocalData;
    [SerializeField] private string songconfig;
    [SerializeField] private AudioSource audioSource;
    [HideInInspector] public string songname;
     
    public List<string> songNames;

    public List<NoteMoodInfo> moodChangeTimes = new List<NoteMoodInfo>();
    public List<NoteData> mainTrackNoteDatas;
    public List<NoteData> obsTrackNoteDatas;

    private List<float> listTime = new List<float>();
    private List<float> listDuration = new List<float>();
    private float timeBetween;
    public float songBpm;
    public string soundPath;
    public List<SongData> playlist = new List<SongData>();
    public float speed = 15f;
    public DataHolder dataHolder;
    public float timePreview;

    public SongData TutorialSong;
    private void Start()
    {
        if (useLocalData)
        {
            LoadLocalSongConfig();
        }
        else 
        {
            Debug.LogError(Application.persistentDataPath);
            StartCoroutine(ReadFromWebsite());
        }

        Observer.PlaySong += PlaySongGamePlay;
        Observer.PlaySongBackGround += PlayBackGroundTheme;
    }

    private void LoadLocalSongConfig() {
        playlist = JsonConvert.DeserializeObject<List<SongData>>(songconfig);
        playlist[0].id = -1;
        TutorialSong = playlist[0];
        playlist.RemoveAt(0);
        for (int i = 0; i < playlist.Count; i++)
        {
            playlist[i].id = i;
        }
        SetUpDataSongs();

        if (GameData.IsTutorialMode()) //Go to Tutorial GamePlay
        {
            PopupController.Instance.Show<PopupLoading>();
            if (SplashController.instance != null)
            {
                SplashController.instance.gameObject.transform.parent.gameObject.SetActive(false);
            }
            StartCoroutine(PlayTutorialSong());
        }
        else
        {
            StartCoroutine(SplashController.instance.LoadingHome());
        }
    }
    private IEnumerator ReadFromWebsite()
    {
        using (UnityWebRequest www = UnityWebRequest.Get(GlobalConstants.MUSIC_URL))
        {
            UnityWebRequestAsyncOperation operation = www.SendWebRequest();

            yield return SplashController.instance.LoadingHome(operation);

            if (www.isNetworkError || www.isHttpError) {
                Debug.LogError("Failed to load JSON file");
                LoadLocalSongConfig();
                yield break;
            } else {
                playlist = JsonConvert.DeserializeObject<List<SongData>>(www.downloadHandler.text);
                
                playlist[0].id = -1;
                TutorialSong = playlist[0];
                playlist.RemoveAt(0);
                for (int i = 0; i < playlist.Count; i++)
                {
                    playlist[i].id = i;
                }
                SetUpDataSongs();

                if (GameData.IsTutorialMode()) //Go to Tutorial GamePlay
                {
                    PopupController.Instance.Show<PopupLoading>();
                    if(SplashController.instance != null)
                    {
                        SplashController.instance.gameObject.transform.parent.gameObject.SetActive(false);
                    }
                    StartCoroutine(PlayTutorialSong());
                }
            }
        }
    }
    public void SetUpDataSongs()
    {
        GameData.LoadSongIsDoneWatchAd();
        foreach (var song in playlist)
        {
            if (GameData.SongIsDoneWatchAd.Contains(song.id))
            {
                song.isPlayAds = false;
            }
            else
            {
                if (song.unlock_type == "free")
                {
                    song.isPlayAds = false;
                }
                else if (song.unlock_type == "ads")
                {
                    song.isPlayAds = true;
                }
            }
            song.isDonePlaying = GameData.GetDonePlaySong(song);
            song.spriteUI = dataHolder.spriteList[song.id];
        }
    }
    public void PlaySongGamePlay()
    {
        if (GameData.IsTutorialMode()) return;
        StopAllCoroutines();
        StartCoroutine(PlaySong());
    }

    public void PlayBackGroundTheme()
    {
        StopAllCoroutines();
        StartCoroutine(PlaySongBackGround());
    }

    IEnumerator PlaySong()
    {
        audioSource.Stop();
        foreach (var song in playlist)
        {
            if (song.id == playlist[GameData.CurrentSongIndex].id)
            {
                songname = song.name;
                songBpm = song.BPM;
                speed = song.speed;

                song.isDonePlaying = true;
                song.isClickAds = true;
                GameData.SetSongDonePlayAds(song, true);
                string mp3Path = Path.Combine(useLocalData ? Application.streamingAssetsPath : Application.persistentDataPath, song.name + ".mp3");
                if (useLocalData)
                {
                    StartCoroutine(ReadFromFile(song.name + ".mid"));
                    StartCoroutine(LoadMp3Audio(mp3Path));
                    GameData.SetDonePlaySong(song);
                    yield return null;
                }
                else
                {
                    if (GameData.IsSongDownloaded(song.name))
                    {
                        StartCoroutine(ReadFromFile(song.name + ".mid"));
                        StartCoroutine(LoadMp3Audio(mp3Path));
                        GameData.SetDonePlaySong(song);
                    }
                    else
                    {
                        GameDownloadResources.Instance.DownloadMusic(song);

                        while (!GameData.IsSongDownloaded(song.name))
                            yield return null;
                        StartCoroutine(ReadFromFile(song.name + ".mid"));
                        StartCoroutine(LoadMp3Audio(mp3Path, false, true));
                        GameData.SetDonePlaySong(song);

                    }
                }
            }
        }
    }
    public IEnumerator PlayTutorialSong()
    {
        songname = TutorialSong.name;
        songBpm = TutorialSong.BPM;
        speed = TutorialSong.speed;

        TutorialSong.isDonePlaying = true;
        TutorialSong.isClickAds = true;
        GameData.SetSongDonePlayAds(TutorialSong, true);
        string mp3Path = Path.Combine(useLocalData ? Application.streamingAssetsPath : Application.persistentDataPath, TutorialSong.name + ".mp3");
        if (useLocalData)
        {
            StartCoroutine(ReadFromFile(TutorialSong.name + ".mid"));
            StartCoroutine(LoadMp3Audio(mp3Path));
            GameData.SetDonePlaySong(TutorialSong);
            yield return null;
        }
        else
        {
            if (GameData.IsSongDownloaded(TutorialSong.name))
            {
                StartCoroutine(ReadFromFile(TutorialSong.name + ".mid"));
                StartCoroutine(LoadMp3Audio(mp3Path));
                GameData.SetDonePlaySong(TutorialSong);
            }
            else
            {
                GameDownloadResources.Instance.DownloadMusic(TutorialSong);

                while (!GameData.IsSongDownloaded(TutorialSong.name))
                    yield return null;

                StartCoroutine(ReadFromFile(TutorialSong.name + ".mid"));
                StartCoroutine(LoadMp3Audio(mp3Path, false, true));
                GameData.SetDonePlaySong(TutorialSong);

            }
        }
    }
    public void PlaySongPreview(int id, AudioClip audioClip)
    {
        audioSource.Stop();
        foreach (var song in playlist)
        {
            if (song.id == id)
            {
                audioSource.clip = audioClip;
                audioSource.loop = true;
                audioSource.Play();
            }
        }
    }

    public void StopPlayPreview(int id)
    {
        foreach (var song in playlist)
        {
            if (song.id == id)
            {
                audioSource.Stop();
            }
        }
    }

    public IEnumerator PlaySongBackGround()
    {
        audioSource.Stop();
        foreach (var song in playlist)
        {
            if (song.id == GameData.CurrentSongIndex)
            {
                song.stream_url = song.mp3_path.Replace("uploads/", "");
                string songBackground = GlobalConstants.MUSIC_LINK_STREAM + song.stream_url;
                yield return null;
                StartCoroutine(LoadMp3Audio(songBackground, true));
                //if (useLocalData)
                //{
                //    StartCoroutine(LoadMp3Audio(songBackground, true));

                //    yield return null;
                //}
                //else
                //{
                //    if (Util.IsSongDownloaded(song.name))
                //    {
                //        StartCoroutine(LoadMp3Audio(songBackground, true));
                //    }
                //    else
                //    {
                //        GameDownloadResources.Instance.DownloadMusic();

                //        while (!Util.IsSongDownloaded(song.name))
                //            yield return null;
                //        StartCoroutine(LoadMp3Audio(songBackground, true));
                //    }
                //}
            }
        }
    }

    private IEnumerator ReadFromFile(string filename)
    {
        string filePath = Path.Combine(useLocalData ? Application.streamingAssetsPath : Application.persistentDataPath, filename);
        if (!useLocalData) {
#if !UNITY_EDITOR || UNITY_EDITOR_OSX
       filePath = "file://" + filePath;
#endif
        }

        using (UnityWebRequest www = UnityWebRequest.Get(filePath)) {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError) {
                Debug.LogError("Failed to load midi");
            } else {
                byte[] results = www.downloadHandler.data;
                using (var stream = new MemoryStream(results)) {
                    midiFile = MidiFile.Read(stream);
                    GetDataFromMidi();
                    if (useLocalData) GameData.SetSongMidiDownloaded(songname);
                }
            }
        }
    }
    IEnumerator LoadMp3Audio(string filePath, bool isBackGroundAudio = false, bool newData = false)
    {
        if (!useLocalData && !isBackGroundAudio)
        {
#if !UNITY_EDITOR || UNITY_EDITOR_OSX
       filePath = "file://" + filePath;
#endif
        }

        using (UnityWebRequest audioRequest = UnityWebRequestMultimedia.GetAudioClip(filePath, AudioType.MPEG))
        {
            yield return audioRequest.SendWebRequest();

            if (audioRequest.isHttpError || audioRequest.isNetworkError)
            {
                Debug.LogError("Failed to load mp3");
            }
            else
            {
                AudioClip myClip = DownloadHandlerAudioClip.GetContent(audioRequest);
                if (!isBackGroundAudio) { 
                    audioSource.clip = myClip;
                    audioSource.loop = false;
                    if (useLocalData) GameData.SetSongMp3Downloaded(songname);
                    LoadSceneController.Instance.LoadSceneGamePlay(newData);
                }
                else
                {
                    audioSource.clip = myClip;
                    audioSource.loop = true;
                    audioSource.Play();
                    audioSource.loop = true;
                }    
            }
        }
    }
    private void ResetFileMidi()
    {
        starTimes.Clear();
        moodChangeTimes.Clear();
        mainTrackNoteDatas.Clear();
        obsTrackNoteDatas.Clear();
        listTime.Clear();
        listDuration.Clear();
    }
    private void GetDataFromMidi() {
        ResetFileMidi();
        var tracks = midiFile.GetTrackChunks();
        int count = 0;
        foreach (var track in tracks) {
            count++;
            if (count == 3)
            {
                mainTrackNoteDatas = AddNotes(track);
                SetFirstMood(mainTrackNoteDatas[0]);

                mainTrackNoteDatas = SetTypeNotes(mainTrackNoteDatas);
                mainTrackNoteDatas = RemoveNormalTile(mainTrackNoteDatas);
                mainTrackNoteDatas = CombineNote(mainTrackNoteDatas);
                mainTrackNoteDatas = ConvertCoinNote(mainTrackNoteDatas);
                
                mainTrackNoteDatas = AddEndGameNote(mainTrackNoteDatas);
            } else if (count == 4) {
                listTime.Clear();
                listDuration.Clear();
                obsTrackNoteDatas =  AddNotes(track);
               // obsTrackNoteDatas =  SetObsTypeNotes(obsTrackNoteDatas);
                obsTrackNoteDatas =  EditObsTrackNotes(obsTrackNoteDatas);
            }
        }
    }
    [HideInInspector] public MoodStyle firtMood;
    public void SetFirstMood(NoteData note) {
        if (note.velocity < 10f) //Blue
            firtMood = MoodStyle.Blue;
        else if (note.velocity < 25f)
            firtMood = MoodStyle.Yellow;
        else
            firtMood = MoodStyle.Red;
    }
    public List<NoteData> EditNotesInTrack(List<NoteData> list) {

        for(int i = 0; i < list.Count; i++) {
            NoteData tmp  = new NoteData();
            tmp = list[i];
            tmp.time = list[i].time - GameController.instance.introTime;
            list[i] = tmp;
        }

        return list;
    }
    public float maxTime;
    private List<NoteData> AddNotes(TrackChunk track)
    {
        NoteData noteData = new NoteData();
        var listNote = new List<NoteData>();
        var tempo = midiFile.GetTempoMap();
        foreach (var note in track.GetNotes())
        {

            //time
            float time = note.TimeAs<MetricTimeSpan>(tempo).TotalMicroseconds / 1000000f;
            if (time < maxTime)
                listTime.Add(note.TimeAs<MetricTimeSpan>(tempo).TotalMicroseconds / 1000000f);
            else
                break;

            //duration
            listDuration.Add(note.LengthAs<MetricTimeSpan>(tempo).TotalMicroseconds / 1000000f);
        }

        int count = 0;
        float lasttime = -1f;
        int noteID = -2;

        foreach (var note in track.GetNotes())
        {
            if (count >= listTime.Count) break;

            noteData.time = listTime[count];
            noteData.duration = listDuration[count];
            noteData.velocity = note.Velocity / 1.27f;
            noteData.name = note.NoteName + note.Octave.ToString();
            noteData.lane = GetLineOfNote(noteData.name, note.Length);
            noteData.Lenght = note.Length;

            if (noteData.name == "G7" && noteData.time > 0) { //Add moodchangetimes
                NoteMoodInfo mood = new NoteMoodInfo();
                mood.time = noteData.time;
                if (noteData.velocity < 10f) //Blue
                    mood.mood = MoodStyle.Blue;
                else if (noteData.velocity < 25f)
                    mood.mood = MoodStyle.Yellow;
                else
                    mood.mood = MoodStyle.Red;
                moodChangeTimes.Add(mood);
            }

            timeBetween = 0f;
            if (count < listTime.Count - 1)
            {
                timeBetween = listTime[count + 1] - listTime[count];

                if (timeBetween == 0)
                {
                    for (int i = count + 1; i < listTime.Count; i++)
                        if (listTime[i] > listTime[count])
                        {
                            timeBetween = listTime[i] - listTime[count];
                            break;
                        }
                }
            }
            if (noteData.time != lasttime)
            {
                noteID++;
            }
            noteData.noteID = noteID;


            lasttime = noteData.time;

            noteData.nextNoteDistance = timeBetween;

            listNote.Add(noteData);

            count++;
        }

        return listNote;
    }

    private int GetLineOfNote(string nameofNote, float lenght)
    {     
        switch (nameofNote)
        {
            case "C7":
                return 2;
            case "CSharp7":
                return 1;
            case "D7":
                return 0;
            case "DSharp7":
                return -1;
            case "E7":
                return -2;
            default:
                return 0;
        }
    }

    public List<float> starTimes = new List<float>();
    private List<NoteData> SetTypeNotes(List<NoteData> listNote)
    {
        for(int i = 0; i < listNote.Count; i++)
        {
            NoteData note = new NoteData();
            note = listNote[i];

            if(note.name == "G7") // mooodchange
            {
                listNote.RemoveAt(i);
                i--;
            }
            else if(note.name == "FSharp7")
            {
                note.type = ObjectType.Star;
                int lane_Index = GetNoteSetLaneIndex(i, listNote);

                note.lane = listNote[lane_Index].lane;
                listNote[i] = note;
                starTimes.Add(note.time);
            }
            else if (note.name == "F7") //Coin
            {
                note.type = ObjectType.Coin;
                int lane_Index = GetNoteSetLaneIndex(i, listNote);

                note.lane = listNote[lane_Index].lane;
                listNote[i] = note;
            }
            else if(note.name == "B6") // SlideTile
            {
                note.type = ObjectType.SlideTile;

                int lane_Index = GetNoteSetLaneIndex(i, listNote);

                note.lane = listNote[lane_Index].lane;
                listNote[i] = note;
                listNote.RemoveAt(lane_Index);
                if (lane_Index < i)
                {
                    i--;
                }
            }
            else if(note.name == "ASharp6") //PlatformRun
            {
                note.type = ObjectType.PlatformRun;
                listNote[i] = note;
            }
            else
            {
                if (note.velocity < 25)
                {
                    note.type = ObjectType.DynamicTile;
                }
                else
                {
                    note.type = ObjectType.NormalTile;
                }
                note.nextJumpNoteID = listNote[i].noteID + 1;
                listNote[i] = note;
            }
        }
        return listNote;
    }

    private List<NoteData> RemoveNormalTile(List<NoteData> listNote)
    {
        for(int i = 0; i < listNote.Count; i++)
        {
            if(listNote[i].type == ObjectType.NormalTile)
            {
                for(int j = 0; j < listNote.Count; j++)
                {
                    if (listNote[j].time > listNote[i].time)
                        break;
                    else
                    {
                        if (listNote[j].time == listNote[i].time && (listNote[j].type == ObjectType.PlatformRun || listNote[j].type == ObjectType.SlideTile))
                        {
                            listNote.RemoveAt(i);
                            i--;
                            break;
                        }
                    }
                }
            }
        }
        return listNote;
    }

    private List<NoteData> CombineNote(List<NoteData> listNote)
    {
        for (int i = 0; i < listNote.Count; i++)
        {
            if (listNote[i].type == ObjectType.DynamicTile)
            {
                for (int j = 0; j < listNote.Count; j++)
                {
                    if (listNote[j].time > listNote[i].time)
                        break;
                    else
                    {
                        if (listNote[j].time == listNote[i].time && (listNote[j].type == ObjectType.Coin || listNote[j].type == ObjectType.Star))
                        {
                            if (listNote[j].type == ObjectType.Star) 
                                Debug.LogError(listNote[i].time);
                            NoteData note = new NoteData();
                            note = listNote[i];
                            note.type = listNote[j].type == ObjectType.Coin ? ObjectType.DynamicTileWithCoin : ObjectType.DynamicTileWithStar;
                            listNote[i] = note;

                            listNote.RemoveAt(j);
                            i--;
                            break;
                        }
                    }
                }
            }
        }
        return listNote;
    }


    private List<NoteData> ConvertCoinNote(List<NoteData> listNote)
    {
        float duration;
        for (int i = 0; i < listNote.Count; i++)
        {
            if (listNote[i].name == "B6") // SlideTile
            {
                NoteData slideNote = new NoteData();
                slideNote = listNote[i];

                slideNote.duration = slideNote.duration - 0.2f;
                slideNote.nextNoteDistance = 0.2f;

                duration = slideNote.duration;
                for (int j = i + 1; j < listNote.Count; j++)
                {
                    if (listNote[j].time - listNote[i].time < duration)
                    {
                        if (!IsSpecialNote(listNote[j]))
                        {
                            NoteData note = new NoteData();
                            note = listNote[j];

                            note.type = ObjectType.Coin;
                            listNote[j] = note;
                        }
                    }
                    else
                    {
                        slideNote.nextJumpNoteID = listNote[j].noteID;
                        listNote[i] = slideNote;
                        i = j - 1;
                        break;
                    }
                }
            } 
            else if (listNote[i].name == "ASharp6") //Platform Run
            {
                NoteData note = new NoteData();
                note = listNote[i];

                note.lane = 0;
                note.type = ObjectType.PlatformRun;
                duration = note.duration;

                int countCoin = 0;
                int j;
                for (j = i + 1; j < listNote.Count; j++)
                {
                    if ((listNote[j].time - listNote[i].time) < duration - 0.05f && (listNote[j].time - listNote[i].time) >= 0 && i != j)
                    {
                        if (!IsSpecialNote(listNote[j]))
                        {
                            if (j > i) countCoin++;
                            NoteData coin = new NoteData();
                            coin = listNote[j];
                            coin.type = ObjectType.Coin;
                            listNote[j] = coin;
                        }
                    }
                    else if (j > i)
                    {
                        if (countCoin == 0)
                        {
                            note.nextNoteDistance = note.nextNoteDistance - note.duration;
                        }
                        else
                        {
                            note.nextNoteDistance = listNote[j].time - (listNote[i].time + duration);
                        }

                        note.nextJumpNoteID = listNote[j].noteID;
                        break;
                    }
                }
                listNote[i] = note;
                i = j - 1;
            }
        }
        return listNote;
    }
    private List<NoteData> EditObsTrackNotes(List<NoteData> listNote) {
        for (int i = 0; i < listNote.Count; i++) {
            NoteData _note = new NoteData();
            _note = listNote[i];
            _note.type = GetObsType(listNote[i].velocity);
            if(_note.type == ObjectType.DynamicObs_3) {

                if (i > 0 && listNote[i - 1].type == ObjectType.None) {
                    _note.laneAttack = listNote[i - 1].lane;
                } else {
                    _note.laneAttack = listNote[i + 1].lane;
                    i++;
                }
            } else if(_note.type == ObjectType.DynamicObs_1) {
                //Debug.LogError(i);
                if ((i > 0 && listNote[i - 1].type == ObjectType.None && listNote[i - 1].time == listNote[i].time) || 
                    ( i < listNote.Count - 1 && listNote[i + 1].velocity < 10f && listNote[i + 1].time == listNote[i].time)) {
                    _note.type = ObjectType.DynamicObs_2;
                    if (i < listNote.Count - 1 && listNote[i + 1].velocity < 10f && listNote[i + 1].time == listNote[i].time) {
                        _note.laneJump = listNote[i + 1].lane;
                        i++;
                    } else {
                        _note.laneJump = listNote[i - 1].lane;
                    }
                }
            }
            listNote[i] = _note;
        }
        return listNote;
    }

    [HideInInspector] public int row;
    private List<NoteData> AddEndGameNote(List<NoteData> mainNotes) {
        row = 8;
        float beatTime = 0.15f;

        NoteData lastnote = mainNotes[mainNotes.Count - 1];
        int lastNoteIndex = 0;
        for(int i = mainNotes.Count - 1; i >= 0; i--)
        {
            if (!IsSpecialNote(mainNotes[i]))
            {
                lastNoteIndex = i;
                lastnote = mainNotes[i];
                break;
            }
        }

        for (int i = mainNotes.Count - 1; i >= 0; i--)
        {
            if (mainNotes[i].time == lastnote.time)
            {
                NoteData _note = new NoteData();
                _note = mainNotes[i];
                _note.nextNoteDistance = 1f;
                mainNotes[i] = _note;
            }
        }

        float lastnoteTime = lastnote.time;
        lastnote.nextJumpNoteID = lastnote.noteID + 1;
        lastnote.nextNoteDistance = 1f;
        mainNotes[lastNoteIndex] = lastnote;
        NoteData note = new NoteData();
        note.noteID = lastnote.noteID + 1;
        note.type = ObjectType.PlatformRun;
        note.time = lastnoteTime + 1f;
        note.duration = beatTime * (row + 15);
        mainNotes.Add(note);

        for(int i = 0; i < row; i++)
            for(int j = 0; j < 5; j++) {
                note.type = ObjectType.CoinEndgame;
                note.time = lastnoteTime + 1f + i * beatTime;
                note.lane = j - 2;

                mainNotes.Add(note);
            }

        return mainNotes;
    }

    private ObjectType GetObsType(float velocity) {
        if(velocity < 10f) {
            return ObjectType.None;
        } else if(velocity > 10f && velocity < 25f) {
            return ObjectType.FakeTile;
        } else if(velocity > 25f && velocity < 50f) {
            return ObjectType.StaticObs_1;
        } else if (velocity > 50f && velocity < 75f) {
            return ObjectType.StaticObs_2;
        } else if (velocity > 75f && velocity < 90f) {
            return ObjectType.DynamicObs_1;
        } else {
            return ObjectType.DynamicObs_3;
        }
    }

    private int GetNoteSetLaneIndex(int index, List<NoteData> listNote)
    {
        for(int i = 0; i < listNote.Count; i++)
        {
            if(i != index && listNote[i].time == listNote[index].time && !IsSpecialNote(listNote[i]))
            {
                return i;
            }
        }
        return 0;
    }
    private bool IsSpecialNote(NoteData note)
    {
        return note.name == "G7" || note.name == "FSharp7" || note.name == "ASharp6" || note.name == "F7"; 
    }

    public void PlayMusic(float time = 0)
    {
        if (PlayerPrefs.GetInt(GlobalConstants.MUSIC_ON_KEY, 1) == 1 && GameController.instance.gameStatus != GameStatus.DIE)
        {
            time += RemoteConfig.instance.fixLatency;

            audioSource.time = time;
            audioSource.Play();
        }
    }

    public void StopMusic()
    {
        audioSource.Stop();
    }
    public void PauseMusic() {
        GameController.instance.timePause = audioSource.time;
        audioSource.Pause();
    }
    public static string StreamingAssetPath()
    {
#if UNITY_EDITOR
        return Application.dataPath + "/StreamingAssets/";
#endif
#if UNITY_ANDROID
        return "jar:file://" + Application.dataPath + "!/assets/";
#endif
#if UNITY_IOS
        return "file://" + Application.dataPath + "/Raw/";
#endif
        throw new System.NotImplementedException("Check the ifdefs above.");
    }
}


