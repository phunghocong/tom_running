﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class StringConstants {

    public const string RC_INTERSTITIAL_FROM_STARTUP_SECONDS = "interstitial_from_startup_seconds";
    public const string RC_INTERSTITIAL_INTERVAL_SECONDS = "interstitial_interval_seconds";
    public const string PREF_INTERSTITIAL_LAST_SHOWN = "interstitial_last_shown";
    public const string RC_TIME_PER_WATCH_ADS = "time_per_watch_ads";
    public const string RC_LEVEL_FIRST_SHOW_INTER = "level_first_show_inter";
    public const string RC_LEVEL_SHOW_RATE_INTERVAL = "level_show_rate_interval";
    public const string RC_PER_LEVEL_SHOW_KEEP_WATCH_ADS = "per_level_show_keep_watch_ads";
    public const string RC_STARS_TO_UNLOCK_NEW_WORLDS = "stars_to_unlock_new_worlds";
    public const string RC_TIME_REFILL_LIFE_SECONDS = "time_refill_life_seconds";
    public const string RC_LEVEL_SHOW_RATE = "level_show_rate";
    public const string RC_KEYS_SHOW_TREASURE_CHEST = "keys_show_treasure_chest";
    public const string RC_COINS_DOUBLE_DAMAGE = "coins_double_damage";
    public const string RC_COINS_DOUBLE_HEALTH = "coins_double_health";
    public const string RC_SHOULD_ASK_ATT = "should_ask_att";
    public const string RC_FREE_COIN_REWARD = "free_coin_reward";
    public const string RC_SILENT_ADS_INTERVAL_SECONDS = "silent_ads_interval_seconds";
    public const string RC_SILENT_ADS_ENABLED = "silent_ads_enabled";
    public const string RC_VIP_WEEKLY_TEXT_ATTRACTIVE_ENABLED = "vip_weekly_text_attractive_enabled";
    public const string RC_VIP_NO_SUBS_FIRST_DAY_SUGGESTION_FIRST_DAY = "vip_no_subs_first_day_suggestion_enabled";
    public const string RC_ENABLE_GET_MORE_LIFE_REFILL = "enable_get_more_life_refill";
    public const string RC_TOTAL_NUMBER_SHOW_GET_MORE_LIFE_REFILL = "total_number_show_get_more_life_refill";
    public const string RC_GOLDEN_TIME = "golden_time";
    public const string RC_GOLDEN_TIME_ENABLED = "golden_time_enabled";
    public const string RC_TOTAL_TIME_UNLIMTED_LIFE_SECONDS = "total_time_unlimted_life_seconds";
    public const string RC_GOLDEN_TIME_DURATION_SECONDS = "golden_time_duration_seconds1";
    public const string RC_INTERSTITIAL_ON_START_LEVEL_ENABLED = "interstitial_on_start_level_enabled";
    public const string RC_LEVEL_FINISH_TUT = "level_finish_tut";
    public const string RC_FIRST_LEVEL_UNLIMITED_LIFE_IN_DAY = "first_level_unlimited_life_in_day";
    public const string RC_ENABLE_LIFE_ADS_LEVEL2 = "enable_life_ads_level2";
    public const string RC_ASK_ATT_LEVEL_INTERVAL = "ask_att_level_interval";
    public const string RC_ASK_ATT_FIRST_LEVEL = "ask_att_first_level";
    public const string RC_INTER_REWARDED_INTERVAL_ENABLED = "inter_rewarded_interval_enabled";
    public const string RC_SHOW_SILENT_ADS_FROM_LEVEL = "show_silent_ads_from_level";
    public const string RC_BEGINNER_INTERSTITIAL_INTERVAL_SECONDS = "beginner_interstitial_interval_seconds";
    public const string RC_BEGINNER_MAX_LEVEL = "beginner_max_level";
    public const string RC_REVIVE_COIN_ENABLED = "revive_coin_enabled";
    public const string RC_DISABLE_INTERSTITIAL_ON_LOW_RAM_DEVICE = "disable_interstitial_on_low_ram_device";
    public const string RC_ENABLE_CLICK_CHEST_4 = "enable_click_chest_4";
    public const string RC_ENABLE_EVENT_HALLOWEEN = "enable_event_halloween";
    public const string RC_END_DATE_HALLOWEEN = "end_date_halloween";
    public const string RC_ENABLE_NOEL = "enable_noel";
    public const string RC_END_DATE_NOEL = "end_date_noel";
    public const string RC_LEVEL_UNLOCK_EVENT_NOEL = "level_unlock_event_noel";
    public class KEY {
        public const string LEVEL = "level";
        public const string TOTAL_COINS = "total_coins";
        public const string NEW_PLAYER = "new_player";
        public const string REMOVE_ADS = "remove_ads";
        public const string COINS = "coins";
        public const string MUTE_AUDIO = "mute_audio";
        public const string BG_GAME = "bg_game";
        public const string RATE_GAME = "rate_game";
    }
}

