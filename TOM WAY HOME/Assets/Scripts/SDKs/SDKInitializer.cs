﻿using Firebase.Analytics;
using Firebase.Crashlytics;
using GoogleMobileAds.Api;
using Kochava;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using WidogameFoundation.Ads;

public class SDKInitializer : MonoBehaviour
{
    public static SDKInitializer instance;

    public bool isFirebaseInitialized;

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSplashScreen)]
    public static void InitAppOpenAd() {
#if !ENV_PROD
            List<string> testDeviceIds = new List<string>();
            RequestConfiguration configuration = new RequestConfiguration.Builder().SetTestDeviceIds(testDeviceIds).build();
            MobileAds.SetRequestConfiguration(configuration);
#endif
            //AppOpenAdManager.Instance.LoadAd(ScreenOrientation.Portrait);
    }

    // Start is called before the first frame update
    private void Awake() {
        instance = this;
        InitializeFirebase();
        //InitializeFacebook();        
    }

    private void Start() {
        StartCoroutine(InitializeFirebaseMessaging());
        AdsMediationController.Instance.InitRewardedAds();
        AdsMediationController.Instance.InitInterstitial();
    }

    private void InitializeFirebase() {
        if (!isFirebaseInitialized) {
            Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
                if (task.Result == Firebase.DependencyStatus.Available) {
                    Firebase.FirebaseApp app = Firebase.FirebaseApp.DefaultInstance;
                    isFirebaseInitialized = true;
                    GameAnalytics.IsFirebaseInitialized = true;
                }
                else {
                    Debug.LogError("Could not resolve all Firebase dependencies: " + task.Result);
                }
                InitializeFirebaseComponents();
            });
        } else {
            isFirebaseInitialized = true;
            GameAnalytics.IsFirebaseInitialized = true;
            InitializeFirebaseComponents();
        }
    }

    private void InitializeFirebaseComponents()
    {
        Crashlytics.IsCrashlyticsCollectionEnabled = true;
        RemoteConfigManager.InitializeRemoteConfig();
        SetupSessionTimeCount();
    }
    private void SetupSessionTimeCount()
    {
        SDKLogsPrefs.SessionID++;
        if (SDKLogsPrefs.firstOpen)
        {
            SDKLogsPrefs.firstOpen = false;
            SDKLogsPrefs.firstOpenTime = DateTime.Now;
        }
        else
        {
            Dictionary<string, string> param = new Dictionary<string, string>();
            param.Add("session_id", SDKLogsPrefs.SessionID.ToString());
        }
        var deltaTime = DateTime.Now - SDKLogsPrefs.firstOpenTime;
        if (SDKLogsPrefs.DayFromInstall != (int)deltaTime.TotalDays)
        {
            SDKLogsPrefs.EngageDay++;
        }
        SDKLogsPrefs.DayFromInstall = (int)deltaTime.TotalDays;
        GameAnalytics.LogFirebaseUserProperty("session_id", $"ss{SDKLogsPrefs.SessionID}:dfi{SDKLogsPrefs.DayFromInstall}:d{SDKLogsPrefs.EngageDay}");

    }
    private IEnumerator InitializeFirebaseMessaging()
    {
        while (!isFirebaseInitialized)
        {
            if (Time.realtimeSinceStartup > 5.0f)
            {
                break;
            }
            yield return null;
        }
        if (isFirebaseInitialized)
        {
#if ENV_LOG
                    Debug.Log("Initializing Firebase Messaging");
#endif
            Firebase.Messaging.FirebaseMessaging.TokenReceived += FirebaseMessaging_TokenReceived;
            Firebase.Messaging.FirebaseMessaging.MessageReceived += FirebaseMessaging_MessageReceived;
        }
    }

    private void FirebaseMessaging_MessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e) {
    }

    private void FirebaseMessaging_TokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs e) {
#if ENV_LOG
            Debug.Log("FirebaseMessaging token: " + e.Token);
#endif
    }




    private void InitDynamicLink()
    {
    }

    //private void InitializeFacebook() {
    //    GameAnalytics.isFBInitialized = true;
    //}

    //    private void InitializeAppsFlyer() {
    //        AppsFlyer.setAppsFlyerKey(GameSDKSettings.appsflyerKey);
    //#if UNITY_ANDROID
    //        AppsFlyer.init(GameSDKSettings.appsflyerKey, "AppsFlyerTrackerCallbacks");
    //#elif UNITY_IOS
    //        AppsFlyer.setAppID(GameSDKSettings.appleAppId);
    //        AppsFlyer.getConversionData();
    //        AppsFlyer.trackAppLaunch();
    //#endif
    //    }

    //private void OnHideUnity(bool isUnityShown) {
    //    if (!isUnityShown) {
    //        Time.timeScale = 0;
    //    } else {
    //        Time.timeScale = 1;
    //    }
    //}

    //private void InitCallback() {
    //    GameAnalytics.isFBInitialized = true;
    //}


}
