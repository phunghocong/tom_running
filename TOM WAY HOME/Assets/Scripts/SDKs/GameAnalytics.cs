﻿using Firebase.Analytics;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class GameAnalytics {

    public static bool isFBInitialized;
    private static bool isFirebaseInitialized;
    private static UnityEvent onFirebaseAnalyticsInitialized = new UnityEvent();
    public const string EVENT_LEVEL_START = "level_start";
    public const string EVENT_LEVEL_END = "level_end";

    public const string EVENT_WATCH_REWARD_ADS = "watch_reward_ads";
    public const string EVENT_WATCH_REWARD_ADS_DONE = "watch_reward_ads_done";
    public const string EVENT_INTERSTITIAL_ADS_IMPRESSION = "interstitial_ads_impression";
    public const string EVENT_INTERSTITIAL_ADS_IMPRESSION_DONE = "interstitial_ads_impression_done";
    public const string EVENT_BUTTON_CLICK = "button_click";
    public const string EVENT_SPEND_VIRTUAL_CURRENCY = "spend_virtual_currency";
    public const string EVENT_GET_ITEM = "get_item";
    public const string EVENT_SPEND_ITEM = "spend_item";
    public const string EVENT_LUCKY_SPIN = "lucky_spin";
    public const string EVENT_BUY_SHOP_CHARACTER = "buy_in_shop_character";
    public const string EVENT_UPGRADE_POWER = "upgrade_power";
    public const string EVENT_CLICK_BUTTON_UPGRADE_POWER = "click_button_upgrade_power";
    public const string EVENT_OPEN_CHEST = "open_chest";
    public const string EVENT_EARN_VIRTUAL_CURRENCY = "earn_virtual_currency";
    public const string EVENT_REVIVE = "revive";
    public const string EVENT_PLAYER_DIE = "player_die";
    public const string EVENT_ATT_REQUEST = "att_request";


    public const string USER_PROPERTIES_LEVEL = "level";
    public const string USER_PROPERTIES_COLLECTED_COINS = "collected_coins";
    public const string USER_PROPERTIES_IAP_USER = "iap_user";
    public const string EVENT_UI_APPEAR = "ui_appear";
    public const string EVENT_LOADING_FINISHED = "loading_finished";
    public const string EVENT_GAMEPLAY_LOADING_FINISHED = "gameplay_loading_finished";

#if UNITY_IOS
    public const string ADJUST_ET_LEVEL_START = "npftd5";
    public const string ADJUST_ET_LEVEL_PASSED = "8ags0j";
    public const string ADJUST_ET_LEVEL_FAILED = "mde2pp";
    public const string ADJUST_ET_IN_APP_PURCHASE = "43ygfj";

#else
    public const string ADJUST_ET_LEVEL_START = "5wlweu";
    public const string ADJUST_ET_LEVEL_PASSED = "3doucw";
    public const string ADJUST_ET_LEVEL_FAILED = "15r7ch";
    public const string ADJUST_ET_IN_APP_PURCHASE = "xejy0r";
#endif


    public static bool IsFirebaseInitialized {
        get {
            return isFirebaseInitialized;
        }
        set {
            isFirebaseInitialized = value;
            if (value) {
                onFirebaseAnalyticsInitialized.Invoke();
                onFirebaseAnalyticsInitialized.RemoveAllListeners();
            }
        }
    }


    //public static void LogUIAppear(string name) {
    //    if (FB.IsInitialized) {
    //        Dictionary<string, object> paramDict = new Dictionary<string, object>();
    //        paramDict["name"] = name;
    //        LogEventFacebook(EVENT_UI_APPEAR, paramDict);
    //    }
    //    Parameter[] firebaseParams = new Parameter[1];
    //    firebaseParams[0] = new Parameter("name", name);
    //    LogEventFirebase(EVENT_UI_APPEAR, firebaseParams);
    //}

    public static void LogLeveStart(int level, string start_from,bool isUsingMagnet, bool isUsingBonusTime, bool isRestart)
    {
        if (SDKInitializer.instance.isFirebaseInitialized)
        {
            Parameter[] param = new Parameter[5];
            param[0] = new Parameter("level", level);
            param[1] = new Parameter("from", start_from);
            param[2] = new Parameter("using_booster_magnet", isUsingMagnet.ToString());
            param[3] = new Parameter("using_booster_bonustime", isUsingBonusTime.ToString());
            param[4] = new Parameter("is_restart", isRestart.ToString());
            LogEventFirebase("level_start",param);
        }
    }

    public static void LogLevelEnd(int level, bool isPassed, float timeSpent, float duration, int totalHint, int totalReturn, int totalFreeze, int totalPinWheel, int outOfTime, int outOfBox)
    {
        if (SDKInitializer.instance.isFirebaseInitialized)
        {
            Parameter[] param = new Parameter[11];
            param[0] = new Parameter("level",level);
            param[1] = new Parameter("pass", isPassed.ToString());
            param[2] = new Parameter("time_spent", timeSpent);
            param[3] = new Parameter("duration", duration);
            param[4] = new Parameter("duration", duration);
            param[5] = new Parameter("using_total_booster_hint", duration);
            param[6] = new Parameter("using_total_booster_return", duration);
            param[7] = new Parameter("using_total_booster_freeze", duration);
            param[8] = new Parameter("using_total_booster_pinwheel", duration);
            param[9] = new Parameter("out_of_time", outOfTime);
            param[10] = new Parameter("out_of_box", outOfBox);
            LogEventFirebase("level_end", param);
        }
    }

    public static void LogLevelFailed(int level, string loseMethod) //show right after failed, before level end
    {
        Parameter[] param = new Parameter[2];
        param[0] = new Parameter("level", level);
        param[1] = new Parameter("method", loseMethod);
        LogEventFirebase("level_failed", param);
    }

    public static void LogRevive(int level, string reviveMethod )
    {
        Parameter[] param = new Parameter[2];
        param[0] = new Parameter("level", level);
        param[1] = new Parameter("method", reviveMethod);
        LogEventFirebase("level_revive", param);
    }

    public static void LogLevelRestart(int level, string restartMethod)
    {
        Parameter[] param = new Parameter[2];
        param[0] = new Parameter("level", level);
        param[1] = new Parameter("method", restartMethod);
        LogEventFirebase("level_restart", param);

    }

    public static void LogWatchRewardAds(string scenarios, bool hasVideo) {
        bool internetAvailable = Application.internetReachability != NetworkReachability.NotReachable;
        Parameter[] firebaseParams = new Parameter[3];
        firebaseParams[0] = new Parameter("placement", scenarios);
        firebaseParams[1] = new Parameter("has_ads", hasVideo.ToString());
        firebaseParams[2] = new Parameter("internet_available", internetAvailable.ToString());
        //firebaseParams[3] = new Parameter("network", AdsMediationController.Instance.LoadedRewardedNetwork);
        LogEventFirebase(EVENT_WATCH_REWARD_ADS, firebaseParams);

    }
    public static void LogWatchRewardAdsDone(string scenarios,bool hasVideo,bool internetAvailable,string result)
    {
         internetAvailable = Application.internetReachability != NetworkReachability.NotReachable;
        Parameter[] firebaseParams = new Parameter[3];
        firebaseParams[0] = new Parameter("placement", scenarios);
        firebaseParams[1] = new Parameter("has_ads", hasVideo.ToString());
        firebaseParams[2] = new Parameter("internet_available", internetAvailable.ToString());
        //firebaseParams[3] = new Parameter("network", AdsMediationController.Instance.LoadedInterstitialNetwork);

        firebaseParams[4] = new Parameter("result", result.ToString());
        LogEventFirebase(EVENT_WATCH_REWARD_ADS_DONE, firebaseParams);
    }
   
    public static void InterstitialAdsImpression(string scenarios, bool hasVideo, bool internet) 
    {
        Parameter[] firebaseParams = new Parameter[3];
        firebaseParams[0] = new Parameter("placement", scenarios);
        firebaseParams[1] = new Parameter("has_ads", hasVideo.ToString());
        firebaseParams[2] = new Parameter("internet_available", internet.ToString());
        //firebaseParams[3] = new Parameter("network", AdsMediationController.Instance.LoadedInterstitialNetwork);
        //firebaseParams[4] = new Parameter("should_show",shoudShow.ToString());
        LogEventFirebase(EVENT_INTERSTITIAL_ADS_IMPRESSION, firebaseParams);
       
    }
    public static void InterstitialAdsImpressionDone(string scenarios, bool hasVideo, bool internet)
    {
        Parameter[] firebaseParams = new Parameter[3];
        firebaseParams[0] = new Parameter("placement", scenarios);
        firebaseParams[1] = new Parameter("has_ads", hasVideo.ToString());
        firebaseParams[2] = new Parameter("internet_available", internet.ToString());
        //firebaseParams[3] = new Parameter("network", AdsMediationController.Instance.LoadedInterstitialNetwork);
        //firebaseParams[4] = new Parameter("result", result.ToString()); 
        LogEventFirebase(EVENT_INTERSTITIAL_ADS_IMPRESSION_DONE, firebaseParams);
        //LogFirebaseUserProperty("last_scenarios", scenarios);
        //AdjustEvent adjustEvent = new AdjustEvent(EVENT_INTERSTITIAL_ADS_IMPRESSION);
        //adjustEvent.addCallbackParameter("scenarios", scenarios);
        //adjustEvent.addCallbackParameter("has_video", hasVideo.ToString());
        //adjustEvent.addCallbackParameter("internet_available", internet.ToString());
        //Adjust.trackEvent(adjustEvent);
    }
    public static void LogButtonClick(string screen, string button)
    {
        Parameter[] firebaseParams = new Parameter[2];
        firebaseParams[0] = new Parameter("name", button);
        firebaseParams[1] = new Parameter("screen_name", screen);
        LogEventFirebase(EVENT_BUTTON_CLICK, firebaseParams);

        //AdjustEvent adjustEvent = new AdjustEvent(EVENT_BUTTON_CLICK);
        //adjustEvent.addCallbackParameter("name", button);
        //adjustEvent.addCallbackParameter("screen_name", screen);
        //Adjust.trackEvent(adjustEvent);
    }

    public static void LogLuckySpin(string getItem, string methodSpin, int sessionSpin)
    {
        Parameter[] firebaseParams = new Parameter[3];
        firebaseParams[0] = new Parameter("get_item", getItem);
        firebaseParams[1] = new Parameter("method", methodSpin);
        firebaseParams[2] = new Parameter("session_spin", sessionSpin);
        LogEventFirebase("lucky_spin", firebaseParams);
    }



    public static void LogUseCoin(string placement, float price, string itemName, int Amount)
    {
        Parameter[] param = new Parameter[4];
        param[0] = new Parameter("placement", placement);
        param[1] = new Parameter("price", price);
        param[2] = new Parameter("item_name", itemName);
        param[3] = new Parameter("amount", Amount);
        LogEventFirebase("use_coin", param);
    }

    public static void LogGetItem(string itemName, float price, int amount, string method, int numOfAds, int numOfTotalAds, string screenName, string placement)
    {
        Parameter[] param = new Parameter[8];
        param[0] = new Parameter("item_name", itemName);
    }

    public static void LogUseItem(string itemName, string method, string screenName, string placement)
    {
        Parameter[] param = new Parameter[4];
        param[0] = new Parameter("item_name", itemName);
        param[1] = new Parameter("method", method);
        param[2] = new Parameter("screen_name", screenName);
        param[3] = new Parameter("placement", placement);
        LogEventFirebase("user_item", param);
    }

    public static void LogUIAppear(string screenName)//, string name)
    {
        Parameter[] param = new Parameter[1];
        param[0] = new Parameter("screen_name", screenName);
        //param[1] = new Parameter("name", name);
        LogEventFirebase("ui_appear", param);
    }

    public static void LogNoitify(string name, bool isSuccessDisplay)
    {
        Parameter[] param = new Parameter[2];
        param[0] = new Parameter("name", name);
        param[1] = new Parameter("success_displaying", isSuccessDisplay.ToString());
        LogEventFirebase("noiti_receive", param);
    }

    public static void LogNoitiOpen(string name)
    {
        Parameter[] param = new Parameter[1];
        param[0] = new Parameter("name", name);
        LogEventFirebase("noiti_open", param);
    }







#region Old_Code
    //public static void LogScenarios(string screen)
    //{
    //    if (FB.IsInitialized)
    //    {
    //        Dictionary<string, object> paramDict = new Dictionary<string, object>();
    //        paramDict["screen_name"] = screen;
    //    }
    //    Parameter[] firebaseParams = new Parameter[1];
    //    firebaseParams[0] = new Parameter("screen_name", screen);


    //}

    //public static void LogSpendVirtualCurrency(string item, ResourceType resourceType, int value, string eventNoel = "") {
    //    //if (FB.IsInitialized) {
    //    //    Dictionary<string, object> paramDict = new Dictionary<string, object>();
    //    //    paramDict["item"] = item;
    //    //    paramDict["value"] = value;
    //    //    paramDict["virtual_currency_name"] = resourceType.ToString();
    //    //    LogEventFacebook(EVENT_SPEND_VIRTUAL_CURRENCY, paramDict);
    //    //}
    //    Parameter[] firebaseParams = new Parameter[3];
    //    firebaseParams[0] = new Parameter("item", item);
    //    firebaseParams[1] = new Parameter("virtual_currency_name", resourceType.ToString());
    //    firebaseParams[2] = new Parameter("value", value);
    //    LogEventFirebase(eventNoel + EVENT_SPEND_VIRTUAL_CURRENCY, firebaseParams);

    //    //AdjustEvent adjustEvent = new AdjustEvent(EVENT_SPEND_VIRTUAL_CURRENCY);
    //    //adjustEvent.addCallbackParameter("item", item);
    //    //adjustEvent.addCallbackParameter("virtual_currency_name", resourceType.ToString());
    //    //adjustEvent.addCallbackParameter("value", value.ToString());
    //    //Adjust.trackEvent(adjustEvent);
    //}
    //public static void LogSpendItem(string placement, ResourceItem[] resourceItems, string method, int amount)
    //{
    //    foreach (ResourceItem resourceItem in resourceItems)
    //    {
    //        if (resourceItem.countable)
    //        {
    //            LogSpendItem(placement,resourceItem.type, method,amount);
    //        }
    //    }
    //}
    //public static void LogSpendItem(string placement, ResourceType resourceType, string method,int amount)
    //{
    //    Parameter[] firebaseParams = new Parameter[4];
    //    firebaseParams[0] = new Parameter("placement", placement);
    //    firebaseParams[1] = new Parameter("name_received_item", resourceType.ToString());
    //    firebaseParams[2] = new Parameter("method", method);
    //    firebaseParams[3] = new Parameter("amount", amount);
    //    LogEventFirebase(EVENT_SPEND_ITEM, firebaseParams);


    //}
    //public static void LogSpinLucky(int resetth,int fee, int sessionSpin, double amount, params ResourceItem[] resourceItems)
    //{
    //    foreach (ResourceItem resourceItem in resourceItems)
    //    {
    //        if (resourceItem.countable)
    //        {
    //            LogSpinLucky(resetth, fee, sessionSpin,amount,  resourceItem.type, resourceItem.quantity.ToString());
    //        }
    //    }
    //}
    //public static void LogSpinLucky(int resetth,int fee, int sessionSpin, double amount, List<ResourceItem> resourceItems, string eventNoel = "")
    //{
    //    LogSpinLucky(resetth,fee,sessionSpin,amount, resourceItems.ToArray());
    //}
    //public static void LogSpinLucky(int resetth,int fee, int sessionSpin, double amount, ResourceType resourceType,string eventNoel = "")
    //{
    //    //if (FB.IsInitialized) {
    //    //    Dictionary<string, object> paramDict = new Dictionary<string, object>();
    //    //    paramDict["location"] = location;
    //    //    paramDict["virtual_currency_name"] = resourceType.ToString();
    //    //    paramDict["value"] = value;
    //    //    LogEventFacebook(EVENT_EARN_VIRTUAL_CURRENCY, paramDict);
    //    //}
    //    Parameter[] firebaseParams = new Parameter[5];
    //    firebaseParams[0] = new Parameter("resetth", resetth);
    //    firebaseParams[1] = new Parameter("fee", fee);
    //    firebaseParams[2] = new Parameter("session_spin", sessionSpin);
    //    firebaseParams[3] = new Parameter("amount", amount);

    //    firebaseParams[4] = new Parameter("received_item", resourceType.ToString());

    //    LogEventFirebase(EVENT_LUCKY_SPIN, firebaseParams);


    //    //AdjustEvent adjustEvent = new AdjustEvent(EVENT_EARN_VIRTUAL_CURRENCY);
    //    //adjustEvent.addCallbackParameter("location", location);
    //    //adjustEvent.addCallbackParameter("virtual_currency_name", resourceType.ToString());
    //    //adjustEvent.addCallbackParameter("value", value.ToString());
    //    //Adjust.trackEvent(adjustEvent);
    //}
    //public static void LogGetItem(ResourceItem[] resourceItems, double amount, string method, string placement, string nameButton)
    //{
    //    foreach (ResourceItem resourceItem in resourceItems)
    //    {
    //        if (resourceItem.countable)
    //        {
    //            LogGetItem(resourceItem.type, resourceItem.quantity,method,placement,nameButton);
    //        }
    //    }
    //}
    //public static void LogGetItem(List<ResourceItem> resourceItems, double amount, string method, string placement, string nameButton)
    //{
    //    LogGetItem( resourceItems.ToArray(), amount,  method, placement,  nameButton);
    //}
    //public static void LogGetItem(ResourceType resourceType, double amount,string method,string placement,string nameButton)
    //{
    //    Parameter[] firebaseParams = new Parameter[5];
    //    firebaseParams[0] = new Parameter("name_item", resourceType.ToString());
    //    firebaseParams[1] = new Parameter("amount", amount);
    //    firebaseParams[2] = new Parameter("method", method);
    //    firebaseParams[3] = new Parameter("placement", placement);
    //    firebaseParams[4] = new Parameter("name_button", nameButton);
    //    LogEventFirebase(EVENT_GET_ITEM, firebaseParams);

    //}


    //public static void LogEarnVirtualCurrency(string location,params ResourceItem[] resourceItems) {
    //    foreach (ResourceItem resourceItem in resourceItems) {
    //        if (resourceItem.countable) {
    //            LogEarnVirtualCurrency(location, resourceItem.type, resourceItem.quantity);
    //        }
    //    }
    //}



    //public static void LogEarnVirtualCurrency(string location,  List<ResourceItem> resourceItems, string eventNoel = "") {
    //    LogEarnVirtualCurrency(location, resourceItems.ToArray());
    //}

    //public static void LogEarnVirtualCurrency(string location, ResourceType resourceType, int value, string eventNoel = "") {
    //    //if (FB.IsInitialized) {
    //    //    Dictionary<string, object> paramDict = new Dictionary<string, object>();
    //    //    paramDict["location"] = location;
    //    //    paramDict["virtual_currency_name"] = resourceType.ToString();
    //    //    paramDict["value"] = value;
    //    //    LogEventFacebook(EVENT_EARN_VIRTUAL_CURRENCY, paramDict);
    //    //}
    //    Parameter[] firebaseParams = new Parameter[3];
    //    firebaseParams[0] = new Parameter("location", location);
    //    firebaseParams[1] = new Parameter("virtual_currency_name", resourceType.ToString());
    //    firebaseParams[2] = new Parameter("value", value);
    //    LogEventFirebase(eventNoel + EVENT_EARN_VIRTUAL_CURRENCY, firebaseParams);

    //    //AdjustEvent adjustEvent = new AdjustEvent(EVENT_EARN_VIRTUAL_CURRENCY);
    //    //adjustEvent.addCallbackParameter("location", location);
    //    //adjustEvent.addCallbackParameter("virtual_currency_name", resourceType.ToString());
    //    //adjustEvent.addCallbackParameter("value", value.ToString());
    //    //Adjust.trackEvent(adjustEvent);
    //}

    //public static void LogInAppPurchase(string screen , UnityEngine.Purchasing.Product product,string product_id) {
    //    AdjustEvent adjustEvent = new AdjustEvent(ADJUST_ET_IN_APP_PURCHASE);
    //    adjustEvent.setRevenue((double)product.metadata.localizedPrice, product.metadata.isoCurrencyCode);
    //    adjustEvent.setTransactionId(product.transactionID);
    //    Adjust.trackEvent(adjustEvent);
    //    if (FB.IsInitialized)
    //    {
    //        Dictionary<string, object> paramDict = new Dictionary<string, object>();
    //        paramDict["screen_name"] = screen;
    //        paramDict["product_id"] = product_id;
    //    }
    //    Parameter[] firebaseParams = new Parameter[1];
    //    firebaseParams[0] = new Parameter("screen_name", screen);
    //    firebaseParams[0] = new Parameter("product_id", product_id);
    //}
#endregion

    public static void LogEventFirebase(string eventName, Parameter[] parameters) {
#if !ENV_PROD
        Array.Resize<Parameter>(ref parameters, parameters.Length + 1);
        parameters[parameters.Length - 1] = new Parameter("test", true.ToString());
#endif
        if (isFirebaseInitialized) {
            FirebaseAnalytics.LogEvent(eventName, parameters);
        } else {
            onFirebaseAnalyticsInitialized.AddListener(() => {
                FirebaseAnalytics.LogEvent(eventName, parameters);
            });
        }
    }

    public static void LogEventFacebook(string eventName, Dictionary<string, object> parameters) {
        if (isFBInitialized) {
//#if !ENV_PROD
//            parameters["test"] = true;
//#endif
//            FB.LogAppEvent(eventName, null, parameters);
        }
    }
    public static void LogFirebaseUserProperty(string userProperty, object value, string eventNoel = "") {
#if ENV_PROD
        if (isFirebaseInitialized) {
            FirebaseAnalytics.SetUserProperty(eventNoel + userProperty, value.ToString());
        } else {
            onFirebaseAnalyticsInitialized.AddListener(() => {
                FirebaseAnalytics.SetUserProperty(eventNoel + userProperty, value.ToString());
            });
        }
#endif

    }

    public static void LogAdsRevenue(string eventName,float revenue)
    {
        Parameter[] firebaseParams = new Parameter[1];
        firebaseParams[0] = new Parameter("value", revenue);
        LogEventFirebase(eventName, firebaseParams);
    }

}
