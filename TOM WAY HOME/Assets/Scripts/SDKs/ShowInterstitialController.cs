﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ShowInterstitialController : MonoBehaviour {
    public UnityEvent onFinished;
    public string scenariosInter;
    public string placement;
    private float lastTimescale;
    private static DateTime lastInterstitialShown;

    public virtual void Show() {
        Debug.Log("show Inter");
        if (ShouldShowInterstitial) {
            bool interstitialReady = AdsMediationController.Instance.IsInterstitialAvailable;
            Debug.Log("bat dau show inter" + interstitialReady);
            if (interstitialReady) {
                Debug.Log("show inter ads vao thoi diem nay");
                lastInterstitialShown = DateTime.Now;
                AudioHelper.Instance.MuteSound();
                AudioHelper.Instance.MutePlaylist();
                lastTimescale = Time.timeScale;
                Time.timeScale = 0;
                GameData.showAds = true;
                AdsMediationController.Instance.ShowInterstitial(placement, InterClosed);
            } else {
                CannotShow();
            }
        } else {
            CannotShow();
        }
    }

    private bool ShouldShowInterstitial
    {
        get
        {
            //DateTime lastShown = SDKPlayPrefs.GetDateTime(StringConstants.PREF_INTERSTITIAL_LAST_SHOWN, DateTime.Now.Subtract(TimeSpan.FromDays(1)));
            //long intervalSeconds = RemoteConfigManager.GetLong(StringConstants.RC_INTERSTITIAL_INTERVAL_SECONDS);

            long intervalSeconds = 10;
            if (Math.Abs((DateTime.Now - lastInterstitialShown).TotalSeconds) > intervalSeconds)
            {
                return true;
            }
            return false;

        }
    }

    private void CannotShow() {
        AudioHelper.Instance.UnmuteSound();
        AudioHelper.Instance.UnmutePlaylist();
        //GameAnalytics.LogInterstitialAdsImpressionDone(scenariosInter, false, false);
        onFinished?.Invoke();
    }

    private void InterClosed(bool success) {
        if (success)
        {
            lastInterstitialShown = DateTime.Now;
            //SDKPlayPrefs.SetDateTime(StringConstants.PREF_INTERSTITIAL_LAST_SHOWN, DateTime.Now);
        }
        Debug.Log("tat inter");
        AudioHelper.Instance.UnmuteSound();
        AudioHelper.Instance.UnmutePlaylist();
        GameData.showAds = false;
        Time.timeScale = 1;
        onFinished?.Invoke();
        Invoke("MarkShowAdsFalseDelay",0f);
    }

    private void MarkShowAdsFalseDelay() {
        GameData.showAds = false;
    }
}
