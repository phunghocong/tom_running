﻿using Firebase.RemoteConfig;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Firebase.Extensions;

public class RemoteConfigManager {

    public static bool remoteConfigActivated;

    public static void InitializeRemoteConfig() {
        Dictionary<string, object> defaults = new Dictionary<string, object>();
        defaults.Add(StringConstants.RC_INTERSTITIAL_INTERVAL_SECONDS, 15);
        defaults.Add(StringConstants.RC_TIME_PER_WATCH_ADS, 300);
        defaults.Add(StringConstants.RC_LEVEL_FIRST_SHOW_INTER, 1);
        defaults.Add(StringConstants.RC_LEVEL_SHOW_RATE_INTERVAL, 8);
        defaults.Add(StringConstants.RC_PER_LEVEL_SHOW_KEEP_WATCH_ADS, 12);
        defaults.Add(StringConstants.RC_STARS_TO_UNLOCK_NEW_WORLDS, "0,35,80,130,175, 220");
        defaults.Add(StringConstants.RC_TIME_REFILL_LIFE_SECONDS, 180);
        defaults.Add(StringConstants.RC_LEVEL_SHOW_RATE, 10);
        defaults.Add(StringConstants.RC_KEYS_SHOW_TREASURE_CHEST, 3);
        defaults.Add(StringConstants.RC_COINS_DOUBLE_DAMAGE, 1000);
        defaults.Add(StringConstants.RC_COINS_DOUBLE_HEALTH, 1000);
        defaults.Add(StringConstants.RC_SHOULD_ASK_ATT, true);
        defaults.Add(StringConstants.RC_FREE_COIN_REWARD, 50);
        defaults.Add(StringConstants.RC_SILENT_ADS_INTERVAL_SECONDS, 30);
        defaults.Add(StringConstants.RC_SILENT_ADS_ENABLED, false);
        defaults.Add(StringConstants.RC_VIP_WEEKLY_TEXT_ATTRACTIVE_ENABLED, false);
        defaults.Add(StringConstants.RC_VIP_NO_SUBS_FIRST_DAY_SUGGESTION_FIRST_DAY, false);
        defaults.Add(StringConstants.RC_ENABLE_GET_MORE_LIFE_REFILL, true);
        defaults.Add(StringConstants.RC_TOTAL_NUMBER_SHOW_GET_MORE_LIFE_REFILL, 3);
        defaults.Add(StringConstants.RC_GOLDEN_TIME, "21:00");
        defaults.Add(StringConstants.RC_GOLDEN_TIME_ENABLED, false);
        defaults.Add(StringConstants.RC_GOLDEN_TIME_DURATION_SECONDS, 86400);
        defaults.Add(StringConstants.RC_TOTAL_TIME_UNLIMTED_LIFE_SECONDS, 86400);
        defaults.Add(StringConstants.RC_INTERSTITIAL_ON_START_LEVEL_ENABLED, false);
        defaults.Add(StringConstants.RC_LEVEL_FINISH_TUT, 3);
        defaults.Add(StringConstants.RC_FIRST_LEVEL_UNLIMITED_LIFE_IN_DAY, false);
        defaults.Add(StringConstants.RC_ENABLE_LIFE_ADS_LEVEL2, true);
        defaults.Add(StringConstants.RC_ASK_ATT_LEVEL_INTERVAL, 10);
        defaults.Add(StringConstants.RC_ASK_ATT_FIRST_LEVEL, 2);
        defaults.Add(StringConstants.RC_INTER_REWARDED_INTERVAL_ENABLED, false);
        defaults.Add(StringConstants.RC_SHOW_SILENT_ADS_FROM_LEVEL, 5);
        defaults.Add(StringConstants.RC_BEGINNER_INTERSTITIAL_INTERVAL_SECONDS, 30);
        defaults.Add(StringConstants.RC_BEGINNER_MAX_LEVEL, 1);
        defaults.Add(StringConstants.RC_REVIVE_COIN_ENABLED, true);
        defaults.Add(StringConstants.RC_DISABLE_INTERSTITIAL_ON_LOW_RAM_DEVICE, true);
        defaults.Add(StringConstants.RC_ENABLE_CLICK_CHEST_4, true);
        defaults.Add(StringConstants.RC_ENABLE_EVENT_HALLOWEEN, false);
        defaults.Add(StringConstants.RC_END_DATE_HALLOWEEN, "11/15/2021 00:00:00");
        defaults.Add(StringConstants.RC_ENABLE_NOEL, false);
        defaults.Add(StringConstants.RC_END_DATE_NOEL, "01/10/2022 00:00:00");
        defaults.Add(StringConstants.RC_LEVEL_UNLOCK_EVENT_NOEL, 5);
        FirebaseRemoteConfig.DefaultInstance.SetDefaultsAsync(defaults).ContinueWith((result) => {
            FirebaseRemoteConfig.DefaultInstance.FetchAndActivateAsync().ContinueWith((subResult) => {
                remoteConfigActivated = true;
            });
        });
    }
    public static long GetLong(string key) {
        return FirebaseRemoteConfig.DefaultInstance.GetValue(key).LongValue;
    }

    public static bool GetBool(string key) {
        return FirebaseRemoteConfig.DefaultInstance.GetValue(key).BooleanValue;
    }

    public static double GetDouble(string key) {
        return FirebaseRemoteConfig.DefaultInstance.GetValue(key).DoubleValue;
    }

    public static string GetString(string key) {
        return FirebaseRemoteConfig.DefaultInstance.GetValue(key).StringValue;
    }

}