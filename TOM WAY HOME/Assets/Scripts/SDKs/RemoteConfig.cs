﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoteConfig : MonoBehaviour
{
    public static RemoteConfig instance;

    //public float PlayerSpeed;
    public float MinPlayerSpeed = 10f;
    public float MaxPlayerSpeed = 20f;
    public float CammeraFollowSpeed = 0.5f;
    public float CammeraBallDistance = 7f;

    public float FingerSensitivity = 4f;
    public float FingerSpeed = 28.5f;
    public float FingerSpeedScreenFix = 900f;
    public float FingerMaxX = 2f;
    public float FingerIntroMaxX = 1f;

    public float fixLatency = 0.1f;

    private void Awake() {
        if(instance == null)
            instance = this;

        SetCurrentData();
    }

    public void SetCurrentData() {
        //PlayerSpeed = PlayerPrefs.GetFloat("GameSettings_GameSpeed", 18f);
        FingerSensitivity = PlayerPrefs.GetFloat("GameSettings_GameSensitive", 8);
    }

    private void Start()
    {
#if UNITY_EDITOR || UNITY_IOS
        fixLatency = 0;
        return;
#endif
#if UNITY_ANDROID
        fixLatency = 0.1f;
        return;
#endif
    }
}
