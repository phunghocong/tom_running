using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserDatas : MonoBehaviour
{
    public static void SetCoin(int number) {
        PlayerPrefs.SetInt("User_Data_TotalCoin", number);
    }

    public static int GetCoin() {
        return PlayerPrefs.GetInt("User_Data_TotalCoin", 0); ;
    }
    public static void PlusCoin(int plus) {
        var currentCoin = GetCoin();
        SetCoin(currentCoin + plus);
    }
    public static void SetHighestScore(int number)
    {
        PlayerPrefs.SetInt("User_Data_HighestScore", number);
    }
    public static int GetHighestScore()
    {
        return PlayerPrefs.GetInt("User_Data_HighestScore", 0);
    }
}
