using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneController : SingletonDonDestroy<LoadSceneController>
{
    private void Start()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;
    }
    public void LoadSceneGamePlay(bool newData)
    {;
        StartCoroutine(LoadScene("GamePlay", newData));
    }

    public void LoadSceneHome()
    {
        SceneManager.LoadScene("Home");
    }
    IEnumerator LoadScene(string sceneName, bool newData)
    {
        yield return null;

        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneName);

        asyncOperation.allowSceneActivation = false;

        //bool canShowProgress = Util.IsSongDownloaded(NotesManager.Instance.songname);

        while (!asyncOperation.isDone)
        {
            if (!newData && PopupLoading.instance != null)
            {
                //NotesManager.Instance.loading.fillAmount = asyncOperation.progress;
                PopupLoading.instance.UpdateProgress(asyncOperation.progress);
            }

            if (asyncOperation.progress >= 0.9f)
            {
                asyncOperation.allowSceneActivation = true;
            }

            yield return null;
        }
    }
}
