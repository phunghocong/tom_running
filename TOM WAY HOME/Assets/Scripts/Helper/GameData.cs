﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class GameData : MonoBehaviour
{
    public static bool showAds;
    public static List<int> SongIsDoneWatchAd = new List<int>();
    public static Vector3 SetPositionX(Vector3 position, float x) {
        position.x = x;
        return position;
    }
    public static Vector3 SetPositionYZ(Vector3 position, float y, float z) {
        position.y = y;
        position.z = z;
        return position;
    }
    public static IEnumerator HideObject(GameObject obj, float duration) {
        yield return new WaitForSeconds(duration);

        obj.SetActive(false);
    }

    //Song

    public static int CurrentSongIndex
    {
        get
        {
            return PlayerPrefs.GetInt("SongID", -1); 
        }
        set
        {
            PlayerPrefs.SetInt("SongID", value);
        }
    }


    public static void SetSongMp3Downloaded(string songname)
    {
        PlayerPrefs.SetInt("Mp3Downloaded_" + songname, 1);
    }
    public static void SetSongMidiDownloaded(string songname)
    {
        PlayerPrefs.SetInt("MidiDownloaded_" + songname, 1);
    }
    public static bool IsSongDownloadedMp3(string songname)
    {
        return PlayerPrefs.GetInt("Mp3Downloaded_" + songname, 0) == 1;
    }
    public static bool IsSongDownloadedMidi(string songname)
    {
        return PlayerPrefs.GetInt("MidiDownloaded_" + songname, 0) == 1;
    }
    public static bool IsSongDownloaded(string songname)
    {
        return PlayerPrefs.GetInt("Mp3Downloaded_" + songname, 0) == 1 && PlayerPrefs.GetInt("MidiDownloaded_" + songname, 0) == 1;
    }

    public static bool BgSoundState
    {
        get => GetBool(GlobalConstants.SOUND_ON_KEY, true);
        set
        {
            SetBool(GlobalConstants.SOUND_ON_KEY, value);
            Observer.SoundChanged?.Invoke();
        }
    }

    public static bool MusicState
    {
        get => GetBool(GlobalConstants.MUSIC_ON_KEY, true);
        set
        {
            SetBool(GlobalConstants.MUSIC_ON_KEY, value);
            Observer.MusicChanged?.Invoke();
        }
    }
    public static void SetHighScore(int songID, int score)
    {
        PlayerPrefs.SetInt(songID + "_HighScore", score);
        PlayerPrefs.Save();
    }

    public static int GetHighScore(int songID)
    {
        return PlayerPrefs.GetInt(songID + "_HighScore", 0);
    }

    public static void SetStarScore(int songID, int starScore)
    {
        PlayerPrefs.SetInt(songID + "_Star_Score", starScore);
        PlayerPrefs.Save();
    }

    public static int GetStarScore(int songID)
    {
        return PlayerPrefs.GetInt(songID + "_Star_Score", 0);
    }

    public static void SetDonePlaySong(SongData song)
    {
        SetBool("Song_Played_" + song.id,song.isDonePlaying);
    }
    public static bool GetDonePlaySong(SongData song)
    {
        return GetBool("Song_Played_" + song.id);
    }

    public static void SetSongDonePlayAds(SongData song,bool isDonePlayAd)
    {
        song.isPlayAds = !isDonePlayAd;
        SetBool("Song_Played_By_Ads " + song.id, song.isPlayAds);
        if (!SongIsDoneWatchAd.Contains(song.id))
        {
            SongIsDoneWatchAd.Add(song.id);
            SaveSongIsDoneWatchAd();
        }
    }

    public static void SaveSongIsDoneWatchAd()
    {
        string json = JsonConvert.SerializeObject(SongIsDoneWatchAd);

        PlayerPrefs.SetString("SongIsDoneWatchAd", json);

        PlayerPrefs.Save();
    }
    public static void LoadSongIsDoneWatchAd()
    {
        if (PlayerPrefs.HasKey("SongIsDoneWatchAd"))
        {
            string json = PlayerPrefs.GetString("SongIsDoneWatchAd");

            SongIsDoneWatchAd = JsonConvert.DeserializeObject<List<int>>(json);
        }
    }
    public static bool GetBool(string key, bool defaultValue = false) =>
        PlayerPrefs.GetInt(key, defaultValue ? 1 : 0) > 0;

    public static void SetBool(string id, bool value) => PlayerPrefs.SetInt(id, value ? 1 : 0);

    public static bool IsTutorialMode()
    {
        //return false;
        return GetBool("IsTutorial", true);
    }
}
