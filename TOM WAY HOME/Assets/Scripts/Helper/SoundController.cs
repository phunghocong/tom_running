using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : SingletonDonDestroy<SoundController> 
{
    public AudioSource backgroundAudio;
    public AudioSource musicIngameAudio;

    private void Start()
    {
        Setup();
        Observer.MusicChanged += OnMusicChanged;
        Observer.SoundChanged += OnSoundChanged;
    }
    private void OnMusicChanged()
    {
        musicIngameAudio.mute = !GameData.MusicState;
    }

    private void OnSoundChanged()
    {
        backgroundAudio.mute = !GameData.BgSoundState;
    }
    private void Setup()
    {
        OnMusicChanged();
        OnSoundChanged();
    }
}
