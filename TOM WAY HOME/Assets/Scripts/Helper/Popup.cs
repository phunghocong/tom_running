using UnityEngine;
using DG.Tweening;

public class Popup : MonoBehaviour
{
    [SerializeField] private bool useAnimation;
    [SerializeField] private bool useShowAnimation;
    [SerializeField] private ShowAnimationType showAnimationType;
    [SerializeField] private bool useHideAnimation;
    [SerializeField] private HideAnimationType hideAnimationType;

    public CanvasGroup CanvasGroup => GetComponent<CanvasGroup>();
    public Canvas Canvas => GetComponent<Canvas>();
    public virtual void Show()
    {
        BeforeShow();
        gameObject.SetActive(true);
        if (useShowAnimation)
        {
            switch (showAnimationType)
            {
                case ShowAnimationType.Fade:
                    CanvasGroup.alpha = 0;
                    CanvasGroup.DOFade(1, ConfigController.Instance.durationPopup).SetUpdate(UpdateType.Normal, true).OnComplete(AfterShown);
                    break;
            }
        }
        else
        {
            AfterShown();
        }
    }

    public virtual void Hide()
    {
        BeforeHide();
        if (useHideAnimation)
        {
            switch (hideAnimationType)
            {
                case HideAnimationType.Fade:
                    CanvasGroup.DOFade(0, ConfigController.Instance.durationPopup).OnComplete(() =>
                    {
                        CanvasGroup.alpha = 1;
                        gameObject.SetActive(false);
                        AfterHidden();
                    });
                    break;
            }
        }
        else
        {
            gameObject.SetActive(false);
            AfterHidden();
        }
    }

    protected virtual void AfterInstantiate() { }
    protected virtual void BeforeShow() { }
    protected virtual void AfterShown() { }
    protected virtual void BeforeHide() { }
    protected virtual void AfterHidden() { }
}

public enum ShowAnimationType
{
    Fade
}

public enum HideAnimationType
{
    Fade,
}

