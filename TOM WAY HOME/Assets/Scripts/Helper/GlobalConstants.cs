﻿using System;
public class GlobalConstants
{
	public static string MUSIC_ON_KEY = "MUSIC_ON";
    public static string SOUND_ON_KEY = "SOUND_ON";
    public static string VIBRATE_ON_KEY = "VIBRATE_ON";
    public static string SENSITIVE_KEY = "SENSITIVE";

    public static float SCREEN_WIDTH_BASED = 1080f;
    public static float SCREEN_HEIGHT_BASED = 1920f;

    public static String BUTTON_CLICK_SOUNDFX = "ButtonClick";

    public static String MUSIC_THEME1 = "MusicTheme1";

    public static String SONG_SELECTED_INDEX = "SONG_SELECTED_INDEX";

    public const string MUSIC_DOMAIN = "https://music.myitsol.com/api/download?src=";

    public const string MUSIC_URL = "https://music.myitsol.com/api/music";

    public const string MUSIC_LINK_STREAM = "https://music.myitsol.com/api/stream/";


}

