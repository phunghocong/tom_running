using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public static class Observer 
{
    #region GameSystem
    // Debug
    public static Action DebugChanged;
    // Currency
    public static Action MoneyChangedDone;
    public static Action<int> MoneyChangedOneParam;
    // Level
    public static Action CurrentLevelChanged;
    public static Action<PointerEventData> FirstClickGame;
    // Setting
    public static Action MusicChanged;
    public static Action SoundChanged;
    public static Action VibrationChanged;
    // Rank
    public static Action SetupRankDone;
    // Daily
    public static Action ClaimDailyRewardDone;
    public static Action<int> DailyClaimed;
    public static Action DailyClaimedViewAds;
    public static Action<int> ClaimDailyDayX;
    public static Action OpenPopupDailyReward;
    public static Action SwipeSong;
    public static Action UpdateSongTitle;
    public static Action<int> UpdateSongItemUI;

    // Item
    public static Action UnlockItemDone;
    // IAP
    public static Action PurchaseRemoveAds;
    // Shop
    public static Action RefreshShop;
    public static Action<int> PreviewPinOutfit;
    public static Action<int> PreviewTopOutfit;
    public static Action<int> PreviewBottomOutfit;
    public static Action<int> PreviewShoesOutfit;
    // Room
    public static Action<string> ChangeRoom;
    public static Action UpgradeCompleted;
    // Task
    public static Action FirstOpenPopupTask;
    public static Action TaskCompleted;
    public static Action TaskClaimed;
    public static Action PrizeTaskClaimed;
    // Other
    public static Action<string> LoadingFirstScene;
    public static Action<string> LoadingFirstSceneSucceed;
    public static Action CoinMove;
    public static Action PurchaseFail;
    public static Action PurchaseSucceed;
    public static Action ClaimReward;
    public static Action<string> ClickButton;
    public static Action PlaySong;
    public static Action PlaySongBackGround;
    public static Action PlayPreview;

    // New Tracking Game (all levels)
    public static Action<int> OnStartLevel;
    public static Action<int> OnCompleteLevel;

    // Old Tracking Game
    public static Action<int> LogEventOnFirstStartLevelLogAll;
    public static Action<int> LogEventOnFirstStartLevel;
    public static Action<int> LogEventCompletedLevelFunnel;


    public static Action<SongItem> ClickAdsItem;

    #endregion
    #region Advertising
    public static Action RequestBanner;
    public static Action ShowBanner;
    public static Action RequestInterstitial;
    public static Action ShowInterstitial;
    public static Action RequestReward;
    public static Action ShowReward;

    #endregion
}
