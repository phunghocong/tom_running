﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ButtonClickEffect : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public void OnPointerDown(PointerEventData eventData)
    {
        LeanTween.scale(gameObject, new Vector3(0.9f, 0.9f, 0f), 0.1f);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        LeanTween.scale(gameObject, new Vector3(1f, 1f, 0f), 0.1f);
    }
}

