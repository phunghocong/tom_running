using UnityEngine;

public class ConfigController : SingletonDonDestroy<ConfigController>
{
    //[SerializeField] private GameConfig gameConfig;

    //public static GameConfig Game;

    [Header("UI config")]
    public float durationPopup = .5f;
    protected override void Awake()
    {
        base.Awake();
        //Game = gameConfig;
        Initialize();
    }

    private void Initialize()
    {

    }
}