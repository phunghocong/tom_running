﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class GameDownloadResources : SingletonDonDestroy<GameDownloadResources>
{
    public void DownloadMusic(SongData song)
    {
        //foreach (var song in NotesManager.Instance.playlist)
        //{
            //if (song.id == NotesManager.Instance.playlist[GameData.CurrentSongIndex].id)
            //{
                if (!GameData.IsSongDownloadedMidi(song.name))
                {
                    StartCoroutine(SaveMusicToFileMidi(song.midi_path, song.name));
                }
                if (!GameData.IsSongDownloadedMp3(song.name))
                {
                    StartCoroutine(SaveMusicToFileMp3(song.mp3_path, song.name));
                }

            //}
        //}
    }

    IEnumerator SaveMusicToFileMp3(string musicPath, string songname)
    {
        string mp3Url = GlobalConstants.MUSIC_DOMAIN + musicPath;

        using (UnityWebRequest www = UnityWebRequest.Get(mp3Url))
        {
            UnityWebRequestAsyncOperation operation = www.SendWebRequest();

            while (!operation.isDone)
            {
                if (PopupLoading.instance != null)
                {
                    PopupLoading.instance.UpdateProgress(operation.progress);
                }
                yield return null;
            }

            if (www.result == UnityWebRequest.Result.Success)
            {
                GameData.SetSongMp3Downloaded(songname);
//#if UNITY_EDITOR
//                string filePath = Path.Combine(Application.streamingAssetsPath, songname + ".mp3");
//#elif UNITY_ANDROID
                string filePath = Path.Combine(Application.persistentDataPath, songname + ".mp3");
//#endif
                if (!File.Exists(filePath))
                {
                    File.WriteAllBytes(filePath, www.downloadHandler.data);
                }
            }
        }
    }

    IEnumerator SaveMusicToFileMidi(string musicPath, string songname)
    {
        string midiUrl = GlobalConstants.MUSIC_DOMAIN + musicPath;

        using (UnityWebRequest www = UnityWebRequest.Get(midiUrl))
        {
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.Success)
            {
                GameData.SetSongMidiDownloaded(songname);
//#if UNITY_EDITOR
//                string filePath = Path.Combine(Application.streamingAssetsPath, songname + ".mid");
//#elif UNITY_ANDROID
                string filePath = Path.Combine(Application.persistentDataPath, songname + ".mid");
//#endif
                if (!File.Exists(filePath))
                {
                    File.WriteAllBytes(filePath, www.downloadHandler.data);
                }
            }
        }
    }

   
}
