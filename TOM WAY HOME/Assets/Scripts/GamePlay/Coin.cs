using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    private bool isCollected;
    [HideInInspector] public bool isCoinOnCurve;
    [HideInInspector] public bool isCoinEndgame;

    //[SerializeField] private ParticleSystem eatCoinVFX;
    [SerializeField] private GameObject model;
    [SerializeField] private GameObject coinVfx;

    public void ResetCoin()
    {
        isCoinOnCurve = false;
        isCollected = false;
        isCoinEndgame = false;
        model.SetActive(true);
        coinVfx.SetActive(true);
    }

    private float rotateSpeed = 2f;
    private void Update()
    {
        model.transform.Rotate(0, rotateSpeed, 0, Space.Self);
    }

    private void OnTriggerEnter(Collider other)
    {
        if ((other.CompareTag("PlatformSlide") || other.CompareTag("Player")) && !isCollected /*&& GameController.instance.gameStatus == GameStatus.LIVE*/)
        {
            isCollected = true;
            //eatCoinVFX.Play();
            PlayerController.instance.PlayCoinVFX();
            if(isCoinEndgame) 
                SFX.instance.PlayCoinSFX();
           
            if(!isCoinOnCurve) 
                VFXController.Instance.PlayTextVFX();
            GameController.instance.coins++;
            GamePlayPopup.instance.UpdateCoinText();

            GamePlayPopup.instance.UpdateScoreText(2);
            model.SetActive(false);
            coinVfx.SetActive(false);
            StartCoroutine(GameData.HideObject(gameObject, 0.3f));
        }
    }
}
