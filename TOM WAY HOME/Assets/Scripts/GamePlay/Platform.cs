using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    public bool isFakeTile;
    public bool isDynamicTile;
    public bool isStrongNote;
    public bool isSlideTile;

    public float time;
    public float nextNoteDistance;
    public float duration;
    public float slideTime;
    public float velocity;

    public int nextJumpNoteID;
    public int noteID;
    public bool isHitted;
    public bool canSlide;

    public Coin coin;
    public Star star;

    //private
    private Vector3 _dynamicPosition;

    private Vector3 _slidePosition;
    private PlayerController _player => PlayerController.instance;

    [SerializeField] private Animator platformAnim;

    //VFX
    [SerializeField] private ParticleSystem tileVFX;

    //FakeTile
    [SerializeField] private GameObject fakeTileModel;
    [SerializeField] private ParticleSystem fakeTileVFX;

    public void ResetPlatform() {
        isHitted = false;
        isDynamicTile = false;
        isStrongNote = false;
        canSlide = false;
        slideTime = 0f;

        if(coin != null) coin.gameObject.SetActive(false);
        if(star != null) star.gameObject.SetActive(false);

        if (platformAnim != null) 
        {
            if (GameController.instance.gameStatus == GameStatus.LIVE) {
                platformAnim.enabled = true;
                platformAnim.Play("Intro");
            }
            else
            {
                platformAnim.enabled = false;
            }
        }

        if (tileVFX != null) 
            tileVFX.gameObject.SetActive(false);
        _time = (transform.position.x + 2f) * 3f;
    }
    public void CheckTimingFakeTile()
    {
        fakeTileModel.SetActive(true);
        StartCoroutine(IECheckingTimingFakeTile());
    }

    IEnumerator IECheckingTimingFakeTile()
    {
        while (GameController.instance.timePlay < time)
            yield return null;

        fakeTileModel.SetActive(false);
        fakeTileVFX.Play();
    }

    private float _time;
    private void LateUpdate()
    {
        if (isDynamicTile)
        {
            _time += Time.deltaTime;
            _dynamicPosition.x = Mathf.PingPong(_time * 3f, 4) - 2f;
            _dynamicPosition.z = transform.position.z;
            _dynamicPosition.y = transform.position.y;
            transform.position = _dynamicPosition;
        } else if (canSlide)
        {
            //Slide
            _slidePosition = _player.transform.position;
            _slidePosition.y = 0f;

            transform.position = _slidePosition;

            UpdatePositionFollowMouse();
        }   
    }

    Vector3 _lastBallPos = Vector3.zero;
    Vector3 _currentBallPos;
    float _speed = 10f;
    private void UpdatePositionFollowMouse() {
        _currentBallPos = _player.transform.position;
        _currentBallPos.y = 0f;
        if (_lastBallPos != Vector3.zero) {
            float deltaX = _currentBallPos.x - _lastBallPos.x;
            Quaternion q = Quaternion.EulerAngles(0, 0, deltaX);
            transform.localRotation = Quaternion.Slerp(transform.localRotation, q,
                _speed * Time.deltaTime);
            _player.transform.localRotation = transform.localRotation;

            _lastBallPos = _currentBallPos;
        } else
            _lastBallPos = _currentBallPos;
    }
    public void Hit() {
        isHitted = true;
        if (tileVFX != null) {
            tileVFX.gameObject.SetActive(true);
            tileVFX.Play();
            platformAnim.Play("Jump");
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PlatformRun"))
        {
            other.gameObject.SetActive(false);
        }
        if (other.CompareTag("Player") && !isHitted && isFakeTile)
        {
            isHitted = true;
            fakeTileModel.SetActive(false);
            fakeTileVFX.Play();
            PlayerController.instance.CheckCollisionPosition(transform.position.x);
        }
    }
}
