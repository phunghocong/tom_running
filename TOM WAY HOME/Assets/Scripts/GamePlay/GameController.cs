﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameStatus {
    DIE,
    CONTINUE,
    INTRO,
    LIVE
}
public enum DeathType {
    OBS,
    FAKETILE,
    FALL
}
public class GameController : MonoBehaviour
{
    public static GameController instance;
    //const
    private float _fingerSpeed = 0.03f;

    //public
    [SerializeField] private PlayerController _player => PlayerController.instance;
    public GameStatus gameStatus;
    public DeathType deathBy;
    public float timePlay;
    public int currentNoteIDToJump;
    [HideInInspector] public float introTime;
    [HideInInspector] public int coins;
    [HideInInspector] public int stars;
    [HideInInspector] public int score;
    [HideInInspector] public bool isTutorial => GameData.IsTutorialMode();

    //private
    [SerializeField] public Animator DeathAnim;

    private Vector3 _pos;
    private float _centerX;
    public bool _isMouseButton;
    private Vector3 _lastPosBall;
    private float _destination;
    
    private Vector3 _lastPosMouse;

    //count score
    private float timePerBeat;
    private int countScore;
    private PopupController _popupController;
    public Camera cameraMain;

    private void Awake() {
        instance = this;
    }

    private void Start()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;
        _popupController = PopupController.Instance;
        GamePrepare();
    }

   
    private void Update() {
        if (gameStatus == GameStatus.LIVE) {
            if (Input.GetMouseButtonDown(0)) {
                _pos = Input.mousePosition;
                _centerX = PlayerController.instance.transform.position.x;
            }

            _isMouseButton = Input.GetMouseButton(0);

            timePlay += Time.deltaTime;

            if(timePlay >= countScore * timePerBeat)
            {
                countScore++;
                if (GamePlayPopup.instance) GamePlayPopup.instance.UpdateScoreText(1);
            }

            if (Input.GetMouseButtonUp(0))
            {
                _player.transform.localRotation = Quaternion.EulerAngles(Vector3.zero);
            }

            //Pause
            if (Input.GetKeyUp(KeyCode.Escape)){
                HandleBackEvent();
            }
        }
    }
    public bool lockMove;
    public void LockMove(bool _lock) {
        lockMove = _lock;

        if (!lockMove) {
            _pos = Input.mousePosition;
            _centerX = PlayerController.instance.transform.position.x;
        }
    }

    void FixedUpdate() {
        if (gameStatus != GameStatus.LIVE && gameStatus != GameStatus.INTRO) return;

        if (_isMouseButton && !lockMove) {
            _destination = _centerX + (Input.mousePosition.x - _pos.x) * _fingerSpeed;
            var position = PlayerController.instance.transform.position;
            float deltaMove = (_destination - position.x) /
                                      RemoteConfig.instance.FingerSensitivity;

            if (MaxX < 1 || Mathf.Abs(position.x + deltaMove) < MaxX) {
                position.x += deltaMove;
                _lastPosMouse = _pos;
            } else {
                // di chuyển quá nhanh, cân bằng lại
                position = GameData.SetPositionX(position, MaxX * Mathf.Sign(position.x));
                _centerX -= deltaMove - (position.x - _lastPosBall.x);
                _pos = _lastPosMouse;
            }

            _lastPosBall = position;
            PlayerController.instance.transform.position = position;
            UpdatePositionFollowMouse();
        }
    }

    Vector3 _lastBallPos = Vector3.zero;
    Vector3 _currentBallPos;
    float _speed = 10f;
    private void UpdatePositionFollowMouse() {
        _currentBallPos = _player.transform.position;
        _currentBallPos.y = 0f;
        if (_lastBallPos != Vector3.zero && !Spawns.instance.isEnding) {
            float deltaX = (_currentBallPos.x - _lastBallPos.x) * 3f;
            Quaternion q = Quaternion.EulerAngles(0, deltaX, 0);
            transform.localRotation = Quaternion.Slerp(transform.localRotation, q,
                _speed * Time.deltaTime);
            _player.transform.localRotation = transform.localRotation;

            _lastBallPos = _currentBallPos;
        } else
            _lastBallPos = _currentBallPos;
    }
        
    public void GamePrepare()
    {
        gameStatus = GameStatus.DIE;
        //GamePlayPopup.instance.GameUIActionStatus(true);
        _popupController.HideAll();
        _popupController.Show<GamePlayPopup>();
        UpdateFingerSpeed();
        SetGameState(true);

        timePerBeat = 60f / NotesManager.Instance.songBpm;
        countScore = 1;
    }
    private float MaxX;
    public void IntroGame() {
        StartCoroutine(PlayIntroGame());
    }
    public IEnumerator PlayIntroGame() {
        MaxX = RemoteConfig.instance.FingerIntroMaxX;
        GameStart();
        NotesManager.Instance.PlayMusic();

        yield return new WaitForSeconds(introTime);

        MaxX = RemoteConfig.instance.FingerMaxX;
        MonsterController.instance.EditZOffset(true);
        Follow.instance.MoveCamera(true);

        yield return new WaitForSeconds(3f);

        Follow.instance.MoveCamera(false);
        MonsterController.instance.EditZOffset(false);

    }
    public void GameStart()
    {
        //if (gameStatus == GameStatus.LIVE) return;
        gameStatus = GameStatus.LIVE;
        PlayerController.instance.GameStart();
        Spawns.instance.SpawnGate();
        ////NotesManager.instance.PlayMusic();
        GamePlayPopup.instance.GameUIActionStatus(false);
    }

    public void PlayEndgame() {
        Vector3 position = _player.transform.position;
        position.y = 0;
        DeathAnim.transform.position = position;
        DeathAnim.gameObject.SetActive(true);

        StartCoroutine(EndGame());
    }
    IEnumerator EndGame()
    {
        gameStatus = GameStatus.DIE;
        NotesManager.Instance.StopMusic();
        _player.Endgame();
        GamePlayPopup.instance.GameUIActionStatus(false);

        yield return new WaitForSeconds(2f);
        _popupController.Show<ContinueUI>();
    }

    public void GameReplay()
    {
        coins = 0;
        stars = 0;
        score = 0;
        countScore = 1;
        _popupController.HideAll();
        _popupController.Show<GamePlayPopup>();
        if (GamePlayPopup.instance != null) { GamePlayPopup.instance.ResetGame(); };
        DeathAnim.gameObject.SetActive(false);
        _player.GameReplay();
        Spawns.instance.ResetGame();
        LockMove(false);

        Follow.instance.ResetCameraPosition(0);
        Follow.instance.RoadStatus(true);
        GamePlayPopup.instance.GameUIActionStatus(true);
    }
    public void GameRevivePrepare() {
        gameStatus = GameStatus.CONTINUE;
        DeathAnim.gameObject.SetActive(false);
        Spawns.instance.GameRevive();
        _player.GameRevive();
        Spawns.instance.SpawnGate();
        Follow.instance.GameRevive();
        Follow.instance.RoadStatus(true);
        GamePlayPopup.instance.GameUIActionStatus(true);
    }
    public void GameRevive()
    {
        LockMove(false);
        _player.ResetTime();
        NotesManager.Instance.PlayMusic(timePlay);
        gameStatus = GameStatus.LIVE;

        if (deathBy == DeathType.OBS){
            if (_player.rolling)
            {
                NoteData note = Spawns.instance.runNote;
                _player.Roll(note.duration, note.nextNoteDistance);
            } else if (_player.canSlide) {
                Platform platform = Spawns.instance.slidePlatform;
                _player.Slide(platform, platform.slideTime, platform.nextNoteDistance);
            }
        }

        GamePlayPopup.instance.GameUIActionStatus(false);
    }

    public void HandleBackEvent() {
        if (gameStatus != GameStatus.DIE) {
            GamePause();
        }
    }
    void OnApplicationPause(bool pauseStatus) {
        if (Application.platform == RuntimePlatform.WindowsEditor ||
            Application.platform == RuntimePlatform.OSXEditor) {
            if (pauseStatus) {
                GamePause();
            }
        }
    }
    private void OnApplicationFocus(bool hasFocus) {
        if (Application.platform != RuntimePlatform.WindowsEditor &&
            Application.platform != RuntimePlatform.OSXEditor) {
            if (!hasFocus) {
                GamePause();
            }
        }
    }

    [HideInInspector] public float timePause;
    public void GamePause() {
        if (gameStatus == GameStatus.DIE) 
            return;
        NotesManager.Instance.PauseMusic();
        SetGameState(false);
        _popupController.Show<PauseUI>();
        GamePlayPopup.instance.GameUIActionStatus(false);
    }
    public void GameResume() {
        NotesManager.Instance.PlayMusic(timePause);
        SetGameState(true);
    }
    public static void SetGameState(bool isPlaying) {
        Time.timeScale = isPlaying ? 1 : 0;
    }
    public void UpdateFingerSpeed() {
        float screenWith = Screen.width;
        float sensitive = PlayerPrefs.GetFloat(GlobalConstants.SENSITIVE_KEY, 0.5f);

        float touchSensitiveUser = 0f;
        _fingerSpeed = (touchSensitiveUser * 2 + (RemoteConfig.instance.FingerSpeed * (0.5f + sensitive))) / screenWith;
        if (touchSensitiveUser > 0) {
            _fingerSpeed += touchSensitiveUser * 2 / screenWith;
        }
    }
    private float lastTimescale;

    public void Win()
    {
        if (AdsMediationController.Instance.IsInterstitialAvailable && Application.internetReachability != NetworkReachability.NotReachable)
        {
            Debug.Log("show inter when win");
            AudioHelper.Instance.MutePlaylist();
            Time.timeScale = 0;
            AdsMediationController.Instance.ShowInterstitial("win", OnInterstitialFinished);
        }
        else
        {
            Spawns.instance.gateEndGame.SetActive(false);
            _popupController.HideAll();
            _popupController.Show<GameResult>();
        }
    }
    private void OnInterstitialFinished(bool success)
    {
        if (success)
        {
            AudioHelper.Instance.UnmutePlaylist();
            Time.timeScale = 1;
            Spawns.instance.gateEndGame.SetActive(false);
            _popupController.HideAll();
            _popupController.Show<GameResult>();
        }
    }
    public void ReturnHome()
    {
        _popupController.HideAll();
        _popupController.Show<PopupReturnTransition>();
    }

    public void GameStarData(SongData song)
    {
        
    }
}
