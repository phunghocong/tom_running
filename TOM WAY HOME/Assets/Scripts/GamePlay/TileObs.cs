using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileObs : MonoBehaviour
{
    private bool hitted;

    private void OnEnable()
    {
        hitted = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !hitted)
        {
            hitted = true;
            PlayerController.instance.CheckCollisionPosition(transform.position.x);
        }
    }
}
