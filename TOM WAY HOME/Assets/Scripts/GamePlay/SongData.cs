using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SongData 
{
    public int id;
    public string name;
    public string mp3_path;
    public string midi_path;
    public string version;
    public int category_id;
    public int BPM;
    public string unlock_type;
    public string difficulty;
    public string stream_url;
    public string midi_url;
    public string preview_mp3;
    public bool active = true;
    public bool isPlayAds;
    public string preview_url;
    public bool isDonePlaying;
    public string artist;
    public string cover_photo;
    public float speed;
    public Sprite spriteUI;
    public bool isClickAds;
    public float timePreview;
}

