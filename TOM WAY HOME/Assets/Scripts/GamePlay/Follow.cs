﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Kino;

public class Follow : MonoBehaviour
{
    public static Follow instance;

    //public
    public GameObject road;
    [HideInInspector] public Transform transformCamera;
    private Vector3 _initPosCamera;

    //private
    private Vector3 _playerPosition;
    private Vector3 _position;
    private float _move;

    private float _deltaZWithRoad;
    private Vector3 _roadPosition;

    //Camera
    private float zoomInPosZ = -5f;
    private float zoomInPosY = 1.7f;
    private float zoomOutPosZ = -6f;
    private float zoomOutPosY = -0.8f;

    //public Bloom bloom;

    private void Awake() {
        instance = this;

        transformCamera = Camera.main != null ? Camera.main.transform : GetComponentInChildren<Camera>().transform;
        _initPosCamera = transformCamera.localPosition;

        _deltaZWithRoad = transform.position.z - road.transform.position.z;
    }

    void LateUpdate() {
        if (GameController.instance.gameStatus != GameStatus.LIVE && GameController.instance.gameStatus != GameStatus.INTRO)
            return;

        _playerPosition = PlayerController.instance.transform.position;

        _position = transform.position;

        //_move = (_playerPosition.x * 0.5f - _position.x) * RemoteConfig.instance.CammeraFollowSpeed / 2f;
        _position.x = _playerPosition.x * 0.5f;
        //_position.x += _move;
        _position.z = PlayerController.instance.transform.position.z - RemoteConfig.instance.CammeraBallDistance;

        transform.position = _position;

        _roadPosition = road.transform.position;
        _roadPosition.z = transform.position.z - _deltaZWithRoad;
        road.transform.position = _roadPosition;
    }

    public void ResetCameraPosition(float timeAnimation) {
        ResetBloom();
        transformCamera.DOLocalMove(_initPosCamera, timeAnimation);

        _position = transform.position;
        _position.x = 0;
        _position.z = PlayerController.instance.transform.position.z - RemoteConfig.instance.CammeraBallDistance;

        transform.position = _position;
    }

    public void RoadStatus(bool isEnable)
    {
        _roadPosition = road.transform.position;
        _roadPosition.z = transform.position.z - _deltaZWithRoad;
        road.transform.position = _roadPosition;

        road.SetActive(isEnable);
    }
    public void GameRevive()
    {
        ResetBloom();
        _playerPosition = PlayerController.instance.transform.position;

        _position = transform.position;

        _move = (_playerPosition.x * 0.5f - _position.x);
        _position.x += _move;
        _position.z = PlayerController.instance.transform.position.z - RemoteConfig.instance.CammeraBallDistance;

        transform.position = _position;
    }
    public void MoveCamera(bool isZoomOut) {
        if (isZoomOut)
        {
            StartCoroutine(IEMoveCamera(zoomOutPosZ, zoomOutPosY));
        }
        else
        {
            StartCoroutine(IEMoveCamera(zoomInPosZ, zoomInPosY));
        }
    }
    IEnumerator IEMoveCamera(float posZ, float posY) {
        Vector3 localPos;
        localPos = transformCamera.localPosition;
        float z = localPos.z;
        float y = localPos.y;

        float timer = 0f;
        float duration = 0.5f;
        while (timer < duration) {
            timer += Time.deltaTime;

            localPos.z = Mathf.Lerp(z, posZ, timer / duration);
            localPos.y = Mathf.Lerp(y, posY, timer / duration);

            transformCamera.localPosition = localPos;
            yield return null;
        }

        localPos.z = posZ;
        localPos.y = posY;
        transformCamera.localPosition = localPos;
    }
    public void MoveCameraEndgame()
    {
        StartCoroutine(CameraEndgame());
    }
    IEnumerator CameraEndgame(float posZ = -2f, float posY = 3f)
    {
        Vector3 position;
        position = transformCamera.localPosition;
        float z = position.z;
        float y = position.y;

        float timer = 0f;
        float duration = 0.3f;
        while (timer < duration)
        {
            timer += Time.deltaTime;

            position.z = Mathf.Lerp(z, posZ, timer / duration);
            position.y = Mathf.Lerp(y, posY, timer / duration);

            transformCamera.localPosition = position;
            yield return null;
        }

        position.z = posZ;
        position.y = posY;
        transformCamera.localPosition = position;
    }

    //public IEnumerator Bloom(float duration = 4f)
    //{
    //    float timer = 0;
    //    float value;
    //    bloom.thresholdGamma = 1;
    //    while(timer < duration)
    //    {
    //        timer += Time.deltaTime;
    //        value = Mathf.Lerp(0f, .3f, timer / duration);
    //        bloom.intensity = value;
    //        value = Mathf.Lerp(1f, 0f, timer / duration);
    //        bloom.softKnee = value;
    //        yield return null;
    //    }
    //}
    public void ResetBloom()
    {
        //bloom.thresholdGamma = 0;
        //bloom.intensity = 0f;
        //bloom.softKnee = 1f;
    }
}
