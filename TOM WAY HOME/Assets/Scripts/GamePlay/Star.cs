using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : MonoBehaviour
{
    private bool isCollected;

    [SerializeField] private GameObject model;

    public void ResetStar()
    {
        isCollected = false;
        model.SetActive(true);
    }

    private void OnTriggerEnter(Collider other)
    {
        if ((other.CompareTag("PlatformSlide") || other.CompareTag("Player")) && !isCollected/* && GameController.instance.gameStatus == GameStatus.LIVE*/)
        {
            isCollected = true;
            GameController.instance.stars++;
            GamePlayPopup.instance.progressbar.MoveStar(transform.position, GameController.instance.stars);
            if (GameController.instance.stars > GameData.GetStarScore(GameData.CurrentSongIndex))
            {
                GameData.SetStarScore(GameData.CurrentSongIndex, GameController.instance.stars);
            }
            SFX.instance.PlayStarSFX();
            model.SetActive(false);
            StartCoroutine(GameData.HideObject(gameObject, 0.3f));
        }
    }
}
