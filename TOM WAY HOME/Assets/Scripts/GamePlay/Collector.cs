using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collector : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("PlatformJump") || other.CompareTag("PlatformRun") || other.CompareTag("PlatformSlide")
            || other.CompareTag("Dynamic_Obs_1") || other.CompareTag("Dynamic_Obs_2") || other.CompareTag("Dynamic_Obs_3")
            || other.CompareTag("Static_Obs_1") || other.CompareTag("Static_Obs_2")
            || other.CompareTag("Coin"))
        {
            other.gameObject.SetActive(false);
        }
    }
}
