using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    [HideInInspector] public bool hitted = false;
    public float time;
    public Animator attack;
    public Animator DO2_Anim;
    public Animator DO1_Anim;
    public bool isAttackLeft;
    public bool isJumpLeft;
    [SerializeField] private bool isDO_1;
    [SerializeField] private bool isDO_2;
    [SerializeField] private GameObject DO_2_Model;
    [SerializeField] private GameObject DO_1_Model;
    [SerializeField] private List<GameObject> SO_2_Model;
    [SerializeField] private List<GameObject> attackTilesDO3;
    [SerializeField] private ParticleSystem obsVFX;

    public void ResetObs()
    {
        hitted = false;
        if(SO_2_Model.Count > 0)
        {
            foreach (var obs in SO_2_Model)
                obs.SetActive(false);

            if(time < Spawns.instance.endRunTime)
            {
                SO_2_Model[1].SetActive(true);
            }
            else
            {
                SO_2_Model[0].SetActive(true);
            }
        }
    }
    private void OnEnable()
    {
        if (attack != null)
        {
            attackTilesDO3[0].SetActive(isAttackLeft);
            attackTilesDO3[1].SetActive(!isAttackLeft);
            if (isAttackLeft)
                obsVFX.transform.localPosition = attackTilesDO3[0].transform.localPosition;
            else
                obsVFX.transform.localPosition = attackTilesDO3[1].transform.localPosition;

            StartCoroutine(ToAttack());
        }
        if (isDO_1 && DO1_Anim != null)
        {
            Vector3 currentPosition = DO_1_Model.transform.position;
            Vector3 startPostion = new Vector3(currentPosition.x, currentPosition.y, currentPosition.z + 5f);
            DO_1_Model.transform.position = startPostion;

            StartCoroutine(IntroDO1(currentPosition));
        }
        if (isDO_2 && DO2_Anim != null)
        {
            DO_2_Model.SetActive(false);
            if (isJumpLeft)
            {
                DO_2_Model.transform.localPosition = new Vector3(-1f, -0.5f, 1f);
            }
            else
            {
                DO_2_Model.transform.localPosition = new Vector3(1f, -0.5f, 1f);
            }
            DO_2_Model.SetActive(true);
            StartCoroutine(IntroDO2());
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && !hitted)
        {
            hitted = true;
            PlayerController.instance.CheckCollisionPosition(transform.position.x);
        }
    }
    IEnumerator ToAttack() {
        while(GameController.instance.timePlay < time - 0.55f) {
            yield return null;
        }
        string _att = isAttackLeft ? "AttackLeft" : "AttackRight";

        attack.SetTrigger(_att);

        while (GameController.instance.timePlay < time)
        {
            yield return null;
        }

        obsVFX.Play();
    }

    IEnumerator IntroDO1(Vector3 currentPosition)
    {
        while (GameController.instance.timePlay < time - 1f)
        {
            yield return null;
        }
        DO1_Anim.Play("Roll");
        DO_1_Model.transform.DOMove(currentPosition, 0.5f);
    }
    IEnumerator IntroDO2()
    {
        while (GameController.instance.timePlay < time - 0.4f)
        {
            yield return null;
        }
        DO2_Anim.enabled = true;
        string _jump = isJumpLeft ? "Intro_Left" : "Intro_Right";

        DO2_Anim.Play(_jump);

        while (GameController.instance.timePlay < time)
        {
            yield return null;
        }

        obsVFX.Play();
    }
}
