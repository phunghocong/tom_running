using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFX : MonoBehaviour
{
    public static SFX instance;

    [SerializeField] private AudioSource coin;
    [SerializeField] private AudioSource star;

    private void Awake()
    {
        instance = this;
    }

    public void PlayCoinSFX()
    {
        if (PlayerPrefs.GetInt(GlobalConstants.SOUND_ON_KEY, 1) == 1)
        {
            coin.Stop();
            coin.Play();
        }
    }
    public void PlayStarSFX()
    {
        if (PlayerPrefs.GetInt(GlobalConstants.SOUND_ON_KEY, 1) == 1)
        {
            star.Stop();
            star.Play();
        }
    }
}
