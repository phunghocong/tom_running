﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;

    //public
    public float playerSpeedFollowBpm;
    public Rigidbody rg;
    public float forwardSpeed;
    public Animator anim;
    public Animator monsterAnim;
    [SerializeField] private ParticleSystem stunVFX;
    [SerializeField] private ParticleSystem eatCoinVFX;

    //private
    private GameController _gameController => GameController.instance;
    private Vector3 _playerPosition;
    private Vector3 _playerPositionInit;
    private Vector3 _velocity = Vector3.zero;
    [SerializeField] private EnvironmentGamePlay environment;

    private void Awake() {
        instance = this;
    }
    private void Start() {
        //forwardSpeed = playerSpeedFollowBpm;
    }
    //public void SetGameSpeed(float speed) {
    //    forwardSpeed = speed;

    //    Spawns.instance.HideAll();
    //    GameController.instance.GamePrepare();
    //    GameController.instance.GameReplay();
    //}

    private void Update() {
        if (GameController.instance.gameStatus != GameStatus.LIVE && GameController.instance.gameStatus != GameStatus.INTRO) return;

        _playerPosition = transform.position;
        _playerPosition.z += forwardSpeed * Time.deltaTime;
        
        transform.position = _playerPosition;
        if(!canSlide)
        {
            //transform.localRotation = Quaternion.EulerAngles(Vector3.zero);
        }
    }

    public void GamePrepare()
    {
        Idle();
    }
    public void GameStart()
    {
        Run();
        _gameController.timePlay = 0;
        _trustTime = 0;
        _beginTime = Time.time;
        _playerPositionInit = transform.position;
        _gameController.currentNoteIDToJump = -1;
        //Jump(0, NotesManager.instance.mainTrackNoteDatas[0].time);
    }

    public void Endgame()
    {
        rg.useGravity = false;
        rg.velocity = Vector3.zero;
        gameObject.SetActive(false);
        monsterAnim.transform.parent.gameObject.SetActive(false);
        CancelInvoke();
    }
    public void GameReplay()
    {
        Idle();
        canSlide = false;
        rolling = false;
        stunning = false;
        stunAnim = false;
        gameObject.SetActive(true);
        transform.localRotation = Quaternion.EulerAngles(Vector3.zero);
        monsterAnim.transform.parent.gameObject.SetActive(true);
        _playerPosition = transform.position;
        _playerPosition.x = 0f;
        _playerPosition.y = 0f;
        transform.position = _playerPosition;

        _playerPositionInit = _playerPosition;
        forwardSpeed = playerSpeedFollowBpm;
    }

    public void GameRevive()
    {
        stunning = false;
        stunAnim = false;
        gameObject.SetActive(true);
        monsterAnim.transform.parent.gameObject.SetActive(true);
        transform.localRotation = Quaternion.EulerAngles(Vector3.zero);

        NoteData note = new NoteData();
        if (GameController.instance.deathBy == DeathType.FAKETILE || GameController.instance.deathBy == DeathType.FALL)
        {
            note = NotesManager.Instance.mainTrackNoteDatas[Spawns.instance.noteDeath];
        } else if(GameController.instance.deathBy == DeathType.OBS)
        {
            note = NotesManager.Instance.obsTrackNoteDatas[Spawns.instance.noteDeath];   
        }
        _playerPosition.z = _playerPositionInit.z + note.time * forwardSpeed;
        _playerPosition.x = 0f;
        _playerPosition.y = 0f;

        transform.position = _playerPosition;
    }
    public Vector3 GetInitPosition()
    {
        return _playerPositionInit;
    }
    private void OnTriggerStay (Collider other) {
        if (GameController.instance.gameStatus != GameStatus.LIVE)
            return;
        if (other.CompareTag("ZoomCam"))
        {
            Follow.instance.MoveCameraEndgame();
        }
        else if (other.CompareTag("Gate")) {
            NotesManager.Instance.StopMusic();
            gameObject.SetActive(false);
            GameController.instance.Win();
        } else if (other.CompareTag("Death"))
        {
            GameController.instance.deathBy = DeathType.FALL;
            VFXController.Instance.PlayDeathVFX(transform.position);
            GameController.instance.PlayEndgame();
        } 
        //else if (other.CompareTag("FakeTile")) {
        //    //GameController.instance.deathBy = rolling ? DeathType.OBS : DeathType.FAKETILE;
        //    CheckCollisionPosition(other.transform.position.x, true);
        //    other.gameObject.SetActive(false);
        //    //VFXController.instance.PlayFakeTileVFX(other.transform.position);
        //    //GameController.instance.PlayEndgame();
        //} 
        else if (other.CompareTag("PlatformJump")) {
            Platform pl = Spawns.instance.GetPlatformJumpByName(other.name);

            if (!pl.isHitted && _gameController.currentNoteIDToJump == pl.noteID) { //Tranh nhay vuot note
                VFXController.Instance.PlayTextVFX();
                Jump(pl.slideTime, pl.nextNoteDistance, pl.noteID);
                //UIManager.instance.UpdateScoreText(1);
                EditPositionZ(pl.transform.position);

                _gameController.currentNoteIDToJump = pl.nextJumpNoteID;
                pl.Hit();
                if(pl.velocity > 90) {
                    VFXController.Instance.PlayStrongNoteVFX(pl.transform.position);
                    environment.PlayStrongNote();
                }
            }
        } else if (other.CompareTag("PlatformRun") && !rolling && !canSlide)
        {
            Platform pl = Spawns.instance.GetPlatformRunByName(other.name);

            if (!pl.isHitted && _gameController.currentNoteIDToJump == pl.noteID)
            {
                //UIManager.instance.UpdateScoreText(10);
                rollingTime = 0;
                Roll(pl.duration, pl.nextNoteDistance);

                _gameController.currentNoteIDToJump = pl.nextJumpNoteID;
                pl.isHitted = true;
                //EditPositionZ(pl.transform.position);
            }
        }
        else if (other.CompareTag("PlatformSlide"))
        {
            Platform pl = Spawns.instance.GetPlatformSlideByName(other.name);
            if (!pl.isHitted && _gameController.currentNoteIDToJump == pl.noteID)
            {
                //UIManager.instance.UpdateScoreText(10);
                slideTime = 0;
                Slide(pl, pl.slideTime, pl.nextNoteDistance);

                _gameController.currentNoteIDToJump = pl.nextJumpNoteID;
                pl.isHitted = true;
                EditPositionZ(pl.transform.position);
            }
        }
    }
    public bool stunning = false;
    public bool stunAnim = false;
    private float stunAnimTime;
    private float stunAnimJump = 1.5f;
    private float stunAnimRoll = 0.6f;
    public void CheckCollisionPosition(float platformX, bool isfakeTile = false)
    {
        if (GameController.instance.gameStatus != GameStatus.LIVE) return;
        if(Mathf.Abs(transform.position.x - platformX) < 0.3f || stunning) // DEATH
        {
            if (isfakeTile) {
                GameController.instance.deathBy = rolling ? DeathType.OBS : DeathType.FAKETILE;
            } else {
                GameController.instance.deathBy = DeathType.OBS;
            }
            GameController.instance.PlayEndgame();
        }
        else
        {
            stunning = true;
            stunAnim = true;
            stunVFX.Play();
            //Play Stun Anim + VFX
            Stun();
            if (MonsterController.instance != null)
                MonsterController.instance.EditZOffset(true);
            Invoke("FinishStun", 5f);
            Invoke("FinishAnimStun", stunAnimTime);
        }
    }

    private void FinishAnimStun() {
        stunAnim = false;
        if (rolling) {
            Run();
        } else if (canSlide) {
            Idle();
        }
    }
    private void FinishStun()
    {
        stunning = false;
        stunVFX.Stop();
        if(MonsterController.instance != null) 
            MonsterController.instance.EditZOffset(false);
    }

    private float _trustTime;
    private float _beginTime;
    private bool jumpping = false;
    private void Jump(float timeBeforeJump, float nextNoteDistance, int noteID, bool JumpAfterSlide = false)
    {
        if (!jumpping && !JumpAfterSlide) {
            jumpping = true;
        }
        _trustTime += nextNoteDistance + timeBeforeJump;
        float jumpTime = (_trustTime - (Time.time - _beginTime)) / 2f;
        rg.useGravity = true;

        Physics.gravity = -Vector3.up * (((GetJumpHeight(nextNoteDistance)) * 2) / (Mathf.Pow(jumpTime, 2)));

        _velocity.y = -Physics.gravity.y * jumpTime;

        rg.velocity = _velocity;

        if (!stunAnim) {
            if (jumpTime > 0.6f) {
                JumpLong(jumpTime);
            } else {
                if (!isJumpLeft)
                    JumpLeft(jumpTime);
                else
                    JumpRight(jumpTime);
            }
        }
    }

    public void ResetTime()
    {
        _trustTime = GameController.instance.timePlay;
        _beginTime = Time.time - GameController.instance.timePlay;
        slideTime -= Spawns.instance.deltaTime;
        rollingTime -= Spawns.instance.deltaTime;
    }

    public void Slide(Platform pl, float _slideTime, float nextNoteDistance)
    {
        jumpping = false;
        StartCoroutine(IESlide(pl, _slideTime, nextNoteDistance));
    }

    public bool canSlide = false;
    public float slideTime = 0;
    IEnumerator IESlide(Platform pl, float _slideTime, float nextNoteDistance)
    {
        Idle();

        canSlide = true;
        pl.canSlide = true;
        rg.useGravity = false;
        rg.velocity = Vector3.zero;

        float firtTimeSlide = slideTime;
        float timer = slideTime;
        while (timer < _slideTime) {
            if (GameController.instance.gameStatus == GameStatus.LIVE) timer += Time.fixedDeltaTime;
            slideTime = timer;
            yield return new WaitForFixedUpdate();
        }
        //yield return new WaitForSeconds(pl.slideTime);

        canSlide = false;
        pl.canSlide = false;
        Jump(pl.slideTime - firtTimeSlide, pl.nextNoteDistance, pl.noteID, true);
        pl.transform.localRotation = Quaternion.EulerAngles(Vector3.zero);
        StartCoroutine(GameData.HideObject(pl.gameObject, 0.5f));
    }
    public bool rolling = false;
    public float rollingTime = 0;
    public void Roll(float duration, float nextNoteDistance) {
        jumpping = false;
        StartCoroutine(IERoll(duration, nextNoteDistance));
    }
    IEnumerator IERoll(float duration, float nextNoteDistance) {
        Run();

        rolling = true;
        rg.useGravity = false;
        rg.velocity = Vector3.zero;

        Vector3 pos = transform.position;
        pos.y = 0f;
        transform.position = pos;
        float firtTimeRun = rollingTime;
        float timer = rollingTime;
        _trustTime = _trustTime + (duration - firtTimeRun + nextNoteDistance);

        while (timer < duration || Spawns.instance.isEnding)
        {
            if(GameController.instance.gameStatus == GameStatus.LIVE) timer += Time.fixedDeltaTime;
            rollingTime = timer;
            yield return new WaitForFixedUpdate();
        }
        //yield return new WaitForSeconds(pl.duration);

        rolling = false;
        rg.useGravity = true;
        //Jump(duration - firtTimeRun, nextNoteDistance);
        //_trustTime = _trustTime + (duration - firtTimeRun + nextNoteDistance);
    }

    private float _deltaZ;
    private void EditPositionZ(Vector3 position)
    {
        _deltaZ = transform.position.z - position.z;
        transform.position = GameData.SetPositionYZ(transform.position, 0f, transform.position.z - _deltaZ / 12f);
    }   

    public float GetJumpHeight(float time)
    {
        if (time < 0.3f)
            return 1.5f * (time / 0.3f);
        if (time < 0.5f)
            return 1.8f;
        if (time >= 0.5f)
            return 2f;

        return 1.2f;
    }

    //TOM Animation
    private void Idle()
    {
        if (stunAnim) return;

        if (anim != null) {
            anim.Play("Idle");
            anim.speed = 1f;
        }
    }
    private void Run()
    {
        if (stunAnim) return;

        if (anim != null) {
            anim.speed = 1f;
            monsterAnim.speed = 1f;
            anim.Play("Run"); 
            monsterAnim.Play("Run"); 
        }
    }
    private void Stun()
    {
        VFXController.Instance.PlayOppTextVFX();
        if (anim != null)
        {
            anim.speed = 1f;
            if (rolling)
                anim.Play("Stun_Run");
            else if (canSlide)
                anim.Play("Stun_Slide");
            else
                anim.Play("Stun");

            if (rolling || canSlide) {
                stunAnimTime = stunAnimRoll;
            } else {
                stunAnimTime = stunAnimJump;
            }
        }
    }
    public void Walk() {
        forwardSpeed = playerSpeedFollowBpm / 3f;
        if (anim != null) {
            anim.speed = 0.3f;
            anim.Play("Run");
        }
        GameController.instance.LockMove(true);
        StartCoroutine(MoveToCenter());
    }
    bool isJumpLeft;
    private void JumpLeft(float time)
    {
        isJumpLeft = true;
        if (anim != null) {
            anim.speed = 0.23f / time;
            monsterAnim.speed = 0.23f / time;
        }

        if (anim != null) {
            anim.Play("JumpLeft", -1, 0.3f);
            monsterAnim.Play("JumpLeft", -1, 0.3f);
        }
    }

    private void JumpRight(float time)
    {
        isJumpLeft = false;
        if (anim != null)
        {
            anim.speed = 0.23f / time;
            monsterAnim.speed = 0.23f / time;
        }

        if (anim != null) {
            anim.Play("JumpRight", -1, 0.3f);
            monsterAnim.Play("JumpRight", -1, 0.3f);
        }
    }
    public void JumpLong(float time)
    {
        if (anim != null) {
            anim.speed = 0.45f / time;
            if (Random.RandomRange(0, 2) == 0) {
                anim.Play("JumpLong", -1, 0f);
            } else {
                anim.Play("JumpLong_2", -1, 0f);
            }
        }
    }

    IEnumerator MoveToCenter(float duration = 1f)
    {
        float timer = 0f;
        float posX = transform.position.x;
        Vector3 position;
        transform.localRotation = Quaternion.EulerAngles(Vector3.zero);
        while (timer < duration)
        {
            timer += Time.deltaTime;
            position = transform.position;
            position.x = Mathf.Lerp(posX, 0, timer / duration);
            transform.position = position;
            yield return null;
        }
    }
    public void PlayCoinVFX() {
        eatCoinVFX.Stop();
        eatCoinVFX.Play();
    }
}
