using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXController : Singleton<VFXController>
{
    [SerializeField] private ParticleSystem WaterFallVFX;
    [SerializeField] private ParticleSystem strongNoteVFX;
    [SerializeField] private ParticleSystem moodchangeVFX;
    [SerializeField] private PerfectTextVFX perfectTextVFX;

    public void PlayDeathVFX(Vector3 position)
    {
        position.y = 0f;
        WaterFallVFX.transform.position = position;
        WaterFallVFX.Play();
    }
    public void PlayStrongNoteVFX(Vector3 position) {
        position.y = 0f;
        strongNoteVFX.transform.position = position;
        strongNoteVFX.Play();

    }
    public void PlayMoodchangeVFX()
    {
        moodchangeVFX.Play();
    }

    public void PlayTextVFX()
    {
        perfectTextVFX.PlayTextVFX();
    }
    public void PlayOppTextVFX()
    {
        perfectTextVFX.PlayOppText();
    }
}
