using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterController : MonoBehaviour
{
    public static MonsterController instance;

    public Transform leader;
    public float zOffset = 2f;

    private float zMin = 3.5f;
    private float zMax = 9f;

    private float duration = 1f;

    private Vector3 position;
    private float _move;
    private float followSpeed = 0.2f;
    private void Awake()
    {
        instance = this;
    }

    private void OnEnable() {
        position = transform.position;
        position.y = leader.transform.position.y;
        position.z = leader.transform.position.z - zMax;

        transform.position = position;
        zOffset = zMax;
        StopAllCoroutines();
    }

    void LateUpdate()
    {
        //position = leader.transform.position;
        if (GameController.instance.gameStatus != GameStatus.LIVE) return;

        position = transform.position;
        position.y = leader.transform.position.y;
        position.z = leader.transform.position.z - zOffset;
        //position.y = 0f;
        _move = (leader.transform.position.x - transform.position.x) * followSpeed;
        position.x += _move;

        transform.position = position;
    }

    public void EditZOffset(bool isForward)
    {
        if (!isForward)
        {
            StartCoroutine(ControlZOffset(zMax));
        }
        else
        {
            StartCoroutine(ControlZOffset(zMin));
        }
    }

    IEnumerator ControlZOffset(float end)
    {
        float start = zOffset;
        float timer = 0f;

        while(timer < duration)
        {
            timer += Time.deltaTime;
            zOffset = Mathf.Lerp(start, end, timer / duration);

            yield return null;
        }

        zOffset = end;
    }
}
