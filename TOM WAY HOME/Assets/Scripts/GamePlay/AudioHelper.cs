using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioHelper : MonoBehaviour
{
    private static AudioHelper instance;
    public static AudioHelper Instance
    {
        get
        {
            return instance;
        }
    }
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    public void MutePlaylist()
    {
        SoundController.Instance.musicIngameAudio.mute = true;
    }

    public void UnmutePlaylist()
    {
        SoundController.Instance.musicIngameAudio.mute = false;
    }

    public void MuteSound()
    {
        SoundController.Instance.backgroundAudio.mute = true;
    }

    public void UnmuteSound()
    {
        SoundController.Instance.backgroundAudio.mute = false;
    }
}
