﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

[Serializable]
public struct MoodInfo {
    public MoodStyle name;
    public Image Bg;
    public List<Color> colors;
    public List<Color> albedo_colors;
    public Color fog_colors;
}
public class Spawns : MonoBehaviour
{
    public static Spawns instance;

    public int cheatFirstMood;
    public List<MoodInfo> moods;
    public List<Material> materials;
    public List<Material> albedo_materials;

    public GameObject gateEndGame;
    public Platform tile;
    public Platform fakeTile;
    public Obstacle staticObs_1;
    public Obstacle staticObs_2;
    public Obstacle dynamicObs_1;
    public Obstacle dynamicObs_2;
    public Obstacle dynamicObs_3;
    public Coin coin;
    public Coin coinOnCurve;
    public Platform platformRun;
    public GameObject platformRunBasic;
    public Star star;
    public List<Platform> slideTiles;

    [HideInInspector] public int currentMood;

    private List<Platform> _listTiles = new List<Platform>();
    private List<Coin> _listCoins = new List<Coin>();
    private List<Coin> _listCoinsOnCurve = new List<Coin>();
    private List<Platform> _listPlatformRun = new List<Platform>();
    private List<GameObject> _listPlatformRunBasic = new List<GameObject>();
    private List<Platform> _listFakeTiles = new List<Platform>();
    private List<Star> _listStars = new List<Star>();

    private List<Obstacle> _listStaticObs_1 = new List<Obstacle>();
    private List<Obstacle> _listStaticObs_2 = new List<Obstacle>();
    private List<Obstacle> _listDynamicObs_1 = new List<Obstacle>();
    private List<Obstacle> _listDynamicObs_2 = new List<Obstacle>();
    private List<Obstacle> _listDynamicObs_3 = new List<Obstacle>();

    private GameController _gameController => GameController.instance;
    private PlayerController _player => PlayerController.instance;
    private List<NoteData> _mainNote => NotesManager.Instance.mainTrackNoteDatas;
    private List<NoteData> _obsNote => NotesManager.Instance.obsTrackNoteDatas;

    private int _numItem = 40;
    private float _timeToSpawn = 2.5f;

    private int _currentTileIndex;
    private int _currentCoinIndex;
    private int _currentCoinOnCurveIndex;
    private int _currentPlatformRunIndex;
    private int _currentPlatformRunBasicIndex;
    private int _currentFakeTileIndex;
    private int _currentStaticObs_1_Index;
    private int _currentStaticObs_2_Index;
    private int _currentDynamicObs_1_Index;
    private int _currentDynamicObs_2_Index;
    private int _currentDynamicObs_3_Index;
    private int _currentStar_Index;

    public int currentMainNote = 0;
    public int currentObsNote = 0;

    private Vector3 _itemPosition;

    private void Awake()
    {
        instance = this;
    }

     IEnumerator Start() {
        SetPlayerSpeedFollowBpm();
        Init();
        yield return new WaitForSeconds(0.02f);
        currentMood = GetSkinIndex(NotesManager.Instance.firtMood);
        if(cheatFirstMood >= 0) 
            currentMood = cheatFirstMood;
        SetMood(currentMood);
        CreatPlatformIntro(NotesManager.Instance.mainTrackNoteDatas[0]);
        //StartCoroutine(UIManager.instance.cloud.IntroGame());
    }

    private void SetPlayerSpeedFollowBpm()
    {
        float minValue = 1.5f;
        float value = NotesManager.Instance.speed;
        float scale = 0;

        for(int i = 2; i < 100; i += 2) {
            scale = (value * 60f) / (i * NotesManager.Instance.songBpm);
            if (scale < minValue) {
                scale = (value * 60f) / ((i - 2) * NotesManager.Instance.songBpm);
                break;
            }
        }
        //while (scale <= maxValue) {
        //    for (int i = 1; i < 100; i++) {
        //        value = (i * scale * NotesManager.Instance.songBpm) / 60f;
        //        if ((value >= RemoteConfig.instance.MinPlayerSpeed && value <= RemoteConfig.instance.MaxPlayerSpeed && NotesManager.Instance.songBpm < 120f) && i % 2 == 0) {
        //            break;
        //        } else if (value > RemoteConfig.instance.MaxPlayerSpeed && (i - 1) % 2 == 0) {
        //            value = ((i - 1) * scale * NotesManager.Instance.songBpm) / 60f;
        //            break;
        //        }
        //    }
        //    if(value >= RemoteConfig.instance.MinPlayerSpeed && value <= RemoteConfig.instance.MaxPlayerSpeed) {
        //        break;
        //    } else {
        //        scale += 0.01f;
        //    }
        //}
        //if(value > RemoteConfig.instance.MaxPlayerSpeed) {
        //    scale = minValue;
        //    for (int i = 1; i < 100; i++) {
        //        value = (i * scale * NotesManager.Instance.songBpm) / 60f;
        //        if (value > RemoteConfig.instance.MaxPlayerSpeed && (i - 1) % 2 == 0) {
        //            value = ((i - 1) * scale * NotesManager.Instance.songBpm) / 60f;
        //            break;
        //        }
        //    }
        //} else if(value < NotesManager.Instance.songBpm / 10f) {
        //    scale = minValue;
        //    for (int i = 2; i < 100; i+=2) {
        //        value = (i * scale * NotesManager.Instance.songBpm) / 60f;
        //        if (value > NotesManager.Instance.songBpm / 10f) {
        //            break;
        //        }
        //    }
        //}
        _scale = new Vector3(2.2f, 2.2f, scale);
        PlayerController.instance.playerSpeedFollowBpm = value;
        PlayerController.instance.forwardSpeed = value;
    }
    public int GetSkinIndex(MoodStyle mood) {
        if (mood == MoodStyle.Blue)
            return 0;
        if (mood == MoodStyle.Yellow)
            return 1;
        if (mood == MoodStyle.Red)
            return 2;
        return 0;
    }
    public void Init()
    {
        tile.transform.localScale = _scale;
        coin.transform.localScale = _scale * 0.5f;
        coinOnCurve.transform.localScale =  _scale * 0.5f;
        star.transform.localScale = _scale * 0.5f;
        fakeTile.transform.localScale =  _scale;
        staticObs_1.transform.localScale = _scale;
        staticObs_2.transform.localScale = _scale;
        dynamicObs_1.transform.localScale = _scale;
        dynamicObs_2.transform.localScale = _scale;
        dynamicObs_3.transform.localScale =  _scale;
        platformRun.transform.localScale = _scale;
        platformRunBasic.transform.localScale = _scale;

        foreach(var item in slideTiles) {
            item.transform.localScale = _scale * 0.7f;
        }

        GameObject gameObject = new GameObject("Items");
        for(int i = 0; i < _numItem; i++)
        {
            _listTiles.Add(Instantiate(tile));
            _listCoins.Add(Instantiate(coin));
            _listCoinsOnCurve.Add(Instantiate(coinOnCurve));
            _listStars.Add(Instantiate(star));
            _listFakeTiles.Add(Instantiate(fakeTile));
            _listStaticObs_1.Add(Instantiate(staticObs_1));
            _listStaticObs_2.Add(Instantiate(staticObs_2));
            _listDynamicObs_1.Add(Instantiate(dynamicObs_1));
            _listDynamicObs_2.Add(Instantiate(dynamicObs_2));
            _listDynamicObs_3.Add(Instantiate(dynamicObs_3));
            
            _listTiles[i].transform.SetParent(gameObject.transform);
            _listCoins[i].transform.SetParent(gameObject.transform);
            _listCoinsOnCurve[i].transform.SetParent(gameObject.transform);
            _listStars[i].transform.SetParent(gameObject.transform);
            _listFakeTiles[i].transform.SetParent(gameObject.transform);
            _listStaticObs_1[i].transform.SetParent(gameObject.transform);
            _listStaticObs_2[i].transform.SetParent(gameObject.transform);
            _listDynamicObs_1[i].transform.SetParent(gameObject.transform);
            _listDynamicObs_2[i].transform.SetParent(gameObject.transform);
            _listDynamicObs_3[i].transform.SetParent(gameObject.transform);
        }

        for(int i = 0; i < 3; i++) {
            _listPlatformRun.Add(Instantiate(platformRun));
            _listPlatformRun[i].transform.SetParent(gameObject.transform);
        }
        for (int i = 0; i < 600; i++)
        {
            _listPlatformRunBasic.Add(Instantiate(platformRunBasic));
            _listPlatformRunBasic[i].transform.SetParent(gameObject.transform);
        }
    }

    public void SpawnGate()
    {
        Vector3 position;
        NoteData lastNote = _mainNote[_mainNote.Count - 1];
        float bpmTime = 60f / NotesManager.Instance.songBpm;
        position.y = -1f;
        position.x = 0;
        position.z = _player.GetInitPosition().z + _player.forwardSpeed * (lastNote.time + 15 * 0.15f) - 5f;

        gateEndGame.transform.position = position;
        gateEndGame.SetActive(true);
    }

    public void ResetGame()
    {
        currentMainNote = 0;
        currentObsNote = 0;
        currentMood = 0;
        _currentMoodIndex = 0;
        isEnding = false;
        endRunTime = 0;
        SetMood(currentMood);
        HideAll();
        CreatPlatformIntro(NotesManager.Instance.mainTrackNoteDatas[0]);
    }

    public void HideAll()
    {
        if(!_player.rolling) StopAllCoroutines();

        foreach(var item in _listTiles)
        {
            item.gameObject.SetActive(false);
        }
        foreach (var item in _listCoins) {
            item.gameObject.SetActive(false);
        }
        foreach (var item in _listStars) {
            item.gameObject.SetActive(false);
        }
        foreach (var item in _listCoinsOnCurve) {
            item.gameObject.SetActive(false);
        }
        if (!_player.rolling)
        {
            foreach (var item in _listPlatformRun)
            {
                item.gameObject.SetActive(false);
            }
            foreach (var item in _listPlatformRunBasic)
            {
                item.gameObject.SetActive(false);
            }
        }
        if (!_player.canSlide)
            foreach (var item in slideTiles){
                item.gameObject.SetActive(false);
            }
        foreach (var item in _listFakeTiles) {
            item.gameObject.SetActive(false);
        }
        foreach (var item in _listDynamicObs_1)
        {
            item.gameObject.SetActive(false);
        }
        foreach (var item in _listDynamicObs_2)
        {
            item.gameObject.SetActive(false);
        }
        foreach (var item in _listStaticObs_1)
        {
            item.gameObject.SetActive(false);
        }
        foreach (var item in _listStaticObs_2)
        {
            item.gameObject.SetActive(false);
        }
        foreach (var item in _listDynamicObs_3) {
            item.gameObject.SetActive(false);
        }
        gateEndGame.SetActive(false);
    }

    public void GameRevive() {
        ChoseNoteDeath();

        SpawnAfterRevive();
    }
    [HideInInspector] public int noteDeath;
    [HideInInspector] public float deltaTime;
    public void ChoseNoteDeath()
    {
        if (GameController.instance.deathBy == DeathType.FAKETILE || GameController.instance.deathBy == DeathType.FALL)
        {
            for (int i = 0; i < _mainNote.Count; i++)
            {
                if (_mainNote[i].noteID == _gameController.currentNoteIDToJump && _mainNote[i].lane < 10)
                {
                    currentMainNote = i;
                    _gameController.timePlay = _mainNote[i].time;
                    noteDeath = i;
                    break;
                }
            }
            for (int i = 0; i < _obsNote.Count; i++)
            {
                if (_obsNote[i].time >= _gameController.timePlay)
                {
                    currentObsNote = i;
                    break;
                }
            }
        }
        else if (GameController.instance.deathBy == DeathType.OBS)
        {
            for (int i = 0; i < _obsNote.Count; i++)
            {
                if (_obsNote[i].time > _gameController.timePlay - 0.15f)
                {
                    for (int j = i; j < _obsNote.Count; j++)
                    {
                        if (_obsNote[j].time >= _obsNote[i].time)
                        {
                            currentObsNote = j;
                            break;
                        }
                    }
                    //currentObsNote = i + 1;
                    deltaTime = _gameController.timePlay - _obsNote[i].time;
                    _gameController.timePlay = _obsNote[i].time;
                    noteDeath = i;
                    break;
                }
            }
            for (int i = 0; i < _mainNote.Count; i++)
            {
                if (_mainNote[i].time >= _gameController.timePlay)
                {
                    if (!_player.rolling) {
                        for (int j = i - 1; j > 0; j--) {
                            if (_mainNote[j].type == ObjectType.PlatformRun) {
                                if (_mainNote[j].time + _mainNote[j].duration > _gameController.timePlay) {
                                    _player.rollingTime = 0;
                                    deltaTime = _mainNote[j].time - _gameController.timePlay;
                                    _gameController.currentNoteIDToJump = _mainNote[j].nextJumpNoteID;
                                    _player.rolling = true;
                                    Debug.LogError("deltatime 2: " + deltaTime);
                                }

                                break;
                            }
                        }
                    }
                    
                    currentMainNote = i;
                    break;
                }
            }
        }
    }
    public NoteData runNote;
    public Platform slidePlatform;
    public void SpawnAfterRevive() {
        HideAll();
        if (GameController.instance.deathBy == DeathType.OBS)
        {
            if (_player.rolling) {
                for (int i = currentMainNote; i >= 0; i--) {
                    if (_mainNote[i].type == ObjectType.PlatformRun) {
                        runNote = _mainNote[i];
                        break;
                    }
                }
            } else if (_player.canSlide) {

            }
            
        }

        for (int i = currentMainNote; i < _mainNote.Count; i++) {
            if(_gameController.timePlay + _timeToSpawn > _mainNote[i].time) {
                Spawn(true, _mainNote[i].time, _mainNote[i].lane);
            }
        }
        for (int i = currentObsNote; i < _obsNote.Count; i++) {
            if (_gameController.timePlay + _timeToSpawn > _obsNote[i].time) {
                Spawn(false, _obsNote[i].time, _obsNote[i].lane, _obsNote[i].laneAttack);
            }
        } 
    }

    private void Update()
    {
        if (GameController.instance.gameStatus != GameStatus.LIVE) return;

        CheckMood();

        if (currentMainNote < _mainNote.Count && _gameController.timePlay + _timeToSpawn > _mainNote[currentMainNote].time)
        {
            Spawn(true, _mainNote[currentMainNote].time, _mainNote[currentMainNote].lane);
        }
        if (currentObsNote < _obsNote.Count && _gameController.timePlay + _timeToSpawn > _obsNote[currentObsNote].time) {
            Spawn(false, _obsNote[currentObsNote].time, _obsNote[currentObsNote].lane, _obsNote[currentObsNote].laneAttack, _obsNote[currentObsNote].laneJump);
        }
    }

    [HideInInspector] public float endRunTime = 0;
    private void Spawn(bool isTrackMain, float time, int lane, int attackLane = 0, int jumpLane = 0)
    {
        SetItemPosition(lane, time, isTrackMain);

        if (isTrackMain)
        {
            if (lane == 40) {
                currentMainNote++;
                return; 
            }

            NoteData currentNote = _mainNote[currentMainNote];
            NoteData nextNote = currentMainNote + 1 < _mainNote.Count ? _mainNote[currentMainNote + 1] : new NoteData();

            if(currentNote.type == ObjectType.SlideTile)
            {
                int index = Random.Range(0, slideTiles.Count);

                while (slideTiles[index].canSlide)
                {
                    index = Random.Range(0, slideTiles.Count);
                }

                slideTiles[index].gameObject.SetActive(true);
                slideTiles[index].ResetPlatform();

                slideTiles[index].noteID = currentNote.noteID;
                slideTiles[index].nextJumpNoteID = currentNote.nextJumpNoteID;

                slideTiles[index].nextNoteDistance = currentNote.nextNoteDistance;
                slideTiles[index].slideTime = currentNote.duration;

                slideTiles[index].transform.position = _itemPosition;

                slidePlatform = slideTiles[index];
            } else if (currentNote.type == ObjectType.NormalTile || IsDynamicTile(currentNote))//Tile
            { 
                _listTiles[_currentTileIndex].gameObject.SetActive(true);
                

                _listTiles[_currentTileIndex].nextNoteDistance = currentNote.nextNoteDistance;
                _listTiles[_currentTileIndex].noteID = currentNote.noteID;
                _listTiles[_currentTileIndex].nextJumpNoteID = currentNote.nextJumpNoteID;
                _listTiles[_currentTileIndex].velocity = currentNote.velocity;
                _listTiles[_currentTileIndex].time = currentNote.time;

                if(currentNote.Lenght > 48) { //Spawn curve coin
                    //Debug.LogError("make coin curve");
                    MakeCurveCoin(currentNote, nextNote);
                }

                _listTiles[_currentTileIndex].name = "PLATFORM_" + currentMainNote.ToString();

                _listTiles[_currentTileIndex].transform.position = _itemPosition;
                _listTiles[_currentTileIndex].ResetPlatform();
                SetTypeOfPlatform(currentNote);

                _currentTileIndex++;
                if (_currentTileIndex >= _numItem) _currentTileIndex = 0;
            } else if (currentNote.type == ObjectType.Coin || currentNote.type == ObjectType.CoinEndgame) //Coin
            {
                _listCoins[_currentCoinIndex].gameObject.SetActive(true);
                _listCoins[_currentCoinIndex].ResetCoin();
                if (currentNote.type == ObjectType.CoinEndgame)
                    _listCoins[_currentCoinIndex].isCoinEndgame = true;

                _listCoins[_currentCoinIndex].transform.position = _itemPosition;

                _currentCoinIndex++;
                if (_currentCoinIndex >= _numItem) _currentCoinIndex = 0;
            } else if (currentNote.type == ObjectType.PlatformRun) {
                //Debug.LogError("CreatePlatformRun");
                endRunTime = currentNote.time + currentNote.duration;
                StartCoroutine(CreatePlatformRun(currentNote));
            } else if (currentNote.type == ObjectType.Star) //Coin
            {
                _listStars[_currentStar_Index].gameObject.SetActive(true);
                _listStars[_currentStar_Index].ResetStar();

                _listStars[_currentStar_Index].transform.position = _itemPosition;

                _currentStar_Index++;
                if (_currentStar_Index >= _numItem) _currentStar_Index = 0;
            }

            //_currentTileIndex++;
            //if (_currentTileIndex >= _numItem) _currentTileIndex = 0;
            currentMainNote++;
        }
        else
        {
            NoteData obsNote = _obsNote[currentObsNote];

            if (GameController.instance.gameStatus == GameStatus.CONTINUE && obsNote.type == ObjectType.FakeTile && _player.rolling) { // chuyen faketile ve platformrun
                _listPlatformRun[_currentPlatformRunIndex].transform.position = _itemPosition;
                _listPlatformRun[_currentPlatformRunIndex].gameObject.SetActive(true);

                _currentPlatformRunIndex++;
                if (_currentPlatformRunIndex >= _numItem) _currentPlatformRunIndex = 0;
            }else if (GameController.instance.gameStatus == GameStatus.LIVE)
            {
                //NoteData obsNote = _obsNote[currentObsNote];

                if (obsNote.type == ObjectType.FakeTile && _gameController.gameStatus != GameStatus.CONTINUE) {
                    _listFakeTiles[_currentFakeTileIndex].gameObject.SetActive(true);
                    _listFakeTiles[_currentFakeTileIndex].transform.position = _itemPosition;
                    _listFakeTiles[_currentFakeTileIndex].time = time;
                    _listFakeTiles[_currentFakeTileIndex].CheckTimingFakeTile();

                    _currentFakeTileIndex++;
                    if (_currentFakeTileIndex >= _numItem) _currentFakeTileIndex = 0;
                } else if (obsNote.type == ObjectType.StaticObs_1)
                {
                    _listStaticObs_1[_currentStaticObs_1_Index].transform.position = _itemPosition;
                    _listStaticObs_1[_currentStaticObs_1_Index].gameObject.SetActive(true);
                    _listStaticObs_1[_currentStaticObs_1_Index].ResetObs();

                    _currentStaticObs_1_Index++;
                    if (_currentStaticObs_1_Index >= _numItem) _currentStaticObs_1_Index = 0;
                }
                else if (obsNote.type == ObjectType.StaticObs_2)
                {
                    _listStaticObs_2[_currentStaticObs_2_Index].transform.position = _itemPosition;
                    _listStaticObs_2[_currentStaticObs_2_Index].gameObject.SetActive(true);
                    _listStaticObs_2[_currentStaticObs_2_Index].time = obsNote.time;
                    _listStaticObs_2[_currentStaticObs_2_Index].ResetObs();

                    _currentStaticObs_2_Index++;
                    if (_currentStaticObs_2_Index >= _numItem) _currentStaticObs_2_Index = 0;
                }
                else if (obsNote.type == ObjectType.DynamicObs_1)
                {
                    _listDynamicObs_1[_currentDynamicObs_1_Index].transform.position = _itemPosition;
                    _listDynamicObs_1[_currentDynamicObs_1_Index].time = obsNote.time;
                    _listDynamicObs_1[_currentDynamicObs_1_Index].gameObject.SetActive(true);
                    _listDynamicObs_1[_currentDynamicObs_1_Index].ResetObs();

                    _currentDynamicObs_1_Index++;
                    if (_currentDynamicObs_1_Index >= _numItem) _currentDynamicObs_1_Index = 0;
                }
                else if (obsNote.type == ObjectType.DynamicObs_2)
                {
                    _listDynamicObs_2[_currentDynamicObs_2_Index].time = time;
                    _listDynamicObs_2[_currentDynamicObs_2_Index].isJumpLeft = lane > jumpLane ? true : false;
                    _listDynamicObs_2[_currentDynamicObs_2_Index].transform.position = _itemPosition;
                    _listDynamicObs_2[_currentDynamicObs_2_Index].gameObject.SetActive(true);
                    _listDynamicObs_2[_currentDynamicObs_2_Index].ResetObs();


                    _currentDynamicObs_2_Index++;
                    if (_currentDynamicObs_2_Index >= _numItem) _currentDynamicObs_2_Index = 0;
                }
                else if ((obsNote.type == ObjectType.DynamicObs_3))
                {

                    _listDynamicObs_3[_currentDynamicObs_3_Index].time = time;
                    _listDynamicObs_3[_currentDynamicObs_3_Index].isAttackLeft = lane > attackLane ? true : false;
                    _listDynamicObs_3[_currentDynamicObs_3_Index].transform.position = _itemPosition;
                    _listDynamicObs_3[_currentDynamicObs_3_Index].gameObject.SetActive(true);
                    _listDynamicObs_3[_currentDynamicObs_3_Index].ResetObs();


                    _currentDynamicObs_3_Index++;
                    if (_currentDynamicObs_3_Index >= _numItem) _currentDynamicObs_2_Index = 0;
                }
            }

            currentObsNote++;
        }
    }

    private void SetItemPosition(int lane, float time, bool isMainTrack = false)
    {
        if(_gameController.gameStatus == GameStatus.CONTINUE && isMainTrack)
        {
            _itemPosition.x = 0;
        }
        else
        {
            _itemPosition.x = lane * (_scale.x / 2f);
        }
        
        _itemPosition.y = 0f;
        _itemPosition.z = _player.GetInitPosition().z + _player.forwardSpeed * time;
    }
    private void SetTypeOfPlatform(NoteData note)
    {
        if(IsDynamicTile(note) && GameController.instance.gameStatus != GameStatus.CONTINUE)
        {
            _listTiles[_currentTileIndex].isDynamicTile = true;
            if (note.type == ObjectType.DynamicTileWithCoin)
            {
                _listTiles[_currentTileIndex].coin.gameObject.SetActive(true);
                _listTiles[_currentTileIndex].coin.ResetCoin();
            }
            if (note.type == ObjectType.DynamicTileWithStar)
            {
                _listTiles[_currentTileIndex].star.gameObject.SetActive(true);
                _listTiles[_currentTileIndex].star.ResetStar(); ;
            }
        } else if(note.velocity > 90)
        {
            _listTiles[_currentTileIndex].isStrongNote = true;
        }
    }

    private bool IsDynamicTile(NoteData note)
    {
        return note.type == ObjectType.DynamicTile || note.type == ObjectType.DynamicTileWithCoin || note.type == ObjectType.DynamicTileWithStar;
    }
    public Platform GetPlatformJumpByName(string name)
    {
        foreach (var pl in _listTiles)
        {
            if (pl.name == name && pl.gameObject.activeSelf)
            {
                return pl;
            }
        }

        return null;
    }
    public Platform GetPlatformRunByName(string name)
    {
        foreach (var pl in _listPlatformRun)
        {
            if (pl.name == name && pl.gameObject.activeSelf)
            {
                return pl;
            }
        }

        return null;
    }

    public Platform GetPlatformSlideByName(string name)
    {
        foreach (var pl in slideTiles)
        {
            if (pl.name == name)
            {
                return pl;
            }
        }

        return null;
    }

    private void MakeCurveCoin(NoteData currentNote, NoteData nextNote) {

        List<Vector3> listPositions = GetCoinsPositionOnCurve(currentNote, nextNote);

        for(int i = 0; i < listPositions.Count; i++) {
            _listCoinsOnCurve[_currentCoinOnCurveIndex].transform.position = listPositions[i];
            _listCoinsOnCurve[_currentCoinOnCurveIndex].gameObject.SetActive(true);
            _listCoinsOnCurve[_currentCoinOnCurveIndex].ResetCoin();
            _listCoinsOnCurve[_currentCoinOnCurveIndex].isCoinOnCurve = true;

            _currentCoinOnCurveIndex++;
            if (_currentCoinOnCurveIndex >= _numItem) _currentCoinOnCurveIndex = 0;
        }
    }
    private List<Vector3> GetCoinsPositionOnCurve(NoteData currentNote, NoteData nextNote) {

        Vector3 currentPlatformPos = new Vector3(currentNote.lane * (_scale.x / 2f), 0f, _player.GetInitPosition().z + _player.forwardSpeed * currentNote.time);
        Vector3 nextPlatformPos = new Vector3(nextNote.lane * (_scale.x / 2f), 0f, _player.GetInitPosition().z + _player.forwardSpeed * nextNote.time);

        if(_gameController.gameStatus == GameStatus.CONTINUE)
        {
            currentPlatformPos.x = 0f;
            nextPlatformPos.x = 0;
        }

        float x1 = currentPlatformPos.x;
        float x2 = nextPlatformPos.x;

        float h = PlayerController.instance.GetJumpHeight(currentNote.time);
        float z1 = currentPlatformPos.z;
        float z2 = nextPlatformPos.z;
        float z3 = (z1 + z2) / 2;

        float a = h / (z3 - z1) / (z3 - z2);
        float b = -(z1 + z2) * a;
        float c = -a * z1 * z1 - b * z1;
        // y = az2 + bz + c

        float posZEndDiamond =
            currentPlatformPos.z + currentNote.nextNoteDistance * _player.forwardSpeed;
        float stepDiamond = _scale.z; //distance
        int totalDiamond = Mathf.FloorToInt((posZEndDiamond - z1) / stepDiamond);

        List<Vector3> positions = new List<Vector3>();

        for (int index = 0; index < totalDiamond; index++) {
            float posZ = z1 + index * stepDiamond; 
            float y =  a * posZ * posZ + b * posZ + c;

            Vector3 position = new Vector3();
            position.x = x1 + (x2 - x1) * (posZ - z1) / (z2 - z1);
            position.y = y;
            position.z = posZ;

            if (y > 0) {
                positions.Add(position);
            }
        }

        return positions;
    }
    [HideInInspector] public Vector3 _scale;
    public IEnumerator CreatePlatformRun(NoteData note, bool isContinue = false) {
        bool isPlatformRunEndgame = note.time > NotesManager.Instance.mainTrackNoteDatas[NotesManager.Instance.mainTrackNoteDatas.Count - 1].time - 5f;

        int numberRow = Mathf.RoundToInt(note.duration * _player.forwardSpeed / _scale.z);
        //float scaleZ = (note.duration * _player.forwardSpeed - numberRow * _scale) / numberRow;
        //Vector3 scale = new Vector3(_scale, _scale, _scale + scaleZ);

        //Debug.LogError(numberRow * 3);
        Vector3 position = new Vector3();
        float z = _player.GetInitPosition().z + _player.forwardSpeed * note.time;
        float timeSpawnEachRow = 0.5f / _player.forwardSpeed;

        if (GameController.instance.gameStatus != GameStatus.LIVE) timeSpawnEachRow = 0f;

        if(isPlatformRunEndgame) 
            StartCoroutine(ToWalk());

        for (int row = 0; row < numberRow; row++) {
            for (int column = 0; column < 3; column++) {
                position.z = z + row * (_scale.z);
                position.y = 0f;
                position.x = (column - 1) * _scale.x;

                if (row == 0)
                {
                    _listPlatformRun[_currentPlatformRunIndex].transform.position = position;
                    //_listPlatformRun[_currentPlatformRunIndex].transform.localScale = scale;
                    _listPlatformRun[_currentPlatformRunIndex].gameObject.SetActive(true);
                    _listPlatformRun[_currentPlatformRunIndex].ResetPlatform();
                    _listPlatformRun[_currentPlatformRunIndex].duration = note.duration;
                    _listPlatformRun[_currentPlatformRunIndex].noteID = note.noteID;
                    _listPlatformRun[_currentPlatformRunIndex].nextJumpNoteID = note.nextJumpNoteID;
                    _listPlatformRun[_currentPlatformRunIndex].nextNoteDistance = note.nextNoteDistance;
                    _listPlatformRun[_currentPlatformRunIndex].name = "PLATFORM_RUN_" + (_currentPlatformRunIndex).ToString();


                    _currentPlatformRunIndex++;
                    if (_currentPlatformRunIndex >= _listPlatformRun.Count)
                        _currentPlatformRunIndex = 0;
                }
                else
                {
                    _listPlatformRunBasic[_currentPlatformRunBasicIndex].transform.position = position;
                    _listPlatformRunBasic[_currentPlatformRunBasicIndex].gameObject.SetActive(true);

                    _currentPlatformRunBasicIndex++;
                    if (_currentPlatformRunBasicIndex >= _listPlatformRunBasic.Count)
                        _currentPlatformRunBasicIndex = 0;
                }
            }
            yield return new WaitForSeconds(timeSpawnEachRow);
        }
    }
    [HideInInspector] public bool isEnding;
    IEnumerator ToWalk() {
        //Debug.LogError("to walk");
        yield return new WaitForSeconds(NotesManager.Instance.row * 0.15f + 3f);
        isEnding = true;
        PlayerController.instance.Walk();
    }
    public void CreatPlatformIntro(NoteData note) {
        int numberRow = (int)(note.time * _player.forwardSpeed / _scale.z);
        float scaleZ = (note.time * _player.forwardSpeed - numberRow * _scale.z) / numberRow;
        Vector3 scale = new Vector3(_scale.x, _scale.y, _scale.z + scaleZ);
        //Debug.LogError(numberRow * 3);
        Vector3 position;
        float z = _player.GetInitPosition().z;

        for (int row = 0; row < numberRow; row++)
        {
            for (int column = 0; column < 3; column++)
            {
                position.z = z + row * (_scale.z + scaleZ);
                position.y = 0f;
                position.x = (column - 1) * _scale.x;

                if (row == 0)
                {
                    _listPlatformRun[_currentPlatformRunIndex].transform.position = position;
                    _listPlatformRun[_currentPlatformRunIndex].transform.localScale = scale;
                    _listPlatformRun[_currentPlatformRunIndex].gameObject.SetActive(true);
                    _listPlatformRun[_currentPlatformRunIndex].ResetPlatform();
                    _listPlatformRun[_currentPlatformRunIndex].duration = note.time;
                    _listPlatformRun[_currentPlatformRunIndex].noteID = -1;
                    _listPlatformRun[_currentPlatformRunIndex].nextJumpNoteID = 0;
                    _listPlatformRun[_currentPlatformRunIndex].nextNoteDistance = 0;
                    _listPlatformRun[_currentPlatformRunIndex].name = "PLATFORM_RUN_" + (_currentPlatformRunIndex).ToString();


                    _currentPlatformRunIndex++;
                    if (_currentPlatformRunIndex >= _listPlatformRun.Count)
                        _currentPlatformRunIndex = 0;
                }
                else
                {
                    _listPlatformRunBasic[_currentPlatformRunBasicIndex].transform.position = position;
                    _listPlatformRunBasic[_currentPlatformRunBasicIndex].transform.localScale = scale;
                    _listPlatformRunBasic[_currentPlatformRunBasicIndex].gameObject.SetActive(true);

                    _currentPlatformRunBasicIndex++;
                    if (_currentPlatformRunBasicIndex >= _listPlatformRunBasic.Count)
                        _currentPlatformRunBasicIndex = 0;
                }
            }
        }
    }

    private int _currentMoodIndex = 0;
    private void CheckMood() {
        if(_currentMoodIndex < NotesManager.Instance.moodChangeTimes.Count &&
            _gameController.timePlay >= NotesManager.Instance.moodChangeTimes[_currentMoodIndex].time) {

            int nextMood = GetSkinIndex(NotesManager.Instance.moodChangeTimes[_currentMoodIndex].mood);
            if (currentMood != nextMood)
            {
                VFXController.Instance.PlayMoodchangeVFX();
                StartCoroutine(ChangeMood(currentMood, nextMood));
            }
            _currentMoodIndex++;
        }
    }

    private IEnumerator ChangeMood(int from, int to, float duration = 1f) {
        float timer = 0;
        Color c;
        Color bgwhite = Color.white;
        moods[to].Bg.gameObject.SetActive(true);
        while (timer < duration) {
            timer += Time.deltaTime;
            int i = 0;

            foreach(var material in materials) {
                c = Color.Lerp(moods[from].colors[i], moods[to].colors[i], timer / duration);
                material.SetColor("_EmissionColor", c);
                i++;
            }
            i = 0;
            foreach (var material in albedo_materials) {
                c = Color.Lerp(moods[from].albedo_colors[i], moods[to].albedo_colors[i], timer / duration);
                material.SetColor("_Color", c);
                i++;
            }

            c = Color.Lerp(moods[from].fog_colors, moods[to].fog_colors, timer / duration);
            RenderSettings.fogColor = c;

            bgwhite.a = Mathf.Lerp(1f, 0f, timer / duration);
            moods[from].Bg.color = bgwhite;
            bgwhite.a = Mathf.Lerp(0f, 1f, timer / duration);
            moods[to].Bg.color = bgwhite;

            yield return null;
        }
        moods[from].Bg.gameObject.SetActive(false);
        currentMood = to;
    }

    private void SetMood(int index) {
        int i = 0;
        foreach (var material in materials) {
            material.SetColor("_EmissionColor", moods[index].colors[i]);
            i++;
        }
        i = 0;
        foreach (var material in albedo_materials) {
            material.SetColor("_Color", moods[index].albedo_colors[i]);
            i++;
        }
        RenderSettings.fogColor = moods[index].fog_colors;
        foreach(var mood in moods) {
            mood.Bg.gameObject.SetActive(false);
        }
        moods[index].Bg.gameObject.SetActive(true);
        Color c = Color.white;
        moods[index].Bg.color = c;
    }
}
