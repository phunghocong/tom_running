using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentGamePlay : MonoBehaviour
{
    public List<ParticleSystem> treeVFXs;

    public void PlayStrongNote()
    {
        foreach(var p in treeVFXs)
        {
            p.Play();
            StartCoroutine(VibrateTree(p.transform.parent.gameObject));
        }
    }
    IEnumerator VibrateTree(GameObject gameObject, float duration = 0.2f)
    {
        float timer = 0;
        float vtimer = 0.05f;
        int z = 1;
        while(timer < duration)
        {
            timer += Time.deltaTime;
            vtimer += Time.deltaTime;

            if (vtimer >= 0.05f)
            {
                z = -z;
                LeanTween.rotateX(gameObject, z * 7f, 0.05f);
                vtimer = 0f;
            }

            yield return null;
        }
    }
}
