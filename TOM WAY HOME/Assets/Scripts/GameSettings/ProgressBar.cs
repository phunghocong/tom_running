using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    public Image progress;
    public List<Transform> stars;
    public List<Image> starImages;
    [SerializeField] private Sprite starGray;
    [SerializeField] private Sprite starGlow;
    [Space] [SerializeField] private AnimationCurve curveX;
    [SerializeField] private AnimationCurve curveY;
    [SerializeField] private AnimationCurve curveScale;
    private float TimeFlyStarToProgressBar = 1f;
    [SerializeField] private Transform[] points;

    public Transform starPrefab;
    [SerializeField] Transform starParent;

    public Camera cam3D;
    public Camera cam2D;

    private float lastTime;
    public RectTransform progressBar;
    [SerializeField]
    private Image starImage;
    [SerializeField] private RectTransform progressPoint;

    private void OnEnable()
    {
       
        progress.fillAmount = 0f;
        for (int i=0;i<stars.Count;i++)
        {
            starImages[i].sprite = starGray;
            starImages[i].SetNativeSize();
        }
    }

    public void InitCamera()
    {
        cam3D = GameController.instance.cameraMain;
        cam2D = PopupController.Instance.cameraUI;
    }
    float amount;
    private void Update() {
        if (GameController.instance.gameStatus != GameStatus.LIVE) return;

        amount = GameController.instance.timePlay / lastTime;
        amount = amount > 1 ? 1 : amount;
        progress.fillAmount = amount;
        progressPoint.anchoredPosition = Vector2.right * (-245f + 490f * amount);
    }
    public void SetStarsPosition(List<float> starTimes) {
        lastTime = NotesManager.Instance.mainTrackNoteDatas[NotesManager.Instance.mainTrackNoteDatas.Count - 1].time;
        //Vector3 position = Vector3.zero;
        float progressBarLength = progressBar.rect.width;
        float[] positions = new float[starTimes.Count];
        for (int i = 0; i < stars.Count; i++) {
            positions[i] = Mathf.Lerp(0f, progressBarLength, (starTimes[i] / lastTime)) - progressBarLength / 2;
            stars[i].localPosition = new Vector3(positions[i], 0f, 0f); ;
            points[i].localPosition = stars[i].localPosition;
        }
    }
    public void UpdateStar() {
        starImages[GameController.instance.stars - 1].sprite = starGlow;
        starImages[GameController.instance.stars - 1].SetNativeSize();
    }

    public void MoveStar(Vector3 position,int star) {
        Vector3 screenPosition = cam3D.WorldToScreenPoint(position);
        screenPosition.y = Screen.height - screenPosition.y;
        screenPosition.x = Screen.width - screenPosition.x;
        Vector3 worldPosition2D = cam2D.ScreenToWorldPoint(screenPosition);
        worldPosition2D.z = 0f;
        worldPosition2D.y = -screenPosition.y;
        starPrefab.localPosition = worldPosition2D;
        starPrefab.gameObject.SetActive(true);
        Vector3 targetPosition = stars[star - 1].position;
        starPrefab.DOScale(Vector3.one, 1).SetEase(curveScale);
        starPrefab.transform.DOMove(targetPosition, TimeFlyStarToProgressBar).SetEase(curveY).OnComplete(() =>
        {
            starPrefab.gameObject.SetActive(false);
            UpdateStar();
        });

        //if (cam == null || rtfStarFly == null || stars <= 0 || stars >= 4) {
        //    return;
        //}

        //Vector3 worldToScreenPoint = cam.WorldToScreenPoint(position);
        //worldToScreenPoint.x -= _mainPixelWidth;
        //worldToScreenPoint.y -= _mainPixelHeight;

        ////from
        //rtfStarFly.anchoredPosition = worldToScreenPoint;
        //rtfStarFly.gameObject.SetActive(true);

        ////to

        //if (stars > 2)
        //{ //=3
        //    rtfStarFly.DOAnchorPosX(targetPosition.x, TimeFlyStarToProgressBar);
        //}
        //else
        //{
        //    rtfStarFly.DOAnchorPosX(targetPosition.x, TimeFlyStarToProgressBar).SetEase(curveX);
        //}

        //rtfStarFly.DOAnchorPosY(targetPosition.y, TimeFlyStarToProgressBar).SetEase(curveY).OnComplete(() => {
        //    rtfStarFly.gameObject.SetActive(false);
        //    UpdateStar();
        //});

        StartCoroutine(OpacityStarFly(0.8f));
    }
    IEnumerator OpacityStarFly(float timeDelay)
    {
        starImage.color = Color.white;

        yield return new WaitForSeconds(timeDelay);

        float t = 0f;
        float timer = 1f - timeDelay;
        Color c = starImage.color;
        while (t < timer)
        {
            t += Time.deltaTime;
            c.a = Mathf.Lerp(1f, 0.3f, t / timer);
            starImage.color = c;
            yield return null;
        }
    }
    public void ResetGame() {
        for (int i = 0; i < stars.Count; i++)
        {
            starImages[i].sprite = starGray;
            starImages[i].SetNativeSize();
        }
        progress.fillAmount = 0;
        progressPoint.anchoredPosition = Vector2.right * (-245f + 490f * (GameController.instance.timePlay / lastTime));
    }
}
