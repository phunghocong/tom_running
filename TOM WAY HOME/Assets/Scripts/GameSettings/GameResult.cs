﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameResult : Popup
{
    [SerializeField] private Button noThankBtn;
    [SerializeField] private Button AdsXCoinBtn;
    [SerializeField] private GameObject AdsXCoinIcon;
    [SerializeField] private RectTransform XCoinIcon;
    [SerializeField] private Button HomeBtn;
    [SerializeField] private Button ReplayBtn;
    [SerializeField] private Button NextBtn;
    [SerializeField] private PointXCoin pointXCoin;
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI highestScoreText;
    [SerializeField] private TextMeshProUGUI coinText;
    [SerializeField] private TextMeshProUGUI plusCoinText;
    [SerializeField] private TextMeshProUGUI XCoinText;
    [SerializeField] private TextMeshProUGUI Songname;
    [SerializeField] private List<GameObject> starColors = new List<GameObject>();

    private int coinPlus;

    [SerializeField] private GameObject adsImg;
    [SerializeField] GameObject noadsIcon;
    [SerializeField] GameObject noadsIconAdsBtn;

    //Onboarding
    [SerializeField] GameObject blackBG;
    [SerializeField] GameObject tooltipXCoin;
    [SerializeField] GameObject tooltipNext;


    protected override void BeforeShow()
    {
        base.BeforeShow();
      
    }
    public override void Show()
    {
        base.Show();
        plusCoinText.text = "+" + GameController.instance.coins.ToString();
        pointXCoin.canMove = true;
        isPlayNext = false;
        UpdateDatas();
    }

    private void OnDisable()
    {
        if(GameController.instance != null)
        {
            GameData.SetBool("IsTutorial", false);
        }
    }
    private void UpdateDatas() {
        if (GameController.instance.isTutorial)
        {
            AdsXCoinIcon.gameObject.SetActive(false);
            Vector2 pos = XCoinIcon.anchoredPosition;
            pos.x = -20f;
            XCoinIcon.anchoredPosition = pos;
            blackBG.SetActive(true);
            blackBG.transform.SetAsLastSibling();
            AdsXCoinBtn.transform.SetParent(blackBG.transform);
            tooltipXCoin.SetActive(true);
        }
        else
        {
            AdsXCoinIcon.gameObject.SetActive(true);
            Vector2 pos = XCoinIcon.anchoredPosition;
            pos.x = 20f;
            XCoinIcon.anchoredPosition = pos;
            blackBG.SetActive(false);
            tooltipXCoin.SetActive(false);
            tooltipNext.SetActive(false);
        }
        //GameData.SetBool("IsTutorial", false);

        var playlist = NotesManager.Instance.playlist;
        isClick = false;
        UserDatas.PlusCoin(GameController.instance.coins);
        coinText.text = UserDatas.GetCoin().ToString();
        scoreText.text = GameController.instance.score.ToString();
        CheckHighestScore();
        Songname.text = NotesManager.Instance.songname;

        foreach(var star in starColors) {
            star.SetActive(false);
        }
        for(int i = 0; i < GameController.instance.stars; i++) {
            starColors[i].SetActive(true);
        }

        GameController.instance.gameStatus = GameStatus.DIE;
        var playList = NotesManager.Instance.playlist;
        noadsIcon.SetActive(false);
        noadsIconAdsBtn.SetActive(false);
        int songid = GameData.CurrentSongIndex + 1;
        songid = songid >= playlist.Count ? 0 : songid;
        if (playList[songid].isPlayAds && !playList[songid].isClickAds)
        {
            adsImg.SetActive(true);
        }
        else
        {
            adsImg.SetActive(false);
        }
    }

    private void CheckHighestScore()
    {
        if (GameController.instance.score > GameData.GetHighScore(GameData.CurrentSongIndex))
        {
            GameData.SetHighScore(GameData.CurrentSongIndex, GameController.instance.score);
        }
        highestScoreText.text = GameData.GetHighScore(GameData.CurrentSongIndex).ToString();
    }

    public void GoHome() {
        GameController.instance.ReturnHome();
    }
    public void Replay() {
        GameController.instance.GameReplay();
        Hide();
    }
    bool isPlayNext;
    public void PlayNextSong() {
        var playlist = NotesManager.Instance.playlist;
        int songid = GameData.CurrentSongIndex + 1;
        songid = songid >= playlist.Count? 0 : songid;
        if (playlist[songid].isPlayAds && !playlist[songid].isClickAds)
        {
            if (AdsMediationController.Instance.IsRewardedAvailable && Application.internetReachability != NetworkReachability.NotReachable)
            {
                Debug.Log("showw ad reward");
                AudioHelper.Instance.MutePlaylist();
                Time.timeScale = 0;
                AdsMediationController.Instance.ShowRewardedAds("play_song", OnRewardedAdsFailedToShow, OnRewardedAdsClosed);
            }
            else
            {
                OnShowAdsFail();
            }
        }
        else
        {
            NextSong();
        }

    }
    public void OnShowAdsFail()
    {
        noadsIcon.SetActive(true);
        noadsIcon.transform.localScale = Vector3.zero;
        noadsIcon.transform.DOScale(1f, 0.5f).SetEase(Ease.OutQuad);
        DOTween.Sequence()
            .AppendInterval(1f) // Chờ 0.5s
            .Append(noadsIcon.transform.DOScale(0f, 0.5f).SetEase(Ease.OutQuad));
    }
    public void OnShowAdsFailAdsBtn()
    {
        noadsIconAdsBtn.SetActive(true);
        noadsIconAdsBtn.transform.localScale = Vector3.zero;
        noadsIconAdsBtn.transform.DOScale(1f, 0.5f).SetEase(Ease.OutQuad);
        DOTween.Sequence()
            .AppendInterval(1f) // Chờ 0.5s
            .Append(noadsIconAdsBtn.transform.DOScale(0f, 0.5f).SetEase(Ease.OutQuad));
    }

    private void OnRewardedAdsClosed(bool success)
    {
        if (success)
        {
            AudioHelper.Instance.UnmutePlaylist();
            Time.timeScale = 1;
            NextSong();
        }
    }
    public void NextSong()
    {
        if (!isPlayNext)
        {
            isPlayNext = true;
            GameData.CurrentSongIndex++;
            if (GameData.CurrentSongIndex >= NotesManager.Instance.playlist.Count)
                GameData.CurrentSongIndex = 0;
            if (blackBG.activeSelf)
            {
                NextBtn.transform.SetParent(transform);
                blackBG.transform.gameObject.SetActive(false);
                tooltipNext.SetActive(false);
            }
            Hide();
            PopupController.Instance.Show<PopupLoading>();
        }
    }

    private void OnRewardedAdsFailedToShow()
    {
        AudioHelper.Instance.UnmuteSound();
        AudioHelper.Instance.UnmutePlaylist();
        Debug.Log("show ads fail");
    }
    public void UpdateXCoinText(int X) {
        coinPlus = GameController.instance.coins * (X - 1);
        XCoinText.text = (GameController.instance.coins * X).ToString();
    }
    bool isClick;
    public void StopMovePointXCoin(bool success) {
        if (!isClick)
        {
            if (success)
            {
                if (!pointXCoin.canMove) return;

                pointXCoin.canMove = false;
                UserDatas.PlusCoin(coinPlus);
                coinText.text = UserDatas.GetCoin().ToString();
                isClick= true;
                if (blackBG.activeSelf)
                {
                    AdsXCoinBtn.transform.SetParent(transform);
                    blackBG.transform.SetAsLastSibling();
                    tooltipXCoin.SetActive(false);
                    tooltipNext.SetActive(true);
                    NextBtn.transform.SetParent(blackBG.transform);
                }
            }
        }
    }

    IEnumerator GotoHome() {
        yield return new WaitForSeconds(0.5f);
        SceneController.Instance.LoadScene("Home");
        HomeController.instance.ReturnHome();
    }

}
