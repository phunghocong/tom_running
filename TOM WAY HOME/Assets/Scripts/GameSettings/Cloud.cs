using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cloud : MonoBehaviour
{
    public RectTransform topRight;
    public RectTransform topLeft;
    public RectTransform bottomRight;
    public RectTransform bottomLeft;

    public Vector3 topRightRtf;
    public Vector3 topLeftRtf;
    public Vector3 bottomRightRtf;
    public Vector3 bottomLeftRtf;

    private float timeTransition = 1f;
    private Vector3 initialPosition1, initialPosition2, initialPosition3, initialPosition4;
    private void OnEnable()
    {
        initialPosition1 = topLeft.transform.position;
        initialPosition2 = topRight.transform.position;
        initialPosition3 = bottomLeft.transform.position;
        initialPosition4 = bottomRight.transform.position;
    }
    public IEnumerator IntroGame() {

        topRight.gameObject.SetActive(true);
        topLeft.gameObject.SetActive(true);
        bottomRight.gameObject.SetActive(true);
        bottomLeft.gameObject.SetActive(true);

        LeanTween.move(topRight, topRightRtf, timeTransition);
        LeanTween.move(topLeft, topLeftRtf, timeTransition);
        LeanTween.move(bottomRight, bottomRightRtf, timeTransition);
        LeanTween.move(bottomLeft, bottomLeftRtf, timeTransition);

        yield return new WaitForSeconds(timeTransition + 0.2f);

        GamePlayPopup.instance.GameUIActionStatus(true);
    }

    private void ResetToInitialPosition(Transform transform, Vector2 initialPosition)
    {
        transform.position = initialPosition;
    }
    public void ResetTransform()
    {
        ResetToInitialPosition(topLeft, initialPosition1);
        ResetToInitialPosition(topRight, initialPosition2);
        ResetToInitialPosition(bottomLeft, initialPosition3);
        ResetToInitialPosition(bottomRight, initialPosition4);
    }
}
