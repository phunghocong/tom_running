using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseUI : Popup
{
    public Button Resume;
    public Button Quit;

   
    public override void Show()
    {
        base.Show();
        Resume.onClick.AddListener(GameResume);
        Quit.onClick.AddListener(QuitGame);
    }
    private void GameResume() {
        Close();
        GameController.instance.GameResume();
    }
    private void QuitGame() {
        //Goto Homescene
        GameController.instance.ReturnHome();
       
    }
    public void Close()
    {
        base.Hide();
    }

}
