using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointXCoin : MonoBehaviour
{
    public bool canMove;
    public RectTransform rTf;
    private Vector2 position;
    public GameResult gameResult;
    private void OnEnable() {
        position = rTf.anchoredPosition;
        position.x = 0;
        rTf.anchoredPosition = position;
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("XCoin")) {
            int value = other.GetComponent<XCoin>().value;
            gameResult.UpdateXCoinText(value);
        }
    }
    //private void OnTriggerExit(Collider other) {
    //    if (other.CompareTag("XCoin")) {
    //        int value = other.GetComponent<XCoin>().value;
    //        Debug.LogError(value);
    //    }
    //}
    private void Update() {
        if (canMove) {
            position.x = Mathf.PingPong(Time.time * 800f, 904f) - 452f;
            rTf.anchoredPosition = position;
        }
        //Debug.LogError(rTf.anchoredPosition);
    }
}
