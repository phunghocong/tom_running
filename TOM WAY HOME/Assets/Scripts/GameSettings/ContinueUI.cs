﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ContinueUI : Popup
{
    [SerializeField] private TMP_Text number;
    [SerializeField] private Image circle;
    [SerializeField] private Button continueBtn;
    [SerializeField] private Button noThankBtn;
    [SerializeField] GameObject noadsIcon;
    [SerializeField] GameObject adsIcon;
    [SerializeField] RectTransform textRevive;


    public override void Show()
    {
        base.Show();
        if (!GameController.instance.isTutorial) {
            StartCoroutine(CountDown());
            adsIcon.SetActive(true);
            Vector2 pos = textRevive.anchoredPosition;
            pos.x = 34f;
            textRevive.anchoredPosition = pos;
        }
        else
        {
            number.text = "0";
            circle.fillAmount = 0f;
            adsIcon.SetActive(false);
            Vector2 pos = textRevive.anchoredPosition;
            pos.x = 0f;
            textRevive.anchoredPosition = pos;
        }
        noadsIcon.SetActive(false);
        noThankBtn.gameObject.SetActive(false);
    }

    IEnumerator CountDown() {
        number.text = "5";
        circle.fillAmount = 1f;
        float timer = 0f;

        while(timer < 5f) {
            timer += Time.deltaTime;
            number.text = (5 - (int)(timer)).ToString();
            circle.fillAmount = 1 - timer / 5f;

            yield return null;
        }
        noThankBtn.gameObject.SetActive(true);
        number.text = "0";
        circle.fillAmount = 0f;
    }

    public void GameContinue(bool success) {
        if (success)
        {
            GameController.instance.GameRevivePrepare();
            Hide(); 
        }
    }
    public void NoThank() {
        GameController.instance.ReturnHome();
    }
    public void OnShowAdsFail()
    {
        noadsIcon.SetActive(true);
        noadsIcon.transform.localScale = Vector3.zero;
        noadsIcon.transform.DOScale(1f, 0.5f).SetEase(Ease.OutQuad);
        DOTween.Sequence()
            .AppendInterval(1f) 
            .Append(noadsIcon.transform.DOScale(0f, 0.5f).SetEase(Ease.OutQuad));
    }
}
