using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerfectTextVFX : MonoBehaviour
{
    [SerializeField] private List<ParticleSystem> textVFXs;
    private int index;
    private bool isStun;
    public void PlayTextVFX()
    {
        if (isStun) return;

        foreach (var vfx in textVFXs)
        {
            vfx.gameObject.SetActive(false);
        }
        int newIndex = index;
        while(newIndex == index)
        {
            index = Random.Range(0, textVFXs.Count - 1);
        }
        
        textVFXs[index].gameObject.SetActive(true);
        textVFXs[index].Play();
    }
    public void PlayOppText()
    {
        textVFXs[textVFXs.Count - 1].gameObject.SetActive(true);
        textVFXs[textVFXs.Count - 1].Play();
        isStun = true;
        StartCoroutine(FinishStun());
    }
    IEnumerator FinishStun() {
        yield return new WaitForSeconds(0.1f);
        isStun = false;
    }
}
