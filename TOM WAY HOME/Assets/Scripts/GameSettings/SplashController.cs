﻿using UnityEngine;
using System.Collections;
using System.Threading.Tasks;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Networking;
using TMPro;

public class SplashController : MonoBehaviour
{
    public static SplashController instance;
    [SerializeField]
    public Image progressBar;
    public TextMeshProUGUI loadingText;

    void Awake()
	{
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public IEnumerator LoadingHome(UnityWebRequestAsyncOperation operation = null)
    {
        float timer = 0;
        if (operation != null)
        {
            while (!operation.isDone)
            {
                timer += Time.deltaTime;
                yield return null;
            }
        }
        float duration = 1.5f;
        while(timer < duration)
        {
            timer += Time.deltaTime;
            progressBar.fillAmount = Mathf.Lerp(0f, 1f, timer / duration);
            loadingText.text = $"Loading {(int)(progressBar.fillAmount * 100)}%";
            yield return null;
        }
        
        progressBar.fillAmount = 1f;
        loadingText.text = $"Loading {(int)(progressBar.fillAmount * 100)}%";

        if (!GameData.IsTutorialMode()) //Go to Home
        {
            LoadSceneController.Instance.LoadSceneHome();
        }
    }
}

