using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameSettings : Popup
{
    public Button close;
    public TMP_InputField speed;
    public TMP_InputField sensitive;

  
    public override void Show()
    {
        base.Show();
        close.onClick.AddListener(Close);
        GetCurrentSettings();

    }
    public void GetCurrentSettings() {
        speed.text = PlayerPrefs.GetFloat("GameSettings_GameSpeed", 7).ToString();
        sensitive.text = PlayerPrefs.GetFloat("GameSettings_GameSensitive", 8).ToString();
    }

    public void SetValue() {
        bool tryParse;
        //tryParse = float.TryParse(speed.text, out RemoteConfig.instance.PlayerSpeed);
        //PlayerPrefs.SetFloat("GameSettings_GameSpeed", RemoteConfig.instance.PlayerSpeed);
        //PlayerController.instance.SetGameSpeed(RemoteConfig.instance.PlayerSpeed);

        tryParse = float.TryParse(sensitive.text, out RemoteConfig.instance.FingerSensitivity);
        PlayerPrefs.SetFloat("GameSettings_GameSensitive", RemoteConfig.instance.FingerSensitivity);
    }

    public void Close() {
        SetValue();
        gameObject.SetActive(false);
    }

}
