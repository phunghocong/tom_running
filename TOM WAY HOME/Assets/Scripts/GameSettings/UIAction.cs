﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIAction : MonoBehaviour, IPointerDownHandler
{
    private bool isFirstStart;
    public bool IsFirstStart => isFirstStart;
    public void OnPointerDown(PointerEventData eventData)
    {
        if(GameController.instance.gameStatus == GameStatus.DIE)
        {
            GameController.instance.IntroGame();
            GamePlayPopup.instance.afterClick.SetActive(true);
            GamePlayPopup.instance.beforeClick.SetActive(false);
        }
        else if (GameController.instance.gameStatus == GameStatus.CONTINUE)
        {
            GameController.instance.GameRevive();
        }
    }

     

}
