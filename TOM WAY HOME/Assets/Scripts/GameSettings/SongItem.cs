﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class SongItem : MonoBehaviour
{
    public int songID;
    public TextMeshProUGUI songName;
    public RectTransform rtf;
    [SerializeField] private Button playButton;
    [SerializeField] private Button playButtonAds;
    public GameObject playbtn, pauseBtn;

    public TextMeshProUGUI txtLabel;
    public TextMeshProUGUI txtSongHighest;
    public TextMeshProUGUI txtArtist;
    public Image[] starImages;
    public Image songItemBg;
    [SerializeField] Sprite starGray;
    [SerializeField] Sprite starGlow;
    [SerializeField] Sprite hardBackground,normalBackground,easyBackground;
    private bool isPlaying = false;
    [SerializeField] GameObject isPlayingObject;
    [SerializeField] GameObject artistLabel;
    public Image icon;
    [SerializeField] Image loadingIcon;
    private bool isPlayFirst;
    public Tween rotationTween;
    public bool isClickAds;
    [SerializeField] GameObject noadsIcon;
    private SongData _song;
    Coroutine coroutine;
    public void Init(SongData song)
    {
        _song = song;
        songID = song.id;
        isClickAds = false;
        loadingIcon.gameObject.SetActive(false);
        playButtonAds.gameObject.SetActive(song.isPlayAds);
        playButton.gameObject.SetActive(!song.isPlayAds);
        txtArtist.text= song.artist;
        int starScore = GameData.GetStarScore(song.id);
        for (int i = 0; i < starImages.Length; i++)
        {
            starImages[i].sprite = i < starScore ? starGlow : starGray;
        }
        switch (song.difficulty)
        {
            case "easy":
                songItemBg.sprite = easyBackground;
                txtLabel.color = new Color(0.1960784f, 0.6470588f, 0.1960784f); 
                break;
            case "normal":
                songItemBg.sprite = normalBackground;
                txtLabel.color = new Color(0.8000001f, 0.5176471f, 0.06666667f);
                break;
            case "hard":
                songItemBg.sprite = hardBackground;
                txtLabel.color = new Color(0.8392158f, 0.2470588f, 0.2470588f);
                break;
        }
        isPlayingObject.SetActive(song.isDonePlaying);
        artistLabel.SetActive(!song.isDonePlaying);
        icon.sprite = song.spriteUI;
        Observer.ClickAdsItem?.Invoke(this);
        noadsIcon.SetActive(false);
    }

    public void PlayGamePlay()
    {
        GameData.CurrentSongIndex = _song.id;
        HomeController.instance.ReturnHome();
        Observer.PlaySongBackGround?.Invoke();
    } 
    public void PlayGamePlayAds(bool success)
    {
        if(success)
        {
            isClickAds= true;
            GameData.CurrentSongIndex = _song.id;
            HomeController.instance.ReturnHome();
            Observer.PlaySongBackGround?.Invoke();
            Observer.ClickAdsItem?.Invoke(this);
            Observer.SwipeSong?.Invoke();
            GameData.SetSongDonePlayAds(_song,true);
        }
    }

    public void OnShowAdsFail()
    {
        noadsIcon.SetActive(true);
        noadsIcon.transform.localScale = Vector3.zero; 
        noadsIcon.transform.DOScale(1f, 0.5f).SetEase(Ease.OutQuad);
        DOTween.Sequence()
            .AppendInterval(1f) // Chờ 0.5s
            .Append(noadsIcon.transform.DOScale(0f, 0.5f).SetEase(Ease.OutQuad));
    }

    
    public void DownLoadMusic()
    {
        GameDownloadResources.Instance.DownloadMusic(_song);
    }


    public void PlayPreviewMp3()
    {
        if (!isPlayFirst)
        {
            loadingIcon.gameObject.SetActive(true);
            loadingIcon.fillAmount = 0;
            playbtn.SetActive(false);
            coroutine = StartCoroutine(GetTimePreview());
            isPlayFirst = true;
        }
        else
        {
            coroutine = StartCoroutine(GetTimePreview());
            playbtn.SetActive(false);
        }
    }

  
    void PlayPreview()
    {
        rotationTween = icon.transform.DORotate(new Vector3(0, 0, -360), 2f, RotateMode.WorldAxisAdd).SetLoops(-1, LoopType.Incremental).SetEase(Ease.Linear);
        loadingIcon.gameObject.SetActive(false);
        pauseBtn.SetActive(true);
        Observer.PlayPreview?.Invoke();
        Observer.UpdateSongItemUI?.Invoke(_song.id);
    }

    public void StopPlayPreview()
    {
        StopCoroutine(coroutine);
        NotesManager.Instance.StopPlayPreview(_song.id);
        playbtn.SetActive(true);
        pauseBtn.SetActive(false);
        rotationTween.Pause();
        icon.transform.eulerAngles = new Vector3(0, 0, 0);
    }

    public IEnumerator GetTimePreview()
    {
        _song.preview_url = _song.preview_mp3.Replace("uploads/", "");
        float startTime = Time.time;
        using (UnityWebRequest audioRequest = UnityWebRequestMultimedia.GetAudioClip(GlobalConstants.MUSIC_LINK_STREAM + _song.preview_url, AudioType.MPEG))
        {
            UnityWebRequestAsyncOperation operation = audioRequest.SendWebRequest();
            while (!operation.isDone)
            {
                loadingIcon.fillAmount = operation.progress;
                yield return null;
            }
            if (audioRequest.result == UnityWebRequest.Result.Success)
            {
                AudioClip audioClip = DownloadHandlerAudioClip.GetContent(audioRequest);
                PlayPreview();
                NotesManager.Instance.PlaySongPreview(_song.id, audioClip);
            }
        }
    }

}
