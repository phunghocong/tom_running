using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsPopup : Popup
{
    private bool musicOn, soundOn, vibrateOn;
    private float sensitiveValue;
    public Button musicOnButton, musicOffButton, soundOnButton, soundOffButton, vibrateOnButton,
        vibrateOffButton, refreshButton, rateButton, privacyButton;
    public Slider sensitiveSlider;
    private void Awake()
    {
        // Init data
        InitData();
        // Init Ui
        InitUiState();
        // Init Event

        musicOnButton.onClick.AddListener(MusicSwitch);
        musicOffButton.onClick.AddListener(MusicSwitch);

        soundOnButton.onClick.AddListener(SoundSwitch);
        soundOffButton.onClick.AddListener(SoundSwitch);

        vibrateOnButton.onClick.AddListener(VibrateSwitch);
        vibrateOffButton.onClick.AddListener(VibrateSwitch);

        refreshButton.onClick.AddListener(ResetDefaultSetting);

        rateButton.onClick.AddListener(RateReview);
        privacyButton.onClick.AddListener(PrivacyView);
    }

    private void InitData()
    {
        // TODO Get Data from Preference
        musicOn = PlayerPrefs.GetInt(GlobalConstants.MUSIC_ON_KEY, 1) == 1;
        soundOn = PlayerPrefs.GetInt(GlobalConstants.SOUND_ON_KEY, 1) == 1;
        vibrateOn = PlayerPrefs.GetInt(GlobalConstants.VIBRATE_ON_KEY, 1) == 1;
        sensitiveValue = PlayerPrefs.GetFloat(GlobalConstants.SENSITIVE_KEY, 0.5f);
    }

    private void ResetDefaultSetting()
    {
        AudioPlayer.playSoundFX(GlobalConstants.BUTTON_CLICK_SOUNDFX);
        PlayerPrefs.SetInt(GlobalConstants.MUSIC_ON_KEY, 1);
        PlayerPrefs.SetInt(GlobalConstants.SOUND_ON_KEY, 1);
        PlayerPrefs.SetInt(GlobalConstants.VIBRATE_ON_KEY, 1);
        PlayerPrefs.SetFloat(GlobalConstants.SENSITIVE_KEY, 0.5f);
        InitData();
        InitUiState();
    }

    private void RateReview() { }

    private void PrivacyView() { }

    private void InitUiState()
    {
        // Music Toggle
        musicOnButton.gameObject.SetActive(musicOn);
        musicOffButton.gameObject.SetActive(!musicOn);

        // Sound Toggle
        soundOnButton.gameObject.SetActive(soundOn);
        soundOffButton.gameObject.SetActive(!soundOn);

        // Vibrate Toggle
        vibrateOnButton.gameObject.SetActive(vibrateOn);
        vibrateOffButton.gameObject.SetActive(!vibrateOn);

        sensitiveSlider.value = sensitiveValue;
    }

    public void MusicSwitch()
    {
        musicOn = !musicOn;
        musicOnButton.gameObject.SetActive(musicOn);
        musicOffButton.gameObject.SetActive(!musicOn);
        GameData.MusicState = musicOn;
        PlayerPrefs.SetInt(GlobalConstants.MUSIC_ON_KEY, musicOn == true ? 1 : 0);
    }

    public void SoundSwitch()
    {
        soundOn = !soundOn;
        soundOnButton.gameObject.SetActive(soundOn);
        soundOffButton.gameObject.SetActive(!soundOn);
        GameData.BgSoundState = soundOn;
    }

    public void VibrateSwitch()
    {
        vibrateOn = !vibrateOn;
        vibrateOnButton.gameObject.SetActive(vibrateOn);
        vibrateOffButton.gameObject.SetActive(!vibrateOn);

    }

   
}
