﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Lean.Pool;
public class HomePopup : Popup
{
    public static HomePopup instance;
    public Button playButton, settingButton, songListButton, buildBtn;

    public RectTransform topLeftPanel, topRightPanel, bottomLeftPanel, bottomRightPanel;
    private Vector3 initialPosition1,initialPosition2, initialPosition3, initialPosition4;
    private IEnumerator coroutine;

    private const float cloudTrasitionDuration = 1.0f;

    [SerializeField] private TextMeshProUGUI coinText;
    [SerializeField] private GameObject adsImg;
    [SerializeField] private SongInfoSwipeHandler songInfoPrefab;
    [SerializeField] private SongInfoSwipeHandler songInfo;
    [SerializeField] private Transform content;
    [SerializeField] private List<SongInfoSwipeHandler> listSongInfoInti;
    [SerializeField] GameObject noadsIcon;

    //Onboarding
    [SerializeField] GameObject redDotIcon;
    [SerializeField] GameObject newIcon;
    private void Awake()
    {
        playButton.onClick.AddListener(StartGame);
        // Song List
        songListButton.onClick.AddListener(OpenSongList);
        // Setting
        settingButton.onClick.AddListener(OpenSetting);
        buildBtn.onClick.AddListener(ShowBuildRoom);
    }
    protected override void BeforeShow()
    {
        base.BeforeShow();
        instance = this;
        Setup();
        Observer.SwipeSong += ShowData;
        Observer.ClickAdsItem += UpdateAds;
    }
    protected override void AfterHidden()
    {
        base.AfterHidden();
        ResetTransform();
    }
    public override void Hide()
    {
        base.Hide();
        Observer.SwipeSong -= ShowData;
        Observer.ClickAdsItem -= UpdateAds;
    }
   

    private void Setup()
    {
        var playList = NotesManager.Instance.playlist;
        ShowData();
        songInfo.Init(this, playList[GameData.CurrentSongIndex]);
        noadsIcon.SetActive(false);
        redDotIcon.SetActive(!GameData.GetBool("ShowBuildRoom"));
        newIcon.SetActive(!GameData.GetBool("ShowSongList"));
    }
    public void UpdateAds(SongItem songItem)
    {
        foreach (var song in NotesManager.Instance.playlist)
        {
            if (song.id == songItem.songID)
            {
                song.isClickAds = songItem.isClickAds;
            }
        }
    }
    public void StartGame()
    {
        var playlist = NotesManager.Instance.playlist;
        if (playlist[GameData.CurrentSongIndex].isPlayAds && !playlist[GameData.CurrentSongIndex].isClickAds)
        {
            if (AdsMediationController.Instance.IsRewardedAvailable && Application.internetReachability != NetworkReachability.NotReachable)
            {
                Debug.Log("showw ad reward");
                AudioHelper.Instance.MutePlaylist();
                Time.timeScale = 0;
                AdsMediationController.Instance.ShowRewardedAds("play_song", OnRewardedAdsFailedToShow, OnRewardedAdsClosed);
            }
            else
            {
                OnShowAdsFail();
            }
        }
        else
        {
            SwitchScene();
        }
      
    }
    public void OnShowAdsFail()
    {
        noadsIcon.SetActive(true);
        noadsIcon.transform.localScale = Vector3.zero;
        noadsIcon.transform.DOScale(1f, 0.5f).SetEase(Ease.OutQuad);
        DOTween.Sequence()
            .AppendInterval(1f) // Chờ 0.5s
            .Append(noadsIcon.transform.DOScale(0f, 0.5f).SetEase(Ease.OutQuad));
    }
    private void OnRewardedAdsFailedToShow()
    {
        AudioHelper.Instance.UnmuteSound();
        AudioHelper.Instance.UnmutePlaylist();
        Debug.Log("show ads fail");
    }
    private void OnRewardedAdsClosed(bool success)
    {
        if (success)
        {
            AudioHelper.Instance.UnmutePlaylist();
            Time.timeScale = 1;
            SwitchScene();
        }
    }
    private void MarkShowAdsFalseDelay()
    {
        GameData.showAds = false;
    }
    void SwitchScene()
    {
        HomeController.instance.TomAnim.Play("Sleep", -1, 0f);
        AudioPlayer.playSoundFX(GlobalConstants.BUTTON_CLICK_SOUNDFX);
        cloudTransition();
    }

    public void ShowData()
    {
        var playList = NotesManager.Instance.playlist;
        coinText.text = UserDatas.GetCoin().ToString();
        if (playList[GameData.CurrentSongIndex].isPlayAds && !playList[GameData.CurrentSongIndex].isClickAds)
        {
            adsImg.SetActive(true);
        }
        else
        {
            adsImg.SetActive(false);
        }

    }
    public void OpenSetting()
    {
        AudioPlayer.playSoundFX(GlobalConstants.BUTTON_CLICK_SOUNDFX);
        PopupController.Instance.Show<SettingsPopup>();
    }


    public void OpenSongList()
    {
        GameData.SetBool("ShowSongList", true);
        newIcon.SetActive(false);
        AudioPlayer.playSoundFX(GlobalConstants.BUTTON_CLICK_SOUNDFX);
        PopupController.Instance.Show<SongListPopup>();
    }

    private void ShowBuildRoom()
    {
        redDotIcon.SetActive(false);
        GameData.SetBool("ShowBuildRoom", true);
        HomeController.instance.ShowBuildRoom();
    }

    private void cloudTransition()
    {
        PopupController.Instance.Show<PopupTransition>();
    }

    private void ResetToInitialPosition(Transform transform, Vector2 initialPosition)
    {
        transform.position = initialPosition;
    }
    private void ResetTransform()
    {
        ResetToInitialPosition(topLeftPanel, initialPosition1);
        ResetToInitialPosition(topRightPanel, initialPosition2);
        ResetToInitialPosition(bottomLeftPanel, initialPosition3);
        ResetToInitialPosition(bottomRightPanel, initialPosition4);
    }

    public void ChangeSongNext()
    {
        var notesManager = NotesManager.Instance;
        GameData.CurrentSongIndex = (GameData.CurrentSongIndex + 1) % notesManager.playlist.Count;
    }
    public void ChangeSongBack()
    {
        var notesManager = NotesManager.Instance;
        GameData.CurrentSongIndex = (GameData.CurrentSongIndex - 1 + notesManager.playlist.Count) % notesManager.playlist.Count;
    }   

    public void OnChangeNextSong(SongInfoSwipeHandler songInfoSwipe)
    {
        ChangeSongNext();
        var playList = NotesManager.Instance.playlist;
        var newSongInfo = playList[GameData.CurrentSongIndex];
        var anchoredPosition = songInfoSwipe.GetComponent<RectTransform>().anchoredPosition;
        if(newSongInfo != null)
        {
            songInfo = LeanPool.Spawn(songInfoPrefab, content);
            if (!listSongInfoInti.Contains(songInfo))
            {
                listSongInfoInti.Add(songInfo);
            }
            songInfo.GetComponent<RectTransform>().anchoredPosition = new Vector3(anchoredPosition.x + 1200f, anchoredPosition.y, 0f);
            songInfo.Init(this,newSongInfo);
            Vector3 backPos = new Vector3(anchoredPosition.x - 1200f, anchoredPosition.y, 0f);
            songInfoSwipe.GetComponent<RectTransform>().DOAnchorPos(backPos, .25f).SetEase(Ease.Linear).OnComplete(() =>
            {
                LeanPool.Despawn(songInfoSwipe.gameObject);
            });
            Invoke("MoveSong", 0.2f);
        }
    }

    public void OnChangePrevSong(SongInfoSwipeHandler songInfoSwipe)
    {
        ChangeSongBack();
        var playList = NotesManager.Instance.playlist;
        var newSongInfo = playList[GameData.CurrentSongIndex];
        var anchoredPosition = songInfoSwipe.GetComponent<RectTransform>().anchoredPosition;
        if(newSongInfo != null)
        {
            songInfo = LeanPool.Spawn(songInfoPrefab, content);
            if (!listSongInfoInti.Contains(songInfo))
            {
                listSongInfoInti.Add(songInfo);
            }
            songInfo.GetComponent<RectTransform>().anchoredPosition = new Vector3(anchoredPosition.x - 1200f, anchoredPosition.y, 0f);
            songInfo.Init(this, newSongInfo);
            Vector3 backPos = new Vector3(anchoredPosition.x + 1200f, anchoredPosition.y, 0f);
            songInfoSwipe.GetComponent<RectTransform>().DOAnchorPos(backPos, .25f).SetEase(Ease.Linear).OnComplete(() =>
            {
                LeanPool.Despawn(songInfoSwipe.gameObject);
            });
            Invoke("MoveSong", 0.2f);
        }
    }

    public void MoveSong()
    {
        Vector3 targetPos = Vector3.zero;
        songInfo.GetComponent<RectTransform>().DOAnchorPos(targetPos, .25f).SetEase(Ease.Linear).OnComplete(() =>
        {
            Observer.SwipeSong?.Invoke();
            Observer.PlaySongBackGround?.Invoke();
            isClickNext = false;
            isCLickPrev = false;
        }); ;
     
    }
    bool isClickNext,isCLickPrev;
    public void ChangeNextSong()
    {
        if (!isClickNext)
        {
            isClickNext = true;
            OnChangeNextSong(songInfo);
        }
    }
    public void ChangeSongPrev()
    {
        if (!isCLickPrev)
        {
            isCLickPrev = true;
            OnChangePrevSong(songInfo);
        }
    }

    
  
}
