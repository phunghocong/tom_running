﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class PopupReturnTransition : Popup
{
    private AsyncOperation _operation;
    [SerializeField]
    private CanvasGroup canvasGroup;
    [Range(0.1f, 10f)] public float timeLoading;
    protected override void BeforeShow()
    {
        base.BeforeShow();
        canvasGroup.alpha = 1;
    }
    public override void Show()
    {
        base.Show();
        FadeOut();
    }
    private void WaitProcess()
    {
        LoadSceneController.Instance.LoadSceneHome();
        PopupController.Instance.loadingSceneAnim.SetActive(true);
        PopupController.Instance.loadingSceneAnim.GetComponent<Animator>().Play("load");
    }

    async void FadeOut()
    {
        await Task.Delay(300);
        canvasGroup.DOFade(0f, timeLoading)
       .SetEase(Ease.OutQuad);
        WaitProcess();
    }

}
