using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GamePlayPopup : Popup
{
    public static GamePlayPopup instance;

    [SerializeField] private GameObject PauseBtn;
    [SerializeField] private GameObject HomeBtn;
    public ProgressBar progressbar;
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI coinText;
    [SerializeField] private AnimatorCallbackHolder cloudGamePlay;
    [SerializeField] private UIAction gameUiAction;
    Coroutine coroutine;
    public GameObject beforeClick, afterClick;

    protected override void BeforeShow()
    {
        base.BeforeShow();
        instance = this;
    }
    public override void Show()
    {
        base.Show();
        Init();
    }
    public override void Hide()
    {
        base.Hide();
        cloudGamePlay.gameObject.SetActive(false);
    }
    void Init()
    {
        //coroutine = StartCoroutine(cloud.IntroGame());
        cloudGamePlay.gameObject.SetActive(true);
        scoreText.text = GameController.instance.score.ToString();
        coinText.text = GameController.instance.coins.ToString();
        progressbar.SetStarsPosition(NotesManager.Instance.starTimes);
        progressbar.InitCamera();
        afterClick.SetActive(gameUiAction.IsFirstStart);
        beforeClick.SetActive(!gameUiAction.IsFirstStart);
        HomeBtn.SetActive(true);
        PauseBtn.SetActive(false);
    }
    public void Pause()
    {
        GameController.instance.GamePause();
    }
    public void GoHome()
    {
        GameController.instance.ReturnHome();
    }

    public void ResetGame()
    {
        progressbar.ResetGame();
        scoreText.text = GameController.instance.score.ToString();
        coinText.text = GameController.instance.coins.ToString();
        HomeBtn.SetActive(true);
        PauseBtn.SetActive(false);
    }

    public void UpdateScoreText(int plus)
    {
        GameController.instance.score += plus;
        if (GameController.instance.score > GameData.GetHighScore(GameData.CurrentSongIndex))
        {
            //UserDatas.SetHighestScore(GameController.instance.score);
            GameData.SetHighScore(GameData.CurrentSongIndex, GameController.instance.score);
        }
        scoreText.text = GameController.instance.score.ToString();
    }
    public void UpdateCoinText()
    {
        coinText.text = GameController.instance.coins.ToString();
    }


    public void GameUIActionStatus(bool isEnable)
    {
        gameUiAction.gameObject.SetActive(isEnable);
        HomeBtn.SetActive(isEnable);
        PauseBtn.SetActive(!isEnable);
    }
}
