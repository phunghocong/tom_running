using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SongInfoGamePlay : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI txtNameSong;
    [SerializeField] TextMeshProUGUI textArtis;

    private void OnEnable()
    {
        if (NotesManager.Instance.playlist.Count > 0 && GameData.CurrentSongIndex >= 0)
        {
            txtNameSong.text = NotesManager.Instance.playlist[GameData.CurrentSongIndex].name;
            textArtis.text = NotesManager.Instance.playlist[GameData.CurrentSongIndex].artist;
        }
        else
        {
            txtNameSong.text = NotesManager.Instance.TutorialSong.name;
            textArtis.text = NotesManager.Instance.TutorialSong.artist;
        }
    }
}
