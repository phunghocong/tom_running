using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingSceneCallBack : MonoBehaviour
{

    public void CallBackLoadScene()
    {
        GetComponent<CanvasGroup>().alpha = 1f;
        gameObject.SetActive(false);
    }
}
