﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Provides an interface to register callbacks for Animation Events, given they are named 'OnAnimEvent'
// with a string parameter defining the actual event name. 
[RequireComponent(typeof(Animator))]
public class AnimatorCallbackHolder : MonoBehaviour
{

    public void CallBackLoadScene()
    {
        PopupController.Instance.Show<PopupLoading>();
    }

    public void CallBackGamePlay()
    {
        gameObject.SetActive(false);
        GamePlayPopup.instance.GameUIActionStatus(true);
    }
}
