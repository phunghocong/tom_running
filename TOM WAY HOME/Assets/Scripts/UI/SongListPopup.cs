using DG.Tweening;
using PolyAndCode.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SongListPopup : Popup, IRecyclableScrollRectDataSource
{
    [SerializeField] private SongItem songItem;
    [SerializeField] private Transform container;
    [SerializeField]
    private List<SongItem> listSongs = new List<SongItem>();

    private float itemHeight = 256f;

    [SerializeField]
    RecyclableScrollRect _recyclableScrollRect;

    [SerializeField]
    private int _dataLength;

    //Dummy data List
    private List<ContactInfo> _contactList = new List<ContactInfo>();
    private bool preview;

    [SerializeField] ShowHideBannerController showHideBannerController;
    protected override void BeforeShow()
    {
        preview = false;
        Observer.PlayPreview += ChangePreviewStatus;
        base.BeforeShow();
    }
    public override void Show()
    {
        base.Show();
        BuildList();
        var notesManager = NotesManager.Instance;
        //_recyclableScrollRect.DataSource = this;
        Observer.UpdateSongItemUI += UpdateSongListUi;
        showHideBannerController.ShowHideBanner(false);
    }
    public override void Hide()
    {
        base.Hide();
        foreach(var item in listSongs)
        {
            Destroy(item.gameObject);
        }
        Observer.UpdateSongItemUI -= UpdateSongListUi;
        showHideBannerController.ShowHideBanner(true);
    }
    void ChangePreviewStatus()
    {
        preview = true;
    }
    public void SetCell(ICell cell, int index)
    {
        var item = cell as DemoCell;
        item.ConfigureCell(_contactList[index], index);
    }
    private void BuildList()
    {
        listSongs.Clear();
        int count = 0;
        foreach (var item in NotesManager.Instance.playlist)
        {
            SongItem song = Instantiate(songItem);
            song.songID = item.id;
            song.songName.text = item.name;
            string input = item.difficulty;
            song.txtLabel.text = ConvertFirstCharToUpper(input);
            song.transform.parent = container;
            song.txtSongHighest.text = GameData.GetHighScore(song.songID).ToString();
            song.transform.localScale = Vector3.one;
            Vector3 currentPosition = song.transform.localPosition;
            currentPosition.z = 0;
            song.transform.localPosition = currentPosition;
            song.Init(item);
            listSongs.Add(song);
            count++;
        }

    }

    public void Close()
    {
        Hide();
        HomeController.instance.ReturnHome();
        if(preview) Observer.PlaySongBackGround?.Invoke();
    }

    public int GetItemCount()
    {
        return _contactList.Count;
    }

    public void UpdateSongListUi(int id)
    {
        for(int i =0;i<listSongs.Count;i++)
        {
            if (listSongs[i].songID == id)
            {
                listSongs[i].playbtn.SetActive(false);
                listSongs[i].pauseBtn.SetActive(true);

            }
            else
            {
                listSongs[i].playbtn.SetActive(true);
                listSongs[i].pauseBtn.SetActive(false);
                listSongs[i].rotationTween.Pause();
                listSongs[i].icon.transform.eulerAngles = new Vector3(0, 0, 0);

            }

        }
    }

    string ConvertFirstCharToUpper(string input)
    {
        if (string.IsNullOrEmpty(input))
        {
            return input;
        }

        char[] charArray = input.ToCharArray();
        charArray[0] = char.ToUpper(charArray[0]);

        string result = new string(charArray);

        return result;
    }

    
}
