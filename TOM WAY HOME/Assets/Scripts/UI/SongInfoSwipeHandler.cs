﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SongInfoSwipeHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    private bool isSwiping = false;
    private Vector2 startSwipePosition;
    public HomePopup _homePopup;
    private SongData _songData;
    public bool hasSwipedRight = false;
    public bool hasSwipedLeft = false;

    [SerializeField] private Button rightClick,leftClick;
    [SerializeField] private TextMeshProUGUI txtNameSong;
    [SerializeField] List<Image> stars;
    [SerializeField] Sprite starGray;
    [SerializeField] Sprite starGlow;
    [SerializeField] TextMeshProUGUI textArtis;
    [SerializeField] TextMeshProUGUI textScore;
    [SerializeField] TextMeshProUGUI txtLabel;

    [SerializeField] Image difficultyImg;
    [SerializeField] Sprite easy, normal, hard;
    public void Init(HomePopup homePopup,SongData song)
    {
        _homePopup = homePopup;
        _songData = song;
        txtNameSong.text = song.name;
        song.isClickAds = false;
        var playlist = NotesManager.Instance.playlist;
        textArtis.text = song.artist;
        textScore.gameObject.SetActive(song.isDonePlaying);
        difficultyImg.gameObject.SetActive(!song.isDonePlaying);


        textScore.text = "Best score: " +  GameData.GetHighScore(song.id).ToString();
        switch (song.difficulty)
        {
            case "easy":
                difficultyImg.sprite = easy;
                txtLabel.color = new Color(0.1960784f, 0.6470588f, 0.1960784f);
                txtLabel.text = "Easy";
                break;
            case "normal":
                difficultyImg.sprite = normal;
                txtLabel.color = new Color(0.8000001f, 0.5176471f, 0.06666667f);
                txtLabel.text = "Normal";
                break;
            case "hard":
                difficultyImg.sprite = hard;
                txtLabel.color = new Color(0.8392158f, 0.2470588f, 0.2470588f);
                txtLabel.text = "Hard";
                break;
        }
      
    }

    public void ChangeNext()
    {
        _homePopup.OnChangeNextSong(this);
    }

    public void ChangePrev()
    {
        _homePopup.OnChangePrevSong(this);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        startSwipePosition = eventData.position;
        isSwiping = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isSwiping = false;
        hasSwipedRight = false;
        hasSwipedLeft = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (isSwiping)
        { 
            Vector2 swipeDelta = eventData.position - startSwipePosition;

            if (Mathf.Abs(swipeDelta.x) > Mathf.Abs(swipeDelta.y))
            {
                if (swipeDelta.x > 0 && !hasSwipedLeft)
                {
                    // Vuốt sang phải
                    _homePopup.OnChangePrevSong(this);
                    hasSwipedLeft = true;
                }
                else if(swipeDelta.x <= 0 && !hasSwipedRight)
                {
                    // Vuốt sang trái
                    _homePopup.OnChangeNextSong(this);
                    hasSwipedRight = true;
                }
            }
        }
    }
}
