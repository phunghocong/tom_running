using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PopupLoading : Popup
{
    [Header("Attributes")]
    public Image progressBar;
    public TextMeshProUGUI loadingText;
    private Sequence _sequence;
    public static PopupLoading instance;
    protected override void BeforeShow()
    {
        base.BeforeShow();
        progressBar.fillAmount = 0;
    }
    public override void Show()
    {
        base.Show();
        instance = this;
        Observer.PlaySong?.Invoke();
    }
    public override void Hide()
    {
        base.Hide();
        progressBar.fillAmount = 0;
    }
    public void UpdateProgress(float loadedProgress)
    {
        progressBar.fillAmount = loadedProgress;
        loadingText.text = $"Loading... {(int)(loadedProgress * 100)}%";
    }
}
