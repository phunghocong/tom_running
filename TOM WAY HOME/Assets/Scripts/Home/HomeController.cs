﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;

public class HomeController : MonoBehaviour
{
    public static HomeController instance;

    [SerializeField] private Camera camera;
    [SerializeField] private GameObject homeRoom;
    [SerializeField] private GameObject buildRoom;
    public Animator TomAnim;
    private PopupController _popupController;



    [SerializeField] private List<string> songNames;

    private IEnumerator coroutine;

    private const float cloudTrasitionDuration = 1.0f;

    private void Awake()
    {
        instance = this;
        Time.timeScale = 1f;
        // Init Event

        if (GameData.CurrentSongIndex < 0)
            GameData.CurrentSongIndex = 0;
    }

    private void Start()
    {
        // Play music
        //if (PlayerPrefs.GetInt(GlobalConstants.MUSIC_ON_KEY, 1) == 1) 
            //AudioPlayer.playMusic(GlobalConstants.MUSIC_THEME1);
        _popupController = PopupController.Instance;
        _popupController.HideAll();
        _popupController.Show<HomePopup>();
        Observer.PlaySongBackGround?.Invoke();
    }
    //public void LoadingSceneGamePlay()
    //{
    //    Observer.PlaySong?.Invoke();
    //    //LoadSceneController.Instance.LoadSceneGamePlay();
    //}

    public void StartGamePlay()
    {
        var PopupHome = PopupController.Instance.Get<HomePopup>();
        if (PopupHome.gameObject.activeInHierarchy)
        {
            HomePopup.instance.StartGame();
        }
    }

    public void ShowBuildRoom()
    {
        homeRoom.SetActive(false);
        buildRoom.SetActive(true);
        _popupController.HideAll();
    }


    public void ShowHomeRoom()
    {
        StartCoroutine(Intro(camera.transform, camera.transform.localEulerAngles.y));
        homeRoom.SetActive(true);
        buildRoom.SetActive(false);
        ReturnHome();
    }
    private float FOV;
    private float minFOV = 60f;
    private float maxFOV = 70f;
    public IEnumerator Intro(Transform tf, float startY, float endY = 244f, bool isZoomOut = false)
    {
        if(Mathf.Abs(endY - startY) < 10)
        {
            if (startY > endY)
                startY = endY + 10f;
            else
                startY = endY - 10f;
        }
        var rotation = tf.localEulerAngles;
        float y;
        float timer = 0f;
        float duration = 0.2f;
        while (timer < duration)
        {
            timer += Time.deltaTime;
            y = Mathf.Lerp(startY, endY, timer / duration);
            rotation.y = y;
            tf.localRotation = Quaternion.Euler(rotation);

            FOV = isZoomOut ? Mathf.Lerp(minFOV, maxFOV, timer / duration) : Mathf.Lerp(maxFOV, minFOV, timer / duration);
            camera.fieldOfView = FOV;

            yield return null;
        }
    }

    public void ReturnHome()
    {
        _popupController.HideAll();
        _popupController.Show<HomePopup>();
    }
}
