﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RoomItem : MonoBehaviour
{
    [SerializeField] private int itemID;
    [SerializeField] private string itemName;
    [SerializeField] private GameObject brokenModel;
    [SerializeField] private GameObject newModel;
    public GameObject buildBtn;
    public GameObject priceBtn;
    [SerializeField] private int price;
    [SerializeField] private int numAds;
    [SerializeField] private TMP_Text priceText;
    [SerializeField] private TMP_Text adsText;
    [SerializeField] GameObject noadsIcon;

    private void Start()
    {
        //PlayerPrefs.SetInt("BuildItem_ID_" + itemID.ToString(), 0);
        //PlayerPrefs.SetInt("TotalAdsForItem_ID_" + itemID.ToString(), 0);
        SetStatus();
    }
    public void SetStatus()
    {
        priceText.text = price.ToString();
        adsText.text = GetItemNumAdsRemaining().ToString();

        if(PlayerPrefs.GetInt("BuildItem_ID_" + itemID.ToString(), 0) == 0)
        {
            brokenModel.SetActive(true);
            newModel.SetActive(false);
        }
        else
        {
            brokenModel.SetActive(false);
            newModel.SetActive(true);
        }
        buildBtn.SetActive(PlayerPrefs.GetInt("BuildItem_ID_" + itemID.ToString(), 0) == 0);
        priceBtn.SetActive(false);
        noadsIcon.SetActive(false);
    }
    public void ShowPrice() {
        BuildManager.instance.HideAllPrice();
        buildBtn.SetActive(false);
        priceBtn.SetActive(true);
        priceText.transform.parent.gameObject.SetActive(price > 0);
        adsText.transform.parent.gameObject.SetActive(numAds > 0);
    }
    public void UnlockFromBuildMission()
    {
        StartCoroutine(IEUnlockItem());
    }
    private IEnumerator IEUnlockItem()
    {
        yield return new WaitForSeconds(0.3f);
        UnlockItem();
    }
    public void UnlockItem()
    {
        if (price > 0 && price <= UserDatas.GetCoin())
        {
            BuildManager.instance.BuildItem(this);
        }
        else
        {
            Debug.LogError("Show PopUp Buy Coin");
        }
    }

    public void UnlockItemWithAds(bool success)
    {
        if(success )
        {
            if (numAds > 0)
            {
                CheckAdsComplete();
            }
        }
    }
    public void OnShowAdsFail()
    {
        noadsIcon.SetActive(true);
        noadsIcon.transform.localScale = Vector3.zero;
        noadsIcon.transform.DOScale(1f, 0.5f).SetEase(Ease.OutQuad);
        DOTween.Sequence()
            .AppendInterval(1f) // Chờ 0.5s
            .Append(noadsIcon.transform.DOScale(0f, 0.5f).SetEase(Ease.OutQuad));
    }
    public void CheckAdsComplete()
    {
        Debug.LogError("ads complete");
        int currentTotalAds = PlayerPrefs.GetInt("TotalAdsForItem_ID_" + itemID.ToString(), 0);
        PlayerPrefs.SetInt("TotalAdsForItem_ID_" + itemID.ToString(), currentTotalAds + 1);

        if(currentTotalAds + 1 == numAds)
        {
            BuildManager.instance.BuildItem(this);
        }
        else
        {
            adsText.text = GetItemNumAdsRemaining().ToString();
        }
    }
    public int GetItemPrice()
    {
        return price;
    }
    public int GetItemNumAds()
    {
        return numAds;
    }
    public int GetItemNumAdsRemaining()
    {
        return numAds - PlayerPrefs.GetInt("TotalAdsForItem_ID_" + itemID.ToString(), 0);
    }
    public int GetItemID()
    {
        return itemID;
    }
    public string GetItemName()
    {
        return itemName;
    }
}
