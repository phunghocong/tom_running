using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BuildMissionItem : MonoBehaviour
{
    public RectTransform rTf;
    [SerializeField] private RoomItem roomItem;
    [SerializeField] private TMP_Text itemName;
    [SerializeField] private TMP_Text priceText;
    [SerializeField] private TMP_Text numAdsText;
    [SerializeField] private Button itemBtn;

    private void Awake()
    {
        itemBtn.onClick.AddListener(CheckToBuild);
    }

    public void SetupItem(RoomItem item, string name, int coinPrice, int adsNum)
    {
        roomItem = item;
        itemName.text = name;
        priceText.text = coinPrice.ToString();
        numAdsText.text = adsNum.ToString();
        priceText.transform.parent.gameObject.SetActive(coinPrice > 0);
        numAdsText.transform.parent.gameObject.SetActive(adsNum > 0);
    }
    public void CheckToBuild()
    {
        BuildManager.instance.buildMission.SetActive(false);
        roomItem.UnlockFromBuildMission();
    }
}
