﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SongListController : MonoBehaviour
{
    [SerializeField]
    public GameObject SongListPanel;
    public void SongItemSlected(int index)
    {
        SongListPanel.SetActive(false);
        PlayerPrefs.SetInt(GlobalConstants.SONG_SELECTED_INDEX, index);
    }

   
}

