﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SensitiveSliderHandler : MonoBehaviour, IPointerUpHandler
{
    Slider slider;

    void Start()
    {
        slider = GetComponent<Slider>();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        PlayerPrefs.SetFloat(GlobalConstants.SENSITIVE_KEY, slider.value);
    }
}

