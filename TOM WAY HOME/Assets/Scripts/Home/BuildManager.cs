using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BuildManager : MonoBehaviour
{
    public static BuildManager instance;

    [SerializeField] private TextMeshProUGUI totalCoin;
    public List<RoomItem> roomItems;
    [SerializeField] private ParticleSystem buildVFX;
    [SerializeField] private Camera camera;
    [SerializeField] private Button buildMissionBtn;
    public GameObject buildMission;
    [SerializeField] private Button closeBtn;

    //private Vector3 camAngle;
    private Vector3 _pos;
    private float _centerX;
    private float _destination;
    private bool _isMouseButton;
    private float minY = 210f;
    private float maxY = 255f;

    private void Awake()
    {
        instance = this;
        //camAngle = camera.transform.localEulerAngles;
        //UserDatas.SetCoin(2000);
        buildMissionBtn.onClick.AddListener(ShowBuildMissionPopup);
        closeBtn.onClick.AddListener(CloseBuildRoom);
    }
    private void OnEnable()
    {
        totalCoin.text = UserDatas.GetCoin().ToString();
        _isMouseButton = false;
        StartCoroutine(HomeController.instance.Intro(camera.transform, camera.transform.localEulerAngles.y, 244f, true));
    }
    public void BuildItem(RoomItem item)
    {
        HomeController.instance.TomAnim.Play("Unlock", -1, 0f);
        buildVFX.Play();
        UserDatas.PlusCoin(-item.GetItemPrice());
        totalCoin.text = UserDatas.GetCoin().ToString();
        PlayerPrefs.SetInt("BuildItem_ID_" + item.GetItemID().ToString(), 1);

        foreach (var it in roomItems)
        {
            it.SetStatus();
        }
    }
    public void HideAllPrice() {
        foreach (var it in roomItems) {
            it.buildBtn.SetActive(PlayerPrefs.GetInt("BuildItem_ID_" + it.GetItemID().ToString(), 0) == 0);
            it.priceBtn.SetActive(false);
        }
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _pos = Input.mousePosition;
            _centerX = camera.transform.localEulerAngles.y;
        }
        _isMouseButton = Input.GetMouseButton(0) && !buildMission.activeSelf;
    }
    private void FixedUpdate()
    {
        if (!_isMouseButton) return;
        _destination = _centerX - (Input.mousePosition.x - _pos.x) * 0.04f;
        var rotation = camera.transform.localEulerAngles;
        float deltaMove = _destination - rotation.y;

        rotation.y += deltaMove;

        if (rotation.y < minY) 
            rotation.y = minY;
        if (rotation.y > maxY)
            rotation.y = maxY;

        camera.transform.localRotation = Quaternion.Euler(rotation);
    }
    private void ShowBuildMissionPopup()
    {
        buildMission.SetActive(true);
    }
    private void CloseBuildRoom()
    {
        if(HomeController.instance != null)
        {
            HomeController.instance.ShowHomeRoom();
        }
    }
}
