using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BuildMission : MonoBehaviour
{
    [SerializeField] private BuildMissionItem item;
    [SerializeField] private RectTransform content;
    [SerializeField] private List<BuildMissionItem> items = new List<BuildMissionItem>();
    [SerializeField] private Button closeBtn;
    [SerializeField] private Image progressImage;
    [SerializeField] private TMP_Text progressText;

    private void Awake()
    {
        closeBtn.onClick.AddListener(Close);
    }
    private void OnEnable()
    {
        SetUpListItem();
    }
    private void SetUpListItem()
    {
        foreach (var it in items)
            if(it != null) Destroy(it.gameObject);

        int count = -1;
        int itemUnlocked = 0;

        float firstItemPosY = item.rTf.anchoredPosition.y;
        for (int i = 0; i < BuildManager.instance.roomItems.Count; i++)
        {
            RoomItem it = BuildManager.instance.roomItems[i];
            if (PlayerPrefs.GetInt("BuildItem_ID_" + it.GetItemID().ToString(), 0) == 0)
            {
                count++;
                BuildMissionItem mIt = Instantiate(item, item.transform.parent);
                items.Add(mIt);
                mIt.SetupItem(it, it.GetItemName(), it.GetItemPrice(), it.GetItemNumAdsRemaining());
                mIt.rTf.anchoredPosition = Vector3.up * (firstItemPosY - 190 * count);
                mIt.gameObject.SetActive(true);
            }
            else
            {
                itemUnlocked++;
            }
        }

        content.sizeDelta = new Vector2(content.sizeDelta.x, (count + 1) * 190 + 50f);

        StartCoroutine(ShowPercent(0.5f, (float)itemUnlocked / BuildManager.instance.roomItems.Count));
        progressText.text = itemUnlocked.ToString() + "/" + BuildManager.instance.roomItems.Count.ToString();
    }
    IEnumerator ShowPercent(float duration = 0.5f, float fillAmount = 0f)
    {
        float timer = 0;
        while(timer < duration)
        {
            timer += Time.deltaTime;
            progressImage.fillAmount = Mathf.Lerp(0, fillAmount, timer / duration);
            yield return null;
        }
    }
    private void Close()
    {
        StopAllCoroutines();
        gameObject.SetActive(false);
    }
}
