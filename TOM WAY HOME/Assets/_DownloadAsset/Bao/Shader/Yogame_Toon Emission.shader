Shader "Yogame/Toon Emission" {
	Properties {
		_ToonRamp ("Toon Ramp", 2D) = "white" {}
		_RimColor ("Rim Color", Vector) = (0,1,0.8758622,0)
		_RimPower ("Rim Power", Range(0.1, 10)) = 0.5
		_RimOffset ("Rim Offset", Float) = 0.24
		_DiffuseColor ("DiffuseColor", Vector) = (1,1,1,1)
		[Toggle(_TEXTURE_ON)] _Texture ("Texture", Float) = 0
		_Diffuse ("Diffuse", 2D) = "white" {}
		[Toggle(_EMISSION_ON)] _Emission ("Emission", Float) = 0
		_EmissionTexture ("EmissionTexture", 2D) = "white" {}
		[HDR] _EmissionColor ("EmissionColor", Vector) = (1,1,1,1)
		_EmissionForce ("EmissionForce", Float) = 0
		[HideInInspector] _texcoord ("", 2D) = "white" {}
		[HideInInspector] __dirty ("", Float) = 1
	}
	//DummyShaderTextExporter
	SubShader{
		Tags { "RenderType" = "Opaque" }
		LOD 200
		CGPROGRAM
#pragma surface surf Standard
#pragma target 3.0

		struct Input
		{
			float2 uv_MainTex;
		};

		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			o.Albedo = 1;
		}
		ENDCG
	}
	Fallback "Diffuse"
}